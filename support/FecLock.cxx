#include "FecLock.h"
#include "included.xpm"
#include "excluded.xpm"
#include <TGPicture.h>

FecLock::FecLock(TGCompositeFrame& p, const char* fullname)
  : BasicObject(p, fullname, kLHintsTop), 
    fLocked(gClient->GetPicturePool()
	    ->GetPicture("included.xpm", 
			 const_cast<char**>(included_xpm))),
    fUnlocked(gClient->GetPicturePool()
	      ->GetPicture("excluded.xpm", 
			   const_cast<char**>(excluded_xpm))),
    fLock(this, fUnlocked, -1, 
	  TGButton::GetDefaultGC().GetGC(),
	  kFixedWidth)
{
  fLock.SetHeight(24);
  SetButton(fLock, 24);
  RemoveFrame(&fLabel);
}
void FecLock::HandleClicked()
{
  SmiAction* action  = 0;
  Int_t i = 0;
  while ((action = getNextAction())) { 
    std::string actName = action->getName();
    if      (IsLocked()  && actName == "EXCLUDE") break;
    else if (!IsLocked() && actName == "INCLUDE") break;
  }
  if (!action) return;
  SendAction(action);
}
void FecLock::Update() {
  bool locked = IsLocked();
  fLock.SetPicture(locked ? fLocked : fUnlocked);
  fLock.ChangeBackground(locked ? 0x00dd00 : 0xafafaf);
  gClient->NeedRedraw(this, kTRUE);
}
//
// EOF
//
