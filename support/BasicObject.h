// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_SUPPORT_BASICOBJECT
#define FMD_DOMAIN_SUPPORT_BASICOBJECT
#include <TGFrame.h>
#include <TGLabel.h>
#include <map>
#include <string>
#ifndef __CINT__
#include <smixx/smiuirtl.hxx>
#else 
class SmiObject;
class SmiAction;
class SmiParam;
#endif
class TGButton;
class TGPopupMenu;
class Action;

//====================================================================
/**
 * A base class for objects
 * 
 */
struct BasicObject : public  TGHorizontalFrame
#ifndef __CINT__
		     , public  SmiObject
#endif 

{
  /** 
   * Constructor 
   * 
   * @param p           Parent fame
   * @param fullname    Full name of object 
   * @param layoutOpts  Layout options 
   */
  BasicObject(TGCompositeFrame& p, const char* fullname, 
	      Int_t layoutOpts=kLHintsExpandX|kLHintsTop);
  /**
   * Destructor 
   * 
   */
  virtual ~BasicObject() {}
  /** 
   * Set the button 
   * 
   * @param b Button to use
   * @param w Width of button
   */
  void SetButton(TGButton& b, Int_t w=200);
  /** 
   * Get the object name from the full name 

   * 
   * @param fullname 
   * 
   * @return Object nmae 
   */
  const char* ObjectName(const char* fullname) const;
  /** 
   * Called when the state of the object changes 
   * 
   */
  void smiStateChangeHandler();
  /** 
   * Get the name 
   * 
   * 
   * @return The name 
   */
  const std::string& Name() const { return fName; }
  /** 
   * Get the state 
   * 
   * 
   * @return 
   */
  const std::string& State() const { return fState; }
  /** 
   * Update 
   * 
   */
  virtual void Update() { }
  /** 
   * Get colour correspondinf to a state
   * 
   * @param state State
   * 
   * @return Colour corresponding to the state 
   */
  Pixel_t StateColor(const std::string& state) const;
  /** 
   * Handle click on button.  Show pop-up dialog with available
   * actions. 
   * 
   */
  virtual void HandleClicked();
  /** 
   * Handle pop-down of actions menu
   * 
   */
  virtual void HandleDown();
  /** 
   * Handle action selection
   * 
   * @param id Selected action 
   */
  virtual void HandleAction(Int_t id);
  /** 
   * Enable the button 
   */
  void Enable(Bool_t on=true);
  /** 
   * List paramaters and attributes
   * 
   */
  void ListParamsAttrs();

  static void ParseName(const std::string& full, 
			std::string& domain, 
			std::string& object);
protected:
  /** 
   * Send the action
   * 
   * @param action Action to send
   * 
   * @return Return value 
   */
  virtual int SendAction(SmiAction* action);
  /**
   * Create a parameter dialog 
   *
   * @param action Action to create dialog for 
   * @param exec On completion of the dialog, this will contain true
   * if the action is to be executed.
   *
   * @return Created dialog 
   */
  virtual Action* CreateAction(SmiAction* action, bool& exec);
  /** 
   * Default constructor 
   * 
   */
  BasicObject();

  /** Map of actions */
  typedef std::map<std::string,Action*> ActionMap;
  /** Object name */
  std::string   fName;
  /** State */
  std::string   fState;
  /** Label */
  TGLabel       fLabel;
  /** Button */
  TGButton*     fButton;
  /** Actions menu */
  TGPopupMenu*  fMenu;
  /** Map of actions */
  ActionMap     fActions;
  ClassDef(BasicObject,0)
};
#endif
//
// EOF
// 
