#ifndef FMD_DOMAIN_SUPPORT_OBJECT
#define FMD_DOMAIN_SUPPORT_OBJECT
#include "BasicObject.h"
#include <TGButton.h>

//====================================================================
/**
 * A 'normal' object
 * 
 */
struct Object : public BasicObject
{
  /** 
   * Constructor 
   * 
   * @param p           Parent
   * @param fullname    Full object name
   * @param bw          Button width 
   * @param layoutOpts  Laoyut
   * 
   * @return 
   */
  Object(TGCompositeFrame& p, const char* fullname, Int_t bw=200,
	 Int_t layoutOpts=kLHintsExpandX|kLHintsTop);
  /**
   * Destructor
   * 
   */
  virtual ~Object() {}
  /** 
   * Update display 
   * 
   */
  virtual void Update();
  TGLabel fState;
  /** Actions button */
  // TGTextButton fActionButton;
  TGPictureButton fActionButton;
  ClassDef(Object,0)
};
#endif
//
// EOF
//
