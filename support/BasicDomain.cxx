#include "BasicDomain.h"
#include "Lock.h"
#include <dim/dic.h>
#include <dim/dic.hxx>
#include <smixx/smiuirtl.hxx>
#include <TApplication.h>
#include <TGTableLayout.h>

BasicDomain::BasicDomain(const char* name)  
  : TGMainFrame(gClient->GetRoot(), 400, 400),
    SmiDomain(const_cast<char*>(name)), 
    fName(name),
    fMenuBar(this), 
    fFileMenu(*(fMenuBar.AddPopup("&File"))), 
    fLock(0)
{
  DISABLE_AST
  AddFrame(&fMenuBar, new TGLayoutHints(kLHintsExpandX));
  Connect("CloseWindow()", "BasicDomain", this, "HandleClose()");
  fFileMenu.AddEntry("&Exit", kExit);
  fFileMenu.Connect("Activated(Int_t)", "BasicDomain", this, 
		    "HandleFileMenu(Int_t)");
  SetWindowName(Form("SMI Domain %s", name));
  SetIconName(name);
  ENABLE_AST
}
/** 
 * Handle close action
 * 
 */
void 
BasicDomain::HandleClose()
{
  DontCallClose();
  if (fLock) fLock->Release();
  gApplication->Terminate();
}
void 
BasicDomain::HandleFileMenu(Int_t id)
{
  switch (id) { 
  case kExit: HandleClose(); break;
  }
}
BasicObject* 
BasicDomain::MakeObject(const std::string& name)
{
  std::cerr << "BasicDomain::MakeObject(\"" << name << "\") called\n"
	    << "This should NOT happen - BasicDomain::smiDomainHandler is\n"
	    << "probably called before full construction of object!" 
	    << std::endl;
  exit(1);
}

void 
BasicDomain::smiDomainHandler()
{
  SmiObject* obj = 0;
  bool gotObjects = false;
  while ((obj = getNextObject())) { 
    std::string name = obj->getName();
    BasicObject* o = MakeObject(name);
    if (!o) continue;
    fObjects.push_back(o);
    gotObjects = true;
  }
  if (!gotObjects) { 
    Warning("smiDomainHandler", "No objects defined in domain %s",
	    fName.c_str());
    // exit(1);
  }
  MapSubwindows();
  Layout();
  if (!gotObjects) 
    Resize(600, 400);
  else 
    Resize(GetDefaultSize());
  MapWindow();
}
void 
BasicDomain::smiStateChangeHandler()
{
}
void 
BasicDomain::Update()
{
  // fTimer.TurnOff();
#if 0
  for (ObjectVector::const_iterator i = fObjects.begin(); 
       i != fObjects.end(); ++i) { 
    (*i)->Update();
  }
#endif
  gClient->NeedRedraw(this, kTRUE);
  // fTimer.TurnOn();
}
void 
BasicDomain::Enable(bool on)
{
  for (ObjectVector::const_iterator i = fObjects.begin(); 
       i != fObjects.end(); ++i) { 
    (*i)->Enable(on);
  }
}
//
// EOF
//
