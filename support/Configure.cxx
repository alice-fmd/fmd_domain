#include "Configure.h"
#include "Util.h"
#include <smixx/smiuirtl.hxx>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGComboBox.h>
#include <TGButton.h>
#include <TGLabel.h>
#define SEP ';'

//____________________________________________________________________
Configure::Configure(const TGWindow* p, 
		     const char* obj, 
		     SmiAction* action, 
		     bool& exec) 
  : Action(p, obj, action, exec)
{
}

//____________________________________________________________________
void 
Configure::ParseArgs(const std::string& args,
		     PartMap& m)
{
  std::cout << "Got arguments: " << args << std::endl;
  std::stringstream s(args);
  while (!s.eof()) { 
    std::string token;
    std::getline(s, token, SEP);
    if (s.bad()) break;
    if (token.size() <= 0) continue;

    size_t eq = token.find('=');
    downcase(token);
    if (eq == std::string::npos || eq >= token.size()-1) {
      std::cout << " Adding key " << token << " as true" << std::endl;
      m[token] = "true";
      continue;
    }
    std::string key = token.substr(0, eq);
    std::string val = token.substr(eq+1);
    std::cout << " Adding key " << key << "=" << val << ";" << std::endl;
    m[key] = val;
    // std::cout << " Adding key " << key << "=" << val 
    //  << " " << m[key] << std::endl;
  }
}

//____________________________________________________________________
const std::string&
Configure::GetPart(const std::string& name, 
		   const PartMap&     m, 
		   const std::string& def) const
{
  PartMap::const_iterator i = m.find(name);
  if (i != m.end()) 
    return i->second;
  return def;
}

//____________________________________________________________________
TGCheckButton*
Configure::AddOption(TGButtonGroup* frame, 
		     const PartMap&     m,
		     const std::string& opt) const
{
  TGCheckButton*     ret = new TGCheckButton(frame, opt.c_str());
  static const std::string def("true");
  const std::string& v   = GetPart(opt, m, def);
  // std::cout << "Adding option " << opt << " with value '" << v 
  // << "'" << std::endl;
  bool               on  = (v == "true") || (v == "yes"); 
  ret->SetDown(on);
  return ret;
}

//____________________________________________________________________
TGComboBox* 
Configure::AddSelect(const char*       label,
		     const PartMap&    parts, 
		     const char**      values, 
		     const int*        ids)
{
  TGHorizontalFrame* frame = new TGHorizontalFrame(this);
  TGComboBox*        box   = new TGComboBox(frame);
  const char**       ptr   = values;  
  int                i     = 0;
  while ((*ptr)) { 
    const char* value = *ptr;
    int id = (ids ? ids[i] : i);
    box->AddEntry(value, id);
    ptr++;
    i++;
  }
  AddLine(label, frame, box);
  static const std::string def(values[0]);
  std::string v = GetPart(label, parts, def);
  //std::cout << "Adding selection " << label << " with value '" 
  // << v << "'" << std::endl;
  TGLBEntry* entry = box->FindEntry(v.c_str());
  int        id    = (ids ? ids[0] : 0);
  if (entry) id    = entry->EntryId();
  box->Select(id, false);
  box->EnableTextInput(false);
  box->GetListBox()->SetWidth(150);
  box->SetWidth(150);
  box->GetListBox()->SetHeight(20);
  box->SetHeight(20);

  return box;
}  
  
//____________________________________________________________________
TGNumberEntry*
Configure::AddNumber(const char*       label, 
		     const PartMap&    parts, 
		     unsigned short    least, 
		     unsigned short    largest,
		     unsigned short    def)
{
  TGHorizontalFrame*  frame = new TGHorizontalFrame(this);
  TGNumberEntry*      entry = 
    new TGNumberEntry(frame, def, 5, -1, 
		      TGNumberFormat::kNESInteger, 
		      TGNumberFormat::kNEANonNegative,
		      TGNumberFormat::kNELLimitMinMax,
		      least, largest);
  AddLine(label, frame, entry);
  static const std::string defs("-");
  const std::string& v = GetPart(label, parts, defs);
  unsigned int       t = def;
  if (v != "-") String2Value(v, t);
  entry->SetIntNumber(t);
  // std::cout << "Adding number " << label << " with value " 
  // << v << "->" << t << "/|" 
  // << entry->GetIntNumber() << ";" << std::endl;
  return entry;
}
		     
//____________________________________________________________________
void 
Configure::SetupParams()
{
  SmiParam*   param = fAction->getFirstParam();
  std::string args  = param->getValueString();
  std::string tag;
  std::string actfec;
  std::stringstream first(args);
  std::getline(first, tag,   SEP);
  std::getline(first, actfec, SEP);
  std::cout << "Tag: " << tag << " Cards: " << actfec << ";" << std::endl;
  
  PartMap     parts;
  std::string rem;
  std::getline(first, rem);
  ParseArgs(rem, parts);
 
  TGCompositeFrame* frame = 0;
  {
    frame = new TGHorizontalFrame(this);
    fTag  = new TGComboBox(frame);
    fTag->AddEntry("physics", 0);
    fTag->AddEntry("standalone", 0);
    fTag->AddEntry("pedestal", 0);
    fTag->AddEntry("gain", 0);
    AddLine("Type", frame, fTag);
    TGLBEntry* entry = fTag->FindEntry(tag.c_str());
    int        id    = (entry ? entry->EntryId() : 0);
    fTag->Select(id, false);
    fTag->EnableTextInput(false);
    fTag->GetListBox()->SetWidth(150);
    fTag->SetWidth(150);
    fTag->GetListBox()->SetHeight(20);
    fTag->SetHeight(20);
  }
  {
    frame                    = new TGHorizontalFrame(this);
    TGButtonGroup*     cards = new TGVButtonGroup(frame, "Cards");
    fFec00                   = new TGCheckButton(cards, "0x00");
    fFec01                   = new TGCheckButton(cards, "0x01");
    fFec10                   = new TGCheckButton(cards, "0x10");
    fFec11                   = new TGCheckButton(cards, "0x11");
    AddLine("", frame, cards);
    unsigned int t = 0;
    String2Value(actfec, t);
    if (t & 0x01)    fFec00->SetDown();
    if (t & 0x02)    fFec01->SetDown();
    if (t & 0x10000) fFec10->SetDown();
    if (t & 0x20000) fFec11->SetDown();
  }
  {
    frame               = new TGHorizontalFrame(this);
    TGButtonGroup* opts = new TGVButtonGroup(frame, "Options");
    fMonitor     = AddOption(opts, parts, "monitor");
    fInterrupt   = AddOption(opts, parts, "interrupt");
    fSOD         = AddOption(opts, parts, "sod");
    fTrigger     = AddOption(opts, parts, "trigger");
    fInterleaved = AddOption(opts, parts, "interleaved");
    fRewrite     = AddOption(opts, parts, "rewrite");
    AddLine("", frame, opts); 
  }
  {
    frame                    = new TGHorizontalFrame(this);
    TGButtonGroup* intGroups = new TGVButtonGroup(frame, "Interrupts");
    fT  = new TGCheckButton(intGroups, "T");
    fDC = new TGCheckButton(intGroups, "DC");
    fAC = new TGCheckButton(intGroups, "AC");
    fDV = new TGCheckButton(intGroups, "DV");
    fAV = new TGCheckButton(intGroups, "AV");
    AddLine("", frame, intGroups);
    static const std::string def("T|DC|AC");
    const std::string& v = GetPart("int_groups", parts, def);
    // std::cout << "Adding interrupt groups " << v << ";" << std::endl;
    if (v.find("T")  != std::string::npos) fT->SetDown();
    if (v.find("DC") != std::string::npos) fDC->SetDown();
    if (v.find("AC") != std::string::npos) fAC->SetDown();
    if (v.find("DV") != std::string::npos) fDV->SetDown();
    if (v.find("AV") != std::string::npos) fAV->SetDown();
  }
  {
    const char* values[] = { "2", "4", "1", 0 };
    const int   ids[]    = {   2,   4,   1, 0 };
    fOver                = AddSelect("over", parts, values, ids);
  }
  fZero      = AddNumber("zero",      parts, 0,      2,   1);
  fNBuf      = AddNumber("nbuf",      parts, 1,      4,   1);
  fCalIter   = AddNumber("cal_iter",  parts, 1,   1000, 100);
  fCalStep   = AddNumber("cal_step",  parts, 1,    255,  32);
  fL1Window  = AddNumber("l1window",  parts, 200, 1024, 212);
  fL0Timeout = AddNumber("l0timeout", parts, 200, 1024, 222);
  {
    unsigned int       min  = 0;
    unsigned int       max  = 127;
    frame                   = new TGHorizontalFrame(this);
    TGHorizontalFrame* cont = new TGHorizontalFrame(frame);
    fMin  = new TGNumberEntry(cont, min, 5, -1, 
			      TGNumberFormat::kNESInteger, 
			      TGNumberFormat::kNEANonNegative,
			      TGNumberFormat::kNELLimitMinMax,
			      min, max);
    fMax  = new TGNumberEntry(cont, max, 5, -1, 
			      TGNumberFormat::kNESInteger, 
			      TGNumberFormat::kNEANonNegative,
			      TGNumberFormat::kNELLimitMinMax,
			      min, max);
    cont->AddFrame(fMin);
    cont->AddFrame(fMax);
    AddLine("range", frame, cont);
    static const std::string def("0-127");
    const std::string& v    = GetPart("range", parts, def);
    // std::cout << "Adding range with value " << v << ";" << std::endl;
    size_t             dash = v.find('-');
    if (dash != std::string::npos && dash < v.size()-2) { 
      std::string smin = v.substr(0, dash);
      std::string smax = v.substr(dash+1);
      String2Value(smin, min);
      String2Value(smax, max);
      fMin->SetIntNumber(min);
      fMax->SetIntNumber(max);
    }
  }
}

//____________________________________________________________________
void
Configure::CheckOption(TGCheckButton* b, std::stringstream& l)
{
  if (!b) return;
  if (!b->IsDown()) return;
  l << SEP << b->GetString();
} 
//____________________________________________________________________
void
Configure::CheckNumber(const char* name, 
		       TGNumberEntry* e, 
		       std::stringstream& l)
{
  if (!e) return;
  // std::cout << "Value of entry " << name << " " << e << " is "
  // << e->GetIntNumber() << std::endl;
  l << SEP << name << "=" << e->GetIntNumber();
}
 
//____________________________________________________________________
void
Configure::SetParameters()
{
  std::stringstream l;
  {
    TGLBEntry* e  = fTag->GetSelectedEntry();
    int        id = (e ? e->EntryId() : 1);
    // std::cout << "Selected entry id: " << id << " " << e << std::endl;
    switch (id) { 
    case 0:  l << "physics";    break;
    case 1:  l << "standalone"; break;
    case 2:  l << "pedestal";   break;
    case 3:  l << "gain";       break;
    default: l << "physics";    break;
    }
  }
  {
    unsigned int mask = 0;
    if (fFec00->IsDown()) mask |= 0x1;
    if (fFec01->IsDown()) mask |= 0x2;
    if (fFec10->IsDown()) mask |= 0x10000;
    if (fFec11->IsDown()) mask |= 0x20000;
    l << SEP << "0x" << std::hex << mask << std::dec;
  }
  {
    TGLBEntry* e  = fOver->GetSelectedEntry();
    int        id = (e ? e->EntryId() : 2);
    l << SEP << "over=" << id;
  }
  CheckOption(fMonitor,     l);
  CheckOption(fInterrupt,   l);
  CheckOption(fSOD,         l);
  CheckOption(fTrigger,     l);
  CheckOption(fInterleaved, l);
  CheckOption(fRewrite,     l);
  CheckNumber("zero",      fZero,      l);
  CheckNumber("nbuf",      fNBuf,      l);
  CheckNumber("cal_iter",  fCalIter,   l);
  CheckNumber("cal_step",  fCalStep,   l);
  CheckNumber("l1window",  fL1Window,  l);
  CheckNumber("l0timeout", fL0Timeout, l);
  l << SEP << "range=" << fMin->GetIntNumber() << "-" << fMax->GetIntNumber();
  {
    l << SEP << "int_groups=";
    bool has = false;
    if (fT->IsDown())  { if (has) l << "|"; l << "T";  has=true; }
    if (fDC->IsDown()) { if (has) l << "|"; l << "DC"; has=true; }
    if (fAC->IsDown()) { if (has) l << "|"; l << "AC"; has=true; }
    if (fDV->IsDown()) { if (has) l << "|"; l << "DV"; has=true; }
    if (fAV->IsDown()) { if (has) l << "|"; l << "AV"; has=true; }
  }

  char* args = "ARGS";
  char* val  = const_cast<char*>(l.str().c_str());
  std::cout << "Argument string is \"" << val << "\"" << std::endl;
  fAction->setParam(args, val);
}

// 
// EOF
//
