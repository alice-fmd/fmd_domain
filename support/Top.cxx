#include "Top.h"
#include "Util.h"
#include "Configure.h"

Action*
Top::CreateAction(SmiAction* action, bool& exec)
{
  if (!action) return 0;
  std::string name = action->getName();
  upcase(name);
  if (name == "CONFIGURE") 
    return new Configure(this, Name().c_str(), action, exec);
  return BasicObject::CreateAction(action, exec);
}

//
// EOF
//
