//-*- mode: C++ -*-
#ifndef FMD_DCS_SUPPORT_BASICDOMAIN
#define FMD_DCS_SUPPORT_BASICDOMAIN
#include <TGFrame.h>
#include <TGMenu.h>
#include <vector>
#ifndef __CINT__
# include <smixx/smiuirtl.hxx>
#else
class SmiDomain;
class SmiObject;
#endif
class Lock;
class BasicObject;

//====================================================================
/**
 * The FSM domain
 * 
 */
struct BasicDomain : public TGMainFrame
#ifndef __CINT__
	      , public SmiDomain
#endif
{
  /** 
   * Button ids 
   */
  enum { 
    kExit = 1
  };
  /** 
   * Constructor
   * 
   * @param name Domain name 
   */
  BasicDomain(const char* name);
  /** 
   * Handle close action
   * 
   */
  void HandleClose();
  /** 
   * Handle menu
   * 
   * @param id 
   */
  void HandleFileMenu(Int_t id);
  /** 
   * Called upon connecting to the domain
   * 
   */
  void smiDomainHandler();
  /** 
   * Handle any state change 
   * 
   */
  void smiStateChangeHandler();
  /** 
   * update the display 
   * 
   */
  void Update();
  /** 
   * Enable all buttons 
   * 
   * @param on 
   */
  void Enable(bool on);
  const std::string& Name() const { return fName; }
protected:
  /** 
   * Constructor 
   */
  BasicDomain();
  /** 
   * Make an object 
   */
  virtual BasicObject* MakeObject(const std::string& name);
  /** List of object   */
  typedef std::vector<BasicObject*> ObjectVector;
  std::string fName;
  /** List of object */
  ObjectVector      fObjects;
  /** Menu bar */
  TGMenuBar         fMenuBar;
  /** File menu */
  TGPopupMenu&      fFileMenu;
  /** Possible lock */
  Lock* fLock;

  ClassDef(BasicDomain,0)
};
#endif
//
// EOF
//
