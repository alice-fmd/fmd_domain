// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_SUPPORT_TOP
#define FMD_DOMAIN_SUPPORT_TOP
#include "Object.h"
#include <TGButton.h>

//====================================================================
/**
 * A lock object
 * 
 */
struct Top : public Object 
{
  /** Domain is a fried */
  friend struct Domain;
  /** 
   * Constructor 
   * 
   * @param p        Parent
   * @param fullname Full object name 
   * @param d        Domain 
   */  
  Top(TGCompositeFrame& p, const char* fullname, 
      Int_t layoutOpts=kLHintsExpandX|kLHintsTop)
    : Object(p, fullname, layoutOpts)
  {}
  /**
   * Destructor
   * 
   */
  virtual ~Top() {}
  /**
   * Create a parameter dialog 
   *
   * @param action Action to create dialog for 
   * @param exec On completion of the dialog, this will contain true
   * if the action is to be executed.
   *
   * @return Created dialog 
   */
  virtual Action* CreateAction(SmiAction* action, bool& exec);
  /** Domain */
  ClassDef(Top,0)
};
#endif
//
// EOF
//
