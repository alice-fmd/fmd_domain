// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_SUPPORT_LOCK
#define FMD_DOMAIN_SUPPORT_LOCK
#include "BasicObject.h"
#include <TGButton.h>

struct BasicDomain;

//====================================================================
/**
 * A lock object
 * 
 */
struct Lock : public BasicObject 
{
  /** Domain is a fried */
  friend struct BasicDomain;
  /** 
   * Constructor 
   * 
   * @param p        Parent
   * @param fullname Full object name 
   * @param d        Domain 
   */  
  Lock(TGCompositeFrame& p, const char* fullname, BasicDomain& d);
  /**
   * Destructor
   * 
   */
  virtual ~Lock() {}
  /** 
   * Check if it is locked 
   * 
   * @return @c true if locked
   */
  bool IsLocked() const { return fState == "INLOCAL"; }
  /** 
   * Handle action click
   * 
   */
  void HandleClicked();
  /** 
   * Update display
   * 
   */
  void Update();
  /** 
   * Send action
   * 
   * @param action Action to send
   * 
   * @return return value
   */
  int SendAction(SmiAction* action);
  /** 
   * Release the lock
   * 
   */
  void Release();
  /** Whether we have the lock or not */
  Bool_t fOurLock;
  /** Locked picture */ 
  const TGPicture* fLocked;
  /** Unlocked picture */ 
  const TGPicture* fUnlocked;
  /** Picture button */ 
  TGPictureButton fLock;
  /** Domain */
  BasicDomain& fDomain;
  ClassDef(Lock,0)
};

#endif
//
// EOF
//
