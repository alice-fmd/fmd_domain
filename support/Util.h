#ifndef FMD_DCS_SUPPORT_UTIL
// -*- mode: C++ -*-
#define FMD_DCS_SUPPORT_UTIL
#include <string>
#include <algorithm>
#include <sstream>

//====================================================================
namespace {
  /**
   * Conver to lower case 
   */
  struct to_lower 
  {
    char operator()(char c) { return ::tolower(c); }
  };
  /** 
   * Convert to upper case 
   */
  struct to_upper 
  {
    char operator()(char c) { return ::toupper(c); }
  };
  std::string& upcase(std::string& s)
  {
    std::transform(s.begin(), s.end(), s.begin(), to_upper());
    return s;
  }
  std::string& downcase(std::string& s)
  {
    std::transform(s.begin(), s.end(), s.begin(), to_lower());
    return s;
  }

  template <typename T>
  void String2Value(const std::string& str, T& v) 
  {
    std::stringstream s(str);
    s >> v;
  }
  template <> 
  void String2Value(const std::string& str, unsigned int& v)
  {
    std::stringstream s(str);
    if (str[0] == '0') { 
      if (str[1] == 'x' || str[1] == 'X') s << std::hex;
      else                                s << std::oct;
    }
    s >> v;
  }
    
  template <typename T>
  void Value2String(const T& v, std::string& s)
  {
    std::stringstream str;
    str << v;
    s = str.str();
  }
  
}
#endif
