#include "BasicObject.h"
#include "Util.h"
#include "Action.h"
#include <TGButton.h>
#include <TGMenu.h>
#include <TGMsgBox.h>

//____________________________________________________________________
BasicObject::BasicObject(TGCompositeFrame& p, const char* fullname, 
			 Int_t layoutOpts)
  : TGHorizontalFrame(&p), 
    SmiObject(const_cast<char*>(fullname)), 
    fName(ObjectName(fullname)),
    fState("UNKNOWN"),
    fLabel(this, fName.c_str(), TGLabel::GetDefaultGC().GetGC(), 
	   TGLabel::GetDefaultFontStruct(), kFixedHeight),
    fButton(0),
    fMenu(0) 
{
  p.AddFrame(this, new TGLayoutHints(layoutOpts, 3, 3, 1, 1));
  AddFrame(&fLabel, new TGLayoutHints(kLHintsLeft, 3, 1));
  // smiStateChangeHandler();
  // Layout();
  fLabel.SetHeight(20);
  fLabel.SetMinWidth(150);
  fLabel.SetWidth(150);

  // ListParamsAttrs();
}
//____________________________________________________________________
void
BasicObject::SetButton(TGButton& b, Int_t w)
{
  fButton = &b;
  fButton->SetWidth(w);
  fButton->SetMinWidth(w);
  fButton->SetMaxWidth(UInt_t(w*1.5));
  AddFrame(fButton, new TGLayoutHints(kLHintsRight, 1, 3));
  fButton->Connect("Clicked()", "BasicObject", this, "HandleClicked()");
  fButton->SetToolTipText(Form("Perform actions on %s", getName()));
}
//____________________________________________________________________
const char* 
BasicObject::ObjectName(const char* fullname) const
{
  static std::string name;
  name = fullname;
  size_t colon = name.find("::");
  if (colon != std::string::npos) 
    name = name.substr(colon+2, name.size()-colon-2);
  return name.c_str();
}
//____________________________________________________________________
void
BasicObject::smiStateChangeHandler()
{
  // TTimer::SingleShot(11, "Object", this, "Update()");
  fState = getState();
  upcase(fState);
  Update();
}
//____________________________________________________________________
Pixel_t
BasicObject::StateColor(const std::string& state) const 
{
  static const Pixel_t kRed    = 0xff0000;
  static const Pixel_t kOrange = 0xFFA500;
  static const Pixel_t kYellow = 0xEFE856;
  static const Pixel_t kGreen  = 0x66CDAA;
  static const Pixel_t kBlue   = 0x41BCDB;
  static const Pixel_t kGray   = 0xAFAFAF;
    
  Pixel_t color = kGray;
  if      (state == "BEAM_TUNING")		color = kBlue;
  else if (state == "CLEARING")		color = kYellow;
  else if (state == "CONFIG_FEE")		color = kYellow;
  else if (state == "CONFIG_PED")		color = kYellow;
  else if (state == "CONFIGURING")		color = kYellow;
  else if (state == "DONE")			color = kGreen;
  else if (state == "DOWNLOADING")		color = kYellow;
  else if (state == "EOR_FAILURE")		color = kOrange;
  else if (state == "EOR_PROGRESSING")	color = kYellow;
  else if (state == "ERROR")			color = kRed;
  else if (state == "EXCLUDED")		color = kOrange;
  else if (state == "IDLE")			color = kBlue;
  else if (state == "IGNORED")		color = kOrange;
  else if (state == "INCLUDED")		color = kOrange;
  else if (state == "INLOCAL")		color = kBlue;
  else if (state == "INMANUAL")		color = kOrange;
  else if (state == "MANUAL")			color = kOrange;
  else if (state == "MIXED")			color = kYellow;
  else if (state == "MOVING_BEAM_TUN")	color = kYellow;
  else if (state == "MOVING_OFF")		color = kYellow;
  else if (state == "MOVING_READY")		color = kYellow;
  else if (state == "MOVING_STANDBY")		color = kYellow;
  else if (state == "MOVING_STBY_CONF")	color = kYellow;
  else if (state == "NOMONITOR")		color = kOrange;
  else if (state == "OFF")			color = kOrange;
  else if (state == "ON")			color = kGreen;
  else if (state == "READY")			color = kGreen;
  else if (state == "RECOVERY")		color = kOrange;
  else if (state == "RESETING")		color = kYellow;
  else if (state == "RUN_INHIBIT")		color = kOrange;
  else if (state == "RUNNING")		color = kGreen;
  else if (state == "RUN_OK")			color = kGreen;
  else if (state == "SAFETY_FEE")		color = kYellow;
  else if (state == "SOR_FAILURE")		color = kOrange;
  else if (state == "SOR_PROGRESSING")	color = kYellow;
  else if (state == "STANDBY")		color = kBlue;
  else if (state == "STBY_CONFIGURED")	color = kBlue;
  else if (state == "TIMEOUT")		color = kOrange;
  else if (state == "TRUE")			color = kGreen;
  else if (state == "WAIT_FOR_ON")		color = kYellow;
  else if (state == "WAIT_FOR_OFF")		color = kYellow;
  return color;
}
//____________________________________________________________________
void 
BasicObject::HandleClicked()
{
  if (!fButton) return;
  fButton->SetState(kButtonDown);	

  if (fMenu) delete fMenu;
  fMenu = 0;
  if (getNActions() <= 0) { 
    fButton->SetState(kButtonUp);
    return;
  }
    
  fMenu = new TGPopupMenu(gClient->GetDefaultRoot());
  SmiAction* action  = 0;
  Int_t i = 0;
  while ((action = getNextAction())) { 
    std::string actName = action->getName();
    if  (actName == "EOR"             || 
	 actName == "SOR"             || 
	 actName == "ACK_RUN_FAILURE" || 
	 actName == "INHIBIT_RUN"     || 
	 actName == "ALLOW_RUN") continue;
    fMenu->AddEntry(Form("%s%s", action->getName(), 
			 action->getNParams() ? " ..." : ""), i);
    i++;
  }
  if (i == 0) 
    fMenu->AddEntry("No available actions", 0);

  Int_t    topx = 0, topy = 0;
  Window_t wdummy;
  Int_t    id     = fButton->GetId();
  gVirtualX->TranslateCoordinates(id, (fMenu->GetParent())->GetId(),
				  0, 0, topx, topy, wdummy);

  fMenu->PlaceMenu(topx,topy+fButton->GetHeight(),kFALSE,kTRUE);
  if (i != 0)
    fMenu->Connect("Activated(Int_t)", "BasicObject", 
		   this, "HandleAction(Int_t)");
  fMenu->Connect("PoppedDown()", "BasicObject", 
		 this, "HandleDown()");
}
//____________________________________________________________________
void 
BasicObject::HandleDown() 
{ 
  if (fButton) fButton->SetState(kButtonUp); 
}
//____________________________________________________________________
Action*
BasicObject::CreateAction(SmiAction* action, bool& exec)
{
  return new Action(this, Name().c_str(), action, exec);
  // return a;
}

//____________________________________________________________________
void 
BasicObject::HandleAction(Int_t id)
{
  if (!fButton) return;
  fButton->SetState(kButtonUp);
  fMenu->UnmapWindow();
  if (getNActions() <= 0) return;
  SmiAction* action  = 0;
  Int_t i = 0;
  while ((action = getNextAction())) { if (id == i) break; i++; }
  if (!action) { 
    std::cerr << "No action chosen" << std::endl;
    return;
  }
  bool exec = true;
  if (action->getNParams()) { 
    /* std::string aName = action->getName();
       ActionMap::iterator i = fActions.find(aName); 
       Action* a;
       if (i == fActions.end()) { 
       a = new Action(this, Name().c_str(), action);
       fActions[aName] = a;
       }
       else 
       a = i->second; */
      
    Action* a = CreateAction(action, exec);
    a->Show();

    std::cout << "Action " << action->getName() << " " 
	      << (exec ? "executed" : "cancelled") << std::endl;
    // exec = a->Show();
  }
  if (!exec) return;
    
  SendAction(action);
  gClient->NeedRedraw(this, kTRUE);
}
//____________________________________________________________________
void 
BasicObject::Enable(Bool_t on) 
{ 
  if (fButton) fButton->SetEnabled(on); 
}
//____________________________________________________________________
void 
BasicObject::ListParamsAttrs() 
{
  std::cout << fName << " parameters  (" << getNParams() << "): " 
	    << std::endl;
  SmiParam* param = getFirstParam();
  do {
    if (!param) break;
    std::cout << "\t" << param->getName() << " ";
    switch (param->getType()) {
    case SMI_STRING: 
      std::cout << "(string):\t" << param->getValueString();
      break;
    case SMI_INTEGER: 
      std::cout << "(integer):\t" << param->getValueInt();
      break;
    case SMI_FLOAT: 
      std::cout << "(real):\t" << param->getValueFloat();
      break;
    default:
      std::cout << "(?):\t" << param->getEscValueString();
      break;
    }
    std::cout << std::endl;
  }  while ((param = getNextParam()));
  std::cout << fName << " attributes: " << std::endl;
  SmiAttribute* attr  = 0;
  while ((attr = getNextAttribute())) {
    std::cout << "\t" << attr->getName() << std::endl;
  }
}
//____________________________________________________________________
int 
BasicObject::SendAction(SmiAction* action)
{
  int ret = action->send();
  if (!ret) 
    new TGMsgBox(gClient->GetRoot(), this, "Action error", 
		 Form("Failed to execute action %s on object %s", 
		      action->getName(), Name().c_str()), kMBIconStop);
  return ret;
}

//____________________________________________________________________
BasicObject::BasicObject() 
  : SmiObject(const_cast<char*>("bla")) 
{}

//____________________________________________________________________
void 
BasicObject::ParseName(const std::string& full, 
		       std::string& domain, 
		       std::string& object)
{
  object       = full;
  size_t colon = std::string::npos;
  do { 
    colon     = object.find("::");
    if (colon == std::string::npos) break;

    domain = object.substr(0, colon);
    object = object.substr(colon+2);
  } while (colon != std::string::npos);
}

//
// EOF
//
