// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_SUPPORT_BASICCONFIGURE
#define FMD_DOMAIN_SUPPORT_BASICCONFIGURE
#include "Action.h"
#include <map>
class TGComboBox;
class TGNumberEntry;
class TGCheckButton;

//====================================================================
/**
 * An Object action
 * 
 */
struct Configure : public Action
{
  /** 
   * Constructor
   * 
   * @param p      Parent frame
   * @param obj    Object 
   * @param action Action 
   * @param exec   Whether to execute or not
   */
  Configure(const TGWindow* p, const char* obj, 
	    SmiAction* action, bool& exec);
  /**
   * Destructor
   * 
   */
  virtual ~Configure()  {}
  virtual void SetupParams();
  virtual void SetParameters();

  typedef std::map<std::string,std::string> PartMap;

  void ParseArgs(const std::string& args, PartMap& m);
  const std::string& GetPart(const std::string& name, 
			     const PartMap&     m, 
			     const std::string& def) const;
  TGCheckButton* AddOption(TGButtonGroup* frame, 
			   const PartMap&     m, 
			   const std::string& opt) const;
  TGComboBox* AddSelect(const char*       label,
			const PartMap&    parts, 
			const char**      values, 
			const int*        ids);
  TGNumberEntry* AddNumber(const char*       label, 
			   const PartMap&    map, 
			   unsigned short    least, 
			   unsigned short    largest,
			   unsigned short    def);
  void CheckOption(TGCheckButton* b, 
		   std::stringstream& s);
  void CheckNumber(const char* name, 
		   TGNumberEntry* b, 
		   std::stringstream& s);

  TGComboBox*    fTag;
  TGButtonGroup* fCards;
  TGCheckButton* fFec00;
  TGCheckButton* fFec01;
  TGCheckButton* fFec10;
  TGCheckButton* fFec11;
  TGComboBox*    fOver;
  TGCheckButton* fMonitor;
  TGCheckButton* fInterrupt;
  TGCheckButton* fSOD;
  TGCheckButton* fTrigger;
  TGCheckButton* fInterleaved;
  TGCheckButton* fRewrite;
  TGNumberEntry* fZero;
  TGNumberEntry* fNBuf;
  TGNumberEntry* fCalIter;
  TGNumberEntry* fCalStep;
  TGNumberEntry* fL1Window;
  TGNumberEntry* fL0Timeout;
  TGCheckButton* fT;
  TGCheckButton* fDC;
  TGCheckButton* fAC;
  TGCheckButton* fDV;
  TGCheckButton* fAV;
  TGNumberEntry* fMin;
  TGNumberEntry* fMax;
  
  ClassDef(Configure,0)
};
#endif
//
// EOF
//
