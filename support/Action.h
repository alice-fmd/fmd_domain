// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_SUPPORT_ACTION
#define FMD_DOMAIN_SUPPORT_ACTION

#include <TGFrame.h>
#include <TGButton.h>
#include <TObjArray.h>
#include <TGButtonGroup.h>
#include <sstream>
#include <map>

class TGTextEntry;
class SmiAction;
class SmiParam;

//====================================================================
/**
 * An Object action
 * 
 */
struct Action : public TGTransientFrame
{
  /**
   * Return value
   * 
   */
  enum { 
    kOk = 1,
    kCancel 
  };
  /** 
   * Constructor
   * 
   * @param p      Parent frame
   * @param obj    Object 
   * @param action Action 
   * @param exec   Whether to execute or not
   */
  Action(const TGWindow* p, const char* obj, 
	      SmiAction* action, bool& exec); 
  /**
   * Destructor
   * 
   */
  virtual ~Action()  { fChildren.Delete(); }
  virtual void Show();
  /** 
   * Show action dialog 
   * 
   * 
   * @return Wether to execute or not
   */
  bool IsShown() { return fExec; }
  /** 
   * Type identifier to string
   * 
   * @param id Type identifier
   * 
   * @return String represenation
   */
  static const char* TypeStr(int id);
  int AddLine(const char* label, TGCompositeFrame* line, TGFrame* entry);
  /** 
   * Make parameter inputs
   * 
   */
  virtual void SetupParams();
  /** 
   * Handle click on buttons
   * 
   * @param id Button clicked
   */
  virtual void HandleButtons(Int_t id);
  virtual void SetParameters();
  /** 
   * Close the window 
   * 
   */
  virtual void CloseWindow() { DeleteWindow(); }
  /** Map of parameters */
  typedef std::map<SmiParam*,TGTextEntry*> ParamMap;
  /** Action object */
  SmiAction*      fAction;
  /** Object name */
  std::string     fObj;
  /** Whether to execute or not */
  bool&           fExec;
  /** Layout hints */
  TGLayoutHints   fButtonsHints;
  /** Button group */
  TGHButtonGroup  fButtons;
  /** OK Button */
  TGTextButton    fOk;
  /** Cancel Button */
  TGTextButton    fCancel;
  /** Layout hints */
  TGLayoutHints   fFrameHints;
  /** Layout hints */
  TGLayoutHints   fLabelHints;
  /** Layout hints */
  TGLayoutHints   fEntryHints;
  /** Parameter map */
  ParamMap        fMap;
  /** Child frames */
  TObjArray       fChildren;
  ClassDef(Action,0)
};
#endif
//
// EOF
//
