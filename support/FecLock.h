// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_SUPPORT_FECLOCK
#define FMD_DOMAIN_SUPPORT_FECLOCK
#include "BasicObject.h"
#include <TGButton.h>

//====================================================================
/**
 * A lock object
 * 
 */
struct FecLock : public BasicObject 
{
  /** Domain is a fried */
  friend struct Domain;
  /** 
   * Constructor 
   * 
   * @param p        Parent
   * @param fullname Full object name 
   * @param d        Domain 
   */  
  FecLock(TGCompositeFrame& p, const char* fullname);
  /**
   * Destructor
   * 
   */
  virtual ~FecLock() {}
  /** 
   * Check if it is locked 
   * 
   * @return @c true if locked
   */
  bool IsLocked() const { return fState == "INCLUDED"; }
  /** 
   * Handle action click
   * 
   */
  void HandleClicked();
  /** 
   * Update display
   * 
   */
  void Update();
  /** Locked picture */ 
  const TGPicture* fLocked;
  /** Unlocked picture */ 
  const TGPicture* fUnlocked;
  /** Picture button */ 
  TGPictureButton fLock;
  /** Domain */
  ClassDef(FecLock,0)
};
#endif
//
// EOF
//
