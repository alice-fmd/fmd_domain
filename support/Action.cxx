#include "Action.h"
#include "Util.h"
#include <smixx/smiuirtl.hxx>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>

//____________________________________________________________________
Action::Action(const TGWindow* p, 
	       const char* obj, 
	       SmiAction* action, 
	       bool& exec) 
  : TGTransientFrame(gClient->GetRoot(), p, 150, 100), 
    fAction(action), 
    fObj(obj),
    fExec(exec),
    fButtonsHints(kLHintsExpandX|kLHintsBottom,3,3,0,3),
    fButtons(this),
    fOk(&fButtons, "OK",     kOk),
    fCancel(&fButtons, "Cancel", kCancel),
    fFrameHints(kLHintsExpandX,5,5,2,1),
    fLabelHints(kLHintsLeft,0,3),
    fEntryHints(kLHintsRight,3,0) 
{
  AddFrame(&fButtons,&fButtonsHints);
  fButtons.SetLayoutHints(&fButtonsHints);
  fButtons.Connect("Clicked(Int_t)", "Action", this, "HandleButtons(Int_t)");
  
  fExec = false;
}

//____________________________________________________________________
void
Action::Show()
{
  fChildren.SetOwner(true);
  if (fAction->getNParams() > 0) SetupParams();
  
  SetWindowName(Form("Parameters for %s to %s",
		     fAction->getName(),fObj.c_str()));
  MapSubwindows();
  Resize(GetDefaultSize());
  MapRaised();
  // gClient->WaitForUnmap(this);
  gClient->WaitFor(this);
}

//____________________________________________________________________
const char* 
Action::TypeStr(int id) 
{
  switch (id) { 
  case SMI_INTEGER: return "integer";
  case SMI_FLOAT:   return "floating point";
  case SMI_STRING:  return "string";
  }
  return "string";
}

//____________________________________________________________________
int
Action::AddLine(const char* lbl, TGCompositeFrame* line, TGFrame* entry)
{
  TGLabel* label = 0;
  if (lbl != 0 && lbl[0] != '\0') label = new TGLabel(line, lbl);

  AddFrame(line, &fFrameHints);
  if (label) line->AddFrame(label, &fLabelHints);
  line->AddFrame(entry, (label ? &fEntryHints : &fFrameHints));
  fChildren.Add(entry);
  if (label) fChildren.Add(label);
  fChildren.Add(line);
  return (label ? label->GetWidth() : 0);
}
//____________________________________________________________________
void 
Action::SetupParams()
{
  SmiParam* param = fAction->getFirstParam();
  std::vector<TGLabel*>     labels;
  Int_t maxL = 0;
  Int_t maxE = 0;
  do {
    TGHorizontalFrame* frame = new TGHorizontalFrame(this);
    TGTextEntry*       value = 0;
    TGFrame*           entry = 0;

    switch (param->getType()) { 
    case SMI_INTEGER: {
      TGNumberEntry* e = new TGNumberEntry(frame, param->getValueInt());
      e->SetFormat(TGNumberEntry::kNESInteger);
      e->SetWidth(100);
      value      = e->GetNumberEntry();
      entry      = e;
      break;
    }
    case SMI_FLOAT: {
      TGNumberEntry* e = new TGNumberEntry(frame, param->getValueInt());
      e->SetFormat(TGNumberEntry::kNESReal);
      e->SetWidth(100);
      value      = e->GetNumberEntry();
      entry      = e;
      break;
    }
    case SMI_STRING: 
      value = new TGTextEntry(frame,param->getValueString());
      value->SetWidth(100);
      entry = value;
      break;
    }
    Int_t l = AddLine(param->getName(), frame, entry);
    
    fMap[param] = value;
    maxL = TMath::Max(maxL, l);
    // entry->Connect("TabPressed()", "Action", this, "HandleTab()");
    // entry->Connect("ReturnPressed()", "Action", this, "HandleTab()");
  } while ((param = fAction->getNextParam()));

  for (std::vector<TGLabel*>::iterator i = labels.begin(); 
       i != labels.end(); i++) (*i)->SetWidth(maxL);
}

//____________________________________________________________________
void 
Action::HandleButtons(Int_t id)
{
  if (id == kCancel) { 
    DeleteWindow();
    return;
  }
  SetParameters();
  // while ((param = fAction->getNextParam()));
  fExec = true;
  // UnmapWindow();
  DeleteWindow();
}
//____________________________________________________________________
void
Action::SetParameters()
{
  // SmiParam* param = fAction->getFirstParam();
  // do {
  for (ParamMap::iterator i = fMap.begin(); i != fMap.end(); ++i) {
    // TGTextEntry* entry = fMap[param];
    SmiParam*    param = i->first;
    TGTextEntry* entry = i->second;
    if (!entry) continue;
    switch (param->getType()) { 
    case SMI_INTEGER: { 
      int val = 0;
      String2Value(entry->GetText(), val);
      fAction->setParam(param->getName(), val);
    } break;
    case SMI_FLOAT: { 
      double val = 0;
      String2Value(entry->GetText(), val);
      fAction->setParam(param->getName(), val);
    } break;
    case SMI_STRING: 
    default:
      { 
	std::string str = entry->GetText();
	param->setValue(const_cast<char*>(str.c_str()));
	//fAction->setParam(param->getName(),const_cast<char*>(str.c_str()));
      }
      break;
    }
  }  
}

// 
// EOF
//
