#include "Lock.h"
#include "BasicDomain.h"
#include <TGPicture.h>
#include "locked.xpm"
#include "unlocked.xpm"

//____________________________________________________________________
Lock::Lock(TGCompositeFrame& p, const char* fullname, BasicDomain& d)
  : BasicObject(p, fullname, kLHintsTop), 
    fOurLock(kFALSE), 
    fLocked(gClient->GetPicturePool()
	    ->GetPicture("locked.xpm", 
			 const_cast<char**>(locked_xpm))),
    fUnlocked(gClient->GetPicturePool()
	      ->GetPicture("unlocked.xpm", 
			   const_cast<char**>(unlocked_xpm))),
  fLock(this, fUnlocked, -1, 
	TGButton::GetDefaultGC().GetGC(),
	kFixedWidth),
    fDomain(d)
{
  fLock.SetHeight(30);
  SetButton(fLock, 30);
  RemoveFrame(&fLabel);
}
//____________________________________________________________________
void 
Lock::HandleClicked()
{
  if (IsLocked() && !fOurLock) return;
  BasicObject::HandleClicked();
}
//____________________________________________________________________
int 
Lock::SendAction(SmiAction* action)
{
  if (IsLocked() && !fOurLock) return 1;
  std::string n(action->getName());
  fOurLock = (n == "TAKE" || n == "GO_TO_INLOCAL");
  int ret = BasicObject::SendAction(action);
  if (!ret) fOurLock = kFALSE;
  return ret;
}
//____________________________________________________________________
void 
Lock::Release()
{
  if (!IsLocked() || !fOurLock) return;
  SmiAction* action  = 0;
  while ((action = getNextAction())) { 
    std::string n(action->getName());
    std::cout << "Possible release action: '" << n << "'" << std::endl;
    if (n == "RETURN" || n == "GO_TO_EXCLUDED" || n == "RELEASE")
      BasicObject::SendAction(action);
  }    
}

//____________________________________________________________________
void 
Lock::Update()
{
  // std::cout << "State of lock is changed : " << State()
  //           << " ours: " << (fOurLock ? "yes" : "no") << std::endl;
  bool locked = IsLocked();
  fLock.SetPicture(locked ? fLocked : fUnlocked);
  fLock.ChangeBackground(locked ? (fOurLock ? 0x00dd00 : 0xff0000):0xafafaf);
  fDomain.Enable(locked && fOurLock);
  if (!locked) Enable(kTRUE);
  // gClient->NeedRedraw(&fLock, kTRUE);
  // gClient->NeedRedraw(this, kTRUE);
  // gClient->NeedRedraw(const_cast<TGWindow*>(fParent), kTRUE);
  // gClient->NeedRedraw(const_cast<TGWindow*>(fParent->GetParent()), kTRUE);
}


//
// EOF
//
