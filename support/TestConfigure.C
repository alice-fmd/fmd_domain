#include "Configure.h"
#include <TGFrame.h>
#include <TGButton.h>
#include <TApplication.h>
#ifndef __CINT__
# include <smixx/smiuirtl.hxx>
#endif

struct Main : public TGMainFrame
{
  Main()
    : TGMainFrame(gClient->GetRoot(), 400, 400),
      fButton(this, "Configure...")
  {
    AddFrame(&fButton);
    fButton.Connect("Clicked()", "Main", this, "Handle()");
    MapSubwindows();
    Layout();
    Resize(600, 400);
    MapWindow();
  }
  virtual ~Main() {}
  void Handle()
  {
    SmiAction* aa = new SmiAction("CONFIGURE", 1);
    SmiParam*  pp = new SmiParam("ARGS");
    pp->setValue("physics;0x10000;monitor;interrupt;sod;trigger;nbuf=1;l1window=212;l0timeout=220;zero=1");
    aa->setParam(pp);
    bool exec = true;
    Action* a = new Configure(this, "Foo", aa, exec);
    a->Show();
  }
  TGTextButton fButton;
  ClassDef(Main,0);
};

#if !defined(__CINT__) && !defined(G__DICTIONARY)
int
main()
{
  TApplication app("app", 0, 0);
  Main m;

  app.Run();
  
  return 0;
}
#endif 
//
// EOF
//


