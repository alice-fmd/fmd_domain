#include "Object.h"
#include "Util.h"

Object::Object(TGCompositeFrame& p, const char* fullname, Int_t bw,
	       Int_t layoutOpts)
  : BasicObject(p,fullname,layoutOpts), 
    fState(this, "UNKNOWN", TGLabel::GetDefaultGC().GetGC(), 
	   TGLabel::GetDefaultFontStruct(), 
	   kFixedWidth|kSunkenFrame|kFixedHeight),
    fActionButton(this, gClient->GetPicture("arrow_down.xpm"))
#if 0
    fActionButton(this, "UNKNOWN", -1, 
		  TGButton::GetDefaultGC().GetGC(),
		  TGTextButton::GetDefaultFontStruct(), 
		  kFixedWidth)
#endif
{
  // fActionButton.SetWidth(200);
  fState.SetWidth(bw);
  fState.SetHeight(20);
  fActionButton.SetHeight(20);
  SetButton(fActionButton, 20/*bw*/);
  AddFrame(&fState, new TGLayoutHints(kLHintsRight,3,1));
}
void 
Object::Update()
{
  char* s = getState();
    std::string state("UNKNOWN");
    if (s) state = s;
    upcase(state);
    // fActionButton.SetText(state.c_str());
    // fActionButton.ChangeBackground(StateColor(state));
    fState.SetText(state.c_str());
    fState.ChangeBackground(StateColor(state));
    gClient->NeedRedraw(&fState, kTRUE);
    // gClient->NeedRedraw(this, kTRUE);
    // gClient->NeedRedraw(const_cast<TGWindow*>(fParent), kTRUE);
    // gClient->NeedRedraw(const_cast<TGWindow*>(fParent->GetParent()), kTRUE);
  }
//
// EOF
//
