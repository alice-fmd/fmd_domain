/** 
    gSystem->SetIncludePath("-I. -I/usr/include/dim");
    gSystem->SetLinkedLibs(Form("%s -lsmiui",gSystem->GetLinkedLibs()))
    .x FMD_cycle.C+
*/
#include "BasicDomain.h"
#include "Object.h"
#include "Lock.h"
#include <TApplication.h>
# include <dim/dic.hxx>

class Domain  : public BasicDomain
{
public:
  Domain(const char* name)  
    : BasicDomain(name),
      fTop(this, 600, 50, kFixedWidth|kChildFrame),
      fParts(this, "Parts", kVerticalFrame), 
      fMisc(this, "Other", kVerticalFrame) 
  {
    AddFrame(&fMenuBar, new TGLayoutHints(kLHintsExpandX));
    AddFrame(&fTop,     new TGLayoutHints(kLHintsExpandX,3,3,3,3));
    AddFrame(&fParts,   new TGLayoutHints(kLHintsExpandX,3,3,3,3));
    AddFrame(&fMisc,    new TGLayoutHints(kLHintsExpandX,3,3,3,3));
  }
  BasicObject* MakeObject(const std::string& full)
  {
    std::string domain = Name();
    
    TGCompositeFrame* frame = 0;

    // std::cout << "Got object " << obj->getName() << std::endl;
    // std::cout << "Got " << name << " ... " << std::flush;
    std::string objDomain = domain;
    std::string objName   = full;
    BasicObject::ParseName(full, objDomain, objName);

    int bw = 200;
    if (objDomain != domain) 
      frame = &fMisc;
    else { 
      if      (objName == "CYCLE_CTRL")   frame = &fTop;
      else                                frame = &fParts;
    }
    if (!frame) return 0;
      
    BasicObject* o = 0;
    if (objName.find("_FWM") != std::string::npos) 
      o = fLock = new Lock(*frame, full.c_str(), *this);
    else if (objName != "&ALLOC") 
      o = new Object(*frame, full.c_str(), bw);
    return o;
  }
  /** Top frame */
  TGHorizontalFrame fTop;
  /** Container */
  TGGroupFrame      fParts;
  /** Container */
  TGGroupFrame      fMisc;
};



//====================================================================
void
FMD_cycle(const char* dns, const char* domain)
{
  DimClient::setDnsNode((dns ? dns : "localhost"));
  Domain* d = new Domain(domain ? domain : "FMD_CYCLE");
  // d->Update();
  d->wakeUp();
  // std::cout << "Domain identifier: " << d->GetId() << std::endl;
  sleep(1);
}

//====================================================================
#ifndef __CINT__
#include <stdexcept>
int
main(int argc, char** argv)
{
  try { 
    TApplication app("app", 0, 0);
    FMD_cycle((argc > 1 ? argv[1] : 0),
	      (argc > 2 ? argv[2] : 0));
    app.Run();
  }
  catch (std::exception& e) { 
    std::cerr << "Error: " << e.what() << std::endl;
    return 1;
  }
  return 0;
}
#endif 
//====================================================================
//
// EOF
//
