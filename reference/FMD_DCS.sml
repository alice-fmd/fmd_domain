class: FMD_PW_A3486TOP_FmdA3486Module_CLASS
!panel: FMD_Panels/FmdA3486Module.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state {NO_CONTROL,OFF,ON} ) )  move_to ERROR
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON ) )  move_to ON
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state {NO_CONTROL,OFF,ON} ) )  move_to ERROR
        when (  ( ( all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state NO_CONTROL ) ) and
       ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when ( ( all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON ) )  move_to ON
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        action: TURN_ON	!visible: 1
            do SWITCH_ON all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETACTIONS
            move_to TURNING_ON
    state: TURNING_ON	!color: FwStateAttention1
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state {NO_CONTROL,OFF,ON} ) )  move_to ERROR
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON ) )  move_to ON
    state: ON	!color: FwStateOKPhysics
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state {NO_CONTROL,ON} ) )  move_to ERROR
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        action: TURN_OFF	!visible: 1
            do SWITCH_OFF all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETACTIONS
            move_to TURNING_OFF
    state: TURNING_OFF	!color: FwStateAttention1
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state {NO_CONTROL,ON,OFF} ) )  move_to ERROR
        when ( ( any_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF ) )  move_to OFF
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to MIXED

object: FMD_PW_A3486 is_of_class FMD_PW_A3486TOP_FmdA3486Module_CLASS

class: FMD_PW_A3486FwCaenChannelDcsEasyPWS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES
            remove &VAL_OF_Device from FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES
            insert &VAL_OF_Device in FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETACTIONS
            move_to READY

object: FMD_PW_A3486FwCaenChannelDcsEasyPWS_FWDM is_of_class FMD_PW_A3486FwCaenChannelDcsEasyPWS_FwDevMode_CLASS


class: FMD_PW_A3486FwCaenChannelDcsEasyPWS_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenEasyPowerSupply.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON	!visible: 1
        action: RESET	!visible: 1
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: ER_REPAIR	!color: FwStateAttention2
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: PWS_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
        action: KILL	!visible: 1

object: FMD_DCS:FMD_RACK_O13:FMD_A3486:LvChannel000 is_of_class FMD_PW_A3486FwCaenChannelDcsEasyPWS_CLASS

objectset: FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETSTATES {FMD_DCS:FMD_RACK_O13:FMD_A3486:LvChannel000 }
objectset: FMD_PW_A3486FWCAENCHANNELDCSEASYPWS_FWSETACTIONS {FMD_DCS:FMD_RACK_O13:FMD_A3486:LvChannel000 }

class: FMD_INFRASTRUCTURETOP_FMD_INFRASTRUCTURE_CLASS
!panel: FMD_INFRASTRUCTURE.pnl
    parameters: int trip_reset_num = 0
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,OFF,ON} ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state {NO_CONTROL,OFF} ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state {NO_CONTROL,OFF} ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state MIXED ) )  move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state {OFF,ON,NO_CONTROL} ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  )  move_to ERROR
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        action: RESET	!visible: 1
            move_to SENDING_RESET
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state {OFF,ON,NO_CONTROL} ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  )  move_to ERROR
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to NO_CONTROL
        action: GO_READY	!visible: 1
            do SWITCH_ON all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ENABLING_RAMPDOWN
        action: RESET	!visible: 1
            move_to SENDING_RESET
    state: ENABLING_RAMPDOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  do ENABLE_LV
        action: ENABLE_LV	!visible: 0
            do TURN_ON all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS
            move_to ENABLING_LV
    state: ENABLING_LV	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state {OFF,TURNING_ON,ON} ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state ON ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        action: GO_OFF	!visible: 1
            do TURN_OFF all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS
            move_to DISABLING_LV
        action: RESET	!visible: 1
            move_to SENDING_RESET
    state: DISABLING_LV	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state {ON,TURNING_OFF,OFF} ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state OFF ) )  do DISABLE_RAMPDOWN
        action: DISABLE_RAMPDOWN	!visible: 0
            do SWITCH_OFF all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to DISABLING_RAMPDOWN
    state: DISABLING_RAMPDOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  move_to OFF
    state: SENDING_RESET	!color: FwStateAttention1
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state {NO_CONTROL,CLEARING,KILLING} ) )  do RESET_TRIPS
        when ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,CLEARING,KILLING} ) )  move_to RESETTING
        action: RESET_TRIPS	!visible: 0
            do RESET all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETACTIONS
            move_to SENDING_TRIP_RESET1
    state: SENDING_TRIP_RESET1	!color: FwStateAttention1
        when ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,KILLING} ) )  move_to RESETTING
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state CLEARING ) )  move_to RESETTING_TRIPS1
    state: RESETTING_TRIPS1	!color: FwStateAttention1
        when ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,KILLING} ) )  move_to RESETTING
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state CLEARING ) )  do RESET_TRIPS
        action: RESET_TRIPS	!visible: 1
            do RESET all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETACTIONS
            move_to SENDING_TRIP_RESET2
    state: SENDING_TRIP_RESET2	!color: FwStateAttention1
        when ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,KILLING} ) )  move_to RESETTING
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state CLEARING ) )  move_to RESETTING_TRIPS2
    state: RESETTING_TRIPS2	!color: FwStateAttention1
        when ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,KILLING} ) )  move_to RESETTING
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state CLEARING ) )  move_to RESETTING
    state: RESETTING	!color: FwStateAttention1
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state {NO_CONTROL,OFF,ON} ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state {NO_CONTROL,OFF} ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state {NO_CONTROL,OFF} ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state ON ) ) and
       ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state ERROR ) )  do RESET_A3486
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state ON ) ) and
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state {NO_CONTROL,OFF} ) )  )  do RECOVER_A3486
        when (  ( ( all_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) ) and
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state {NO_CONTROL,OFF} ) ) and
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  do RECOVER_RAMPDOWN
        when ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state {MIXED,NO_CONTROL,OFF,TURNING_ON,ON,TURNING_OFF,ERROR} ) )  move_to ERROR
        action: RESET_A3486	!visible: 0
            do RESET all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS
            move_to RESETTING_A3486
        action: RECOVER_A3486	!visible: 0
            do TURN_OFF all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS
            move_to RECOVERING_A3486
        action: RECOVER_RAMPDOWN	!visible: 0
            do SWITCH_OFF all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RECOVERING_RAMPDOWN
    state: RESETTING_A3486	!color: FwStateAttention1
        when ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state {MIXED,NO_CONTROL,OFF,ON} ) )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state MIXED ) )  move_to RESETTING
    state: RECOVERING_A3486	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES not_in_state {NO_CONTROL,ON,TURNING_OFF,OFF} ) )  )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES in_state {NO_CONTROL,OFF} ) )  move_to RESETTING
    state: RECOVERING_RAMPDOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES not_in_state ON ) ) or
       ( ( any_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {NO_CONTROL,READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( all_in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES in_state {NO_CONTROL,OFF} ) )  move_to RESETTING
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to SENDING_RESET

object: FMD_INFRASTRUCTURE is_of_class FMD_INFRASTRUCTURETOP_FMD_INFRASTRUCTURE_CLASS

class: FMD_INFRASTRUCTUREFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD_INFRASTRUCTUREFwCaenChannelDcsHV_FWDM is_of_class FMD_INFRASTRUCTUREFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD_INFRASTRUCTUREFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD_RACK_CR4:HvRampDownChannel is_of_class FMD_INFRASTRUCTUREFwCaenChannelDcsHV_CLASS

objectset: FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD_RACK_CR4:HvRampDownChannel }
objectset: FMD_INFRASTRUCTUREFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD_RACK_CR4:HvRampDownChannel }

class: FMD_INFRASTRUCTUREFwCaenCrateSY1527Dcs_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES
            remove &VAL_OF_Device from FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES
            insert &VAL_OF_Device in FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETACTIONS
            move_to READY

object: FMD_INFRASTRUCTUREFwCaenCrateSY1527Dcs_FWDM is_of_class FMD_INFRASTRUCTUREFwCaenCrateSY1527Dcs_FwDevMode_CLASS


class: FMD_INFRASTRUCTUREFwCaenCrateSY1527Dcs_CLASS/associated
!panel: FwCaenCrateSY1527|dcsCaen\dcsCaenCrateSY1527.pnl
    state: OFF	!color: _3DFace
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ON	!color: FwStateOKPhysics
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: PWS_FAULT	!color: FwStateAttention3
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL	!visible: 1
        action: RESET	!visible: 1
        action: ACKNOLEDGE	!visible: 1
    state: CLEARING	!color: FwStateAttention1
    state: KILLING	!color: FwStateAttention1

object: FMD_DCS:FMD_RACK_CR4:FMD_PWR_SUPPLY_MAINFRAME is_of_class FMD_INFRASTRUCTUREFwCaenCrateSY1527Dcs_CLASS

objectset: FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETSTATES {FMD_DCS:FMD_RACK_CR4:FMD_PWR_SUPPLY_MAINFRAME }
objectset: FMD_INFRASTRUCTUREFWCAENCRATESY1527DCS_FWSETACTIONS {FMD_DCS:FMD_RACK_CR4:FMD_PWR_SUPPLY_MAINFRAME }

class: FMD_INFRASTRUCTUREFmdA3486Module_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES
            remove &VAL_OF_Device from FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES
            insert &VAL_OF_Device in FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS
            move_to READY

object: FMD_INFRASTRUCTUREFmdA3486Module_FWDM is_of_class FMD_INFRASTRUCTUREFmdA3486Module_FwDevMode_CLASS


objectset: FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETSTATES {FMD_PW_A3486 }
objectset: FMD_INFRASTRUCTUREFMDA3486MODULE_FWSETACTIONS {FMD_PW_A3486 }

class: FMD1_PW_RCUTOP_FMD_RCU_CLASS
!panel: FMD_Panels/FMD_RCU.pnl
    parameters: string runtype = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to OFF
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  )  move_to STANDBY
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  )  move_to INVALID_CONF
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state {READY,RUNNING} ) )  )  move_to READY
        when (  not (  (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  ) or (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  ) or (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  and  ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  ) or (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  ) or (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state {READY,RUNNING} ) )  )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state NO_LINK ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state NO_LINK ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        action: GO_STANDBY	!visible: 1
            do GO_READY all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {NO_LINK,STARTUP,STANDBY} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to BOOTING
    state: BOOTING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {NO_LINK,STARTUP,STANDBY} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  move_to STANDBY
    state: STANDBY	!color: FwStateOKNotPhysics
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state STANDBY ) )  move_to ERROR
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to POWERING_DOWN
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(Tag = run_type) all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to DOWNLOADING
    state: POWERING_DOWN	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,NO_LINK} ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  )  do FORCE_NO_LINK
        when (  ( ( all_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to OFF
        action: FORCE_NO_LINK	!visible: 0
            do FORCE_NO_LINK all_in FMD1_PW_RCUDIMRCU_FWSETACTIONS
    state: DOWNLOADING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,CONFIGURING,READY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {IDLE,CONFIGURING,DONE,INVALID_CONF,SKIPPED} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        when ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  move_to INVALID_CONF
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CONFIGURING
    state: INVALID_CONF	!color: FwStateAttention2
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        action: CLEAR	!visible: 1
            do ACKNOWLEDGE all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CLEARING_INVALID
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do ACKNOWLEDGE all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to RESETTING
    state: CLEARING_INVALID	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {INVALID_CONF,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  do CLEAR
        action: CLEAR	!visible: 1
            do RESET all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to SET_CLEAR
    state: RESETTING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {INVALID_CONF,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  do CONFIGURE
        action: CONFIGURE	!visible: 1
            do CONFIGURE(Tag = runtype) all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to DOWNLOADING
    state: CONFIGURING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,CONFIGURING,READY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state READY ) )  )  do UPLOAD
        action: UPLOAD	!visible: 1
            do UPLOAD(runtype = runtype) all_in FMD1_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to INIT_ZERO_SUP
    state: INIT_ZERO_SUP	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state {IDLE,UPLOADING,DONE,SKIPPED} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD1_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to UPLOADING
    state: UPLOADING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES in_state IDLE ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {READY,RUNNING} ) )  move_to ERROR
        action: CLEAR	!visible: 1
            do RESET all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to SET_CLEAR
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(Tag = run_type) all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to DOWNLOADING
    state: SET_CLEAR	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {RUNNING,READY,STANDBY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {IDLE,CONFIGURING,DONE,SKIPPED} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CLEARING
    state: CLEARING	!color: FwStateAttention1
        when ( ( any_in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMRCU_FWSETSTATES not_in_state {RUNNING,READY,STANDBY} ) )  move_to ERROR
        when ( ( any_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) ) and
       ( ( all_in FMD1_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  )  move_to STANDBY
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to MIXED

object: FMD1_PW_RCU is_of_class FMD1_PW_RCUTOP_FMD_RCU_CLASS

class: FMD1_PW_RCUFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_PW_RCUFwCaenChannelDcsLV_FWDM is_of_class FMD1_PW_RCUFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_PW_RCUFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_RCU:LvChannel000 is_of_class FMD1_PW_RCUFwCaenChannelDcsLV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_RCU:LvChannel001 is_of_class FMD1_PW_RCUFwCaenChannelDcsLV_CLASS

objectset: FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_RCU:LvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_RCU:LvChannel001 }
objectset: FMD1_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_RCU:LvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_RCU:LvChannel001 }

class: FMD1_PW_RCUDIMRCU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_RCUDIMRCU_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_RCUDIMRCU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_RCUDIMRCU_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_RCUDIMRCU_FWSETACTIONS
            move_to READY

object: FMD1_PW_RCUDIMRCU_FWDM is_of_class FMD1_PW_RCUDIMRCU_FwDevMode_CLASS


class: FMD1_PW_RCUDIMRCU_CLASS/associated
!panel: FMD_Panels/FMD_DIMRCU.pnl
    parameters: int TimerStart = 0
    state: NO_LINK	!color: FwStateAttention2
        action: WAIT_FOR_STANDBY	!visible: 1
    state: STARTUP	!color: FwStateAttention1
        action: WAIT_FOR_STANDBY	!visible: 1
    state: STANDBY	!color: FwStateOKNotPhysics
        action: FORCE_NO_LINK	!visible: 1
        action: WAIT_FOR_READY	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: WAIT_FOR_READY	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: WAIT_FOR_STANDBY	!visible: 1
    state: MIXED	!color: FwStateAttention1
    state: ERROR	!color: FwStateAttention3
    state: RUNNING	!color: FwStateOKPhysics
    state: WAITING_FOR_STANDBY	!color: FwStateAttention1
    state: WAITING_FOR_READY	!color: FwStateAttention1
    state: UNKNOWN	!color: FwStateAttention3

object: RCU_1 is_of_class FMD1_PW_RCUDIMRCU_CLASS

objectset: FMD1_PW_RCUDIMRCU_FWSETSTATES {RCU_1 }
objectset: FMD1_PW_RCUDIMRCU_FWSETACTIONS {RCU_1 }

class: FMD1_PW_RCUDIMMiniconf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_RCUDIMMINICONF_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_RCUDIMMINICONF_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to READY

object: FMD1_PW_RCUDIMMiniconf_FWDM is_of_class FMD1_PW_RCUDIMMiniconf_FwDevMode_CLASS


class: FMD1_PW_RCUDIMMiniconf_CLASS/associated
!panel: FMD_Panels/FMD_Miniconf.pnl
    state: NO_LINK	!color: FwStateAttention2
    state: IDLE	!color: FwStateOKNotPhysics
        action: CONFIGURE(string Tag = "physics")	!visible: 1
        action: RESET	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: CLEAR	!visible: 1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
        action: CLEAR	!visible: 1
    state: INVALID_CONF	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: SKIPPED	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: CLEAR	!visible: 1
    state: UNKNOWN	!color: FwStateAttention3
        action: CLEAR	!visible: 1

object: Miniconf_1 is_of_class FMD1_PW_RCUDIMMiniconf_CLASS

objectset: FMD1_PW_RCUDIMMINICONF_FWSETSTATES {Miniconf_1 }
objectset: FMD1_PW_RCUDIMMINICONF_FWSETACTIONS {Miniconf_1 }

class: FMD1_PW_RCUDIMPedconf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_RCUDIMPEDCONF_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_RCUDIMPEDCONF_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to READY

object: FMD1_PW_RCUDIMPedconf_FWDM is_of_class FMD1_PW_RCUDIMPedconf_FwDevMode_CLASS


class: FMD1_PW_RCUDIMPedconf_CLASS/associated
!panel: FMD_Panels/FMD_Miniconf.pnl
    state: NO_LINK	!color: FwStateAttention2
    state: IDLE	!color: FwStateOKNotPhysics
        action: UPLOAD(string runtype = "")	!visible: 1
    state: UPLOADING	!color: FwStateAttention1
        action: CLEAR	!visible: 1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
        action: CLEAR	!visible: 1
    state: SKIPPED	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: CLEAR	!visible: 1
    state: UNKNOWN	!color: FwStateAttention3
        action: CLEAR	!visible: 1

object: Pedconf_1 is_of_class FMD1_PW_RCUDIMPedconf_CLASS

objectset: FMD1_PW_RCUDIMPEDCONF_FWSETSTATES {Pedconf_1 }
objectset: FMD1_PW_RCUDIMPEDCONF_FWSETACTIONS {Pedconf_1 }

class: FMD1_3V3_INNER_TOPTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_3V3_INNER_TOP is_of_class FMD1_3V3_INNER_TOPTOP_FmdFEE3V3_CLASS

class: FMD1_3V3_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_3V3_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD1_3V3_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_3V3_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel000 is_of_class FMD1_3V3_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel000 }
objectset: FMD1_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel000 }

class: FMD1_2V5_INNER_TOPTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_2V5_INNER_TOP is_of_class FMD1_2V5_INNER_TOPTOP_FmdFEE2V5_CLASS

class: FMD1_2V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_2V5_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD1_2V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_2V5_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel001 is_of_class FMD1_2V5_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel001 }
objectset: FMD1_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel001 }

class: FMD1_1V5_INNER_TOPTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_1V5_INNER_TOP is_of_class FMD1_1V5_INNER_TOPTOP_FmdFEE1V5_CLASS

class: FMD1_1V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_1V5_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD1_1V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_1V5_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel002 is_of_class FMD1_1V5_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel002 }
objectset: FMD1_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel002 }

class: FMD1_M2V_INNER_TOPTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_M2V_INNER_TOP is_of_class FMD1_M2V_INNER_TOPTOP_FmdFEEM2V_CLASS

class: FMD1_M2V_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_M2V_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD1_M2V_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_M2V_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel003 is_of_class FMD1_M2V_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel003 }
objectset: FMD1_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:LvChannel003 }

class: FMD1_PW_INNER_TOPTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD1_PW_INNER_TOP is_of_class FMD1_PW_INNER_TOPTOP_FmdSector_CLASS

class: FMD1_PW_INNER_TOPFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPFmdFEE3V3_FWDM is_of_class FMD1_PW_INNER_TOPFmdFEE3V3_FwDevMode_CLASS


objectset: FMD1_PW_INNER_TOPFMDFEE3V3_FWSETSTATES {FMD1_3V3_INNER_TOP }
objectset: FMD1_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS {FMD1_3V3_INNER_TOP }

class: FMD1_PW_INNER_TOPFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPFmdFEE2V5_FWDM is_of_class FMD1_PW_INNER_TOPFmdFEE2V5_FwDevMode_CLASS


objectset: FMD1_PW_INNER_TOPFMDFEE2V5_FWSETSTATES {FMD1_2V5_INNER_TOP }
objectset: FMD1_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS {FMD1_2V5_INNER_TOP }

class: FMD1_PW_INNER_TOPFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPFmdFEE1V5_FWDM is_of_class FMD1_PW_INNER_TOPFmdFEE1V5_FwDevMode_CLASS


objectset: FMD1_PW_INNER_TOPFMDFEE1V5_FWSETSTATES {FMD1_1V5_INNER_TOP }
objectset: FMD1_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS {FMD1_1V5_INNER_TOP }

class: FMD1_PW_INNER_TOPFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPFmdFEEM2V_FWDM is_of_class FMD1_PW_INNER_TOPFmdFEEM2V_FwDevMode_CLASS


objectset: FMD1_PW_INNER_TOPFMDFEEM2V_FWSETSTATES {FMD1_M2V_INNER_TOP }
objectset: FMD1_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS {FMD1_M2V_INNER_TOP }

class: FMD1_PW_INNER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPFwCaenChannelDcsHV_FWDM is_of_class FMD1_PW_INNER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD1_PW_INNER_TOPFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel000 is_of_class FMD1_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel001 is_of_class FMD1_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel002 is_of_class FMD1_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel003 is_of_class FMD1_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel004 is_of_class FMD1_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

objectset: FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel003,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel004 }
objectset: FMD1_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel003,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_TOP:HvChannel004 }

class: FMD1_PW_INNER_TOPDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPDIMFEC_FWDM is_of_class FMD1_PW_INNER_TOPDIMFEC_FwDevMode_CLASS


class: FMD1_PW_INNER_TOPDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC1_I_U is_of_class FMD1_PW_INNER_TOPDIMFEC_CLASS

objectset: FMD1_PW_INNER_TOPDIMFEC_FWSETSTATES {FEC1_I_U }
objectset: FMD1_PW_INNER_TOPDIMFEC_FWSETACTIONS {FEC1_I_U }

class: FMD1_PW_INNER_TOPFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_TOPFmdLVMode_FWDM is_of_class FMD1_PW_INNER_TOPFmdLVMode_FwDevMode_CLASS


class: FMD1_PW_INNER_TOPFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD1IU_LVMode is_of_class FMD1_PW_INNER_TOPFmdLVMode_CLASS

objectset: FMD1_PW_INNER_TOPFMDLVMODE_FWSETSTATES {FMD1IU_LVMode }
objectset: FMD1_PW_INNER_TOPFMDLVMODE_FWSETACTIONS {FMD1IU_LVMode }

class: FMD1_3V3_INNER_BOTTOMTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_3V3_INNER_BOTTOM is_of_class FMD1_3V3_INNER_BOTTOMTOP_FmdFEE3V3_CLASS

class: FMD1_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD1_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_3V3_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel000 is_of_class FMD1_3V3_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel000 }
objectset: FMD1_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel000 }

class: FMD1_2V5_INNER_BOTTOMTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_2V5_INNER_BOTTOM is_of_class FMD1_2V5_INNER_BOTTOMTOP_FmdFEE2V5_CLASS

class: FMD1_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD1_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_2V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel001 is_of_class FMD1_2V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel001 }
objectset: FMD1_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel001 }

class: FMD1_1V5_INNER_BOTTOMTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_1V5_INNER_BOTTOM is_of_class FMD1_1V5_INNER_BOTTOMTOP_FmdFEE1V5_CLASS

class: FMD1_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD1_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_1V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel002 is_of_class FMD1_1V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel002 }
objectset: FMD1_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel002 }

class: FMD1_M2V_INNER_BOTTOMTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD1_M2V_INNER_BOTTOM is_of_class FMD1_M2V_INNER_BOTTOMTOP_FmdFEEM2V_CLASS

class: FMD1_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD1_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD1_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD1_M2V_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel003 is_of_class FMD1_M2V_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel003 }
objectset: FMD1_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel003 }

class: FMD1_PW_INNER_BOTTOMTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD1_PW_INNER_BOTTOM is_of_class FMD1_PW_INNER_BOTTOMTOP_FmdSector_CLASS

class: FMD1_PW_INNER_BOTTOMFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMFmdFEE3V3_FWDM is_of_class FMD1_PW_INNER_BOTTOMFmdFEE3V3_FwDevMode_CLASS


objectset: FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES {FMD1_3V3_INNER_BOTTOM }
objectset: FMD1_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS {FMD1_3V3_INNER_BOTTOM }

class: FMD1_PW_INNER_BOTTOMFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMFmdFEE2V5_FWDM is_of_class FMD1_PW_INNER_BOTTOMFmdFEE2V5_FwDevMode_CLASS


objectset: FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES {FMD1_2V5_INNER_BOTTOM }
objectset: FMD1_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS {FMD1_2V5_INNER_BOTTOM }

class: FMD1_PW_INNER_BOTTOMFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMFmdFEE1V5_FWDM is_of_class FMD1_PW_INNER_BOTTOMFmdFEE1V5_FwDevMode_CLASS


objectset: FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES {FMD1_1V5_INNER_BOTTOM }
objectset: FMD1_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS {FMD1_1V5_INNER_BOTTOM }

class: FMD1_PW_INNER_BOTTOMFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMFmdFEEM2V_FWDM is_of_class FMD1_PW_INNER_BOTTOMFmdFEEM2V_FwDevMode_CLASS


objectset: FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES {FMD1_M2V_INNER_BOTTOM }
objectset: FMD1_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS {FMD1_M2V_INNER_BOTTOM }

class: FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_FWDM is_of_class FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel000 is_of_class FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel001 is_of_class FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel002 is_of_class FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel003 is_of_class FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel004 is_of_class FMD1_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

objectset: FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel004 }
objectset: FMD1_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel004 }

class: FMD1_PW_INNER_BOTTOMDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMDIMFEC_FWDM is_of_class FMD1_PW_INNER_BOTTOMDIMFEC_FwDevMode_CLASS


class: FMD1_PW_INNER_BOTTOMDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC1_I_D is_of_class FMD1_PW_INNER_BOTTOMDIMFEC_CLASS

objectset: FMD1_PW_INNER_BOTTOMDIMFEC_FWSETSTATES {FEC1_I_D }
objectset: FMD1_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS {FEC1_I_D }

class: FMD1_PW_INNER_BOTTOMFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD1_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD1_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD1_PW_INNER_BOTTOMFmdLVMode_FWDM is_of_class FMD1_PW_INNER_BOTTOMFmdLVMode_FwDevMode_CLASS


class: FMD1_PW_INNER_BOTTOMFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD1ID_LVMode is_of_class FMD1_PW_INNER_BOTTOMFmdLVMode_CLASS

objectset: FMD1_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES {FMD1ID_LVMode }
objectset: FMD1_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS {FMD1ID_LVMode }

class: FMD1_MODTOP_FmdPowerModule_CLASS
!panel: FmdPowerModule.pnl
    parameters: string run_type_tag = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  )  move_to STANDBY
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  )  move_to STBY_CONFIGURED
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state {OFF,NO_CONTROL} ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {OFF,NO_CONTROL} ) )  )  move_to ERROR
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: GO_STANDBY	!visible: 1
            move_to CHECKING_COOLING
    state: CHECKING_COOLING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDRINGCOOLING_FWSETSTATES in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  do GO_STANDBY
        action: GO_STANDBY	!visible: 0
            do GO_STANDBY all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to POWERING_RCU
    state: POWERING_RCU	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state {OFF,POWERING,BOOTING,STANDBY} ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state STANDBY ) )  do POWER_SECTORS
        action: POWER_SECTORS	!visible: 0
            do POWER all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to POWERING_SECTORS
    state: POWERING_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {OFF,POWERING,POWERED} ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  move_to STANDBY
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state POWERED ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_OFF	!visible: 1
            do POWER_DOWN all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to POWERING_DOWN_SECTORS
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do START all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            set run_type_tag = run_type
            move_to STARTING_SECTORS
    state: POWERING_DOWN_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {POWERED,POWERING_DOWN,OFF} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  do POWER_DOWN_RCU
        action: POWER_DOWN_RCU	!visible: 0
            do GO_OFF all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to POWERING_DOWN_RCU
    state: POWERING_DOWN_RCU	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state {STANDBY,POWERING_DOWN,OFF} ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state OFF ) )  move_to OFF
    state: STARTING_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {POWERED,STARTING,STARTED_NO_MON} ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state STARTED_NO_MON ) )  do CONFIGURE
        action: CONFIGURE	!visible: 0
            do CONFIGURE(run_type = run_type_tag) all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: CONFIGURING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state {STANDBY,DOWNLOADING,INVALID_CONF,RESETTING,CONFIGURING,INIT_ZERO_SUP,UPLOADING,READY} ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {STARTED_NO_MON,RUNNING,RUNNING_NO_MON} ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when (  ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  )  move_to STBY_CONFIGURED
        when ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state INVALID_CONF ) )  move_to INVALID_CONF
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state RUNNING ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do STOP all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            do CLEAR all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to CLEARING
        action: GO_READY	!visible: 1
            do GO_READY all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_READY
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do INHIBIT all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            do CONFIGURE(run_type = run_type) all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: INVALID_CONF	!color: FwStateAttention2
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state INVALID_CONF ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state RUNNING ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do CLEAR all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to CLEARING
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(run_type = run_type) all_in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: CLEARING	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state {INVALID_CONF,CLEARING_INVALID,READY,SET_CLEAR,CLEARING,STANDBY} ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,STOPPING_PREPARE,STOPPING,POWERED} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMD_RCU_FWSETSTATES in_state STANDBY ) )  move_to MOVING_STANDBY
        action: STOP_SECTORS	!visible: 0
            do STOP all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_STANDBY
    state: MOVING_STANDBY	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,STOPPING_PREPARE,STOPPING,POWERED} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  move_to STANDBY
    state: MOVING_READY	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,POWERING_HYBRIDS,RAMPING_HV,READY} ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STBY_CONF	!visible: 1
            do GO_NOTREADY all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_STBY_CONF
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when (  ( ( any_in FMD1_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD1_MODFMDSECTOR_FWSETSTATES not_in_state {READY,RAMPING_DOWN_HV,POWERING_DOWN_HYBRIDS,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD1_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  move_to STBY_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            do RESET all_in FMD1_MODFMD_RCU_FWSETACTIONS
            do RESET all_in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to MIXED

object: FMD1_MOD is_of_class FMD1_MODTOP_FmdPowerModule_CLASS

class: FMD1_MODFMDRingCooling_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_MODFMDRINGCOOLING_FWSETSTATES
            remove &VAL_OF_Device from FMD1_MODFMDRINGCOOLING_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_MODFMDRINGCOOLING_FWSETSTATES
            insert &VAL_OF_Device in FMD1_MODFMDRINGCOOLING_FWSETACTIONS
            move_to READY

object: FMD1_MODFMDRingCooling_FWDM is_of_class FMD1_MODFMDRingCooling_FwDevMode_CLASS


class: FMD1_MODFMDRingCooling_CLASS/associated
!panel: FMD_Panels/FMD_RingCooling.pnl
    parameters: int TimerStart = 0
    state: IGNORED	!color: FwStateAttention2
    state: NO_COMM	!color: FwStateAttention2
    state: NO_COMM_INT	!color: FwStateAttention1
    state: DISABLED	!color: FwStateOKNotPhysics
    state: DISABLED_INT	!color: FwStateAttention1
    state: OFF	!color: FwStateOKNotPhysics
    state: OFF_INT	!color: FwStateAttention1
    state: RUNNING	!color: FwStateOKPhysics
    state: ERROR	!color: FwStateAttention3

object: FMD1_Cooling is_of_class FMD1_MODFMDRingCooling_CLASS

objectset: FMD1_MODFMDRINGCOOLING_FWSETSTATES {FMD1_Cooling }
objectset: FMD1_MODFMDRINGCOOLING_FWSETACTIONS {FMD1_Cooling }

class: FMD1_MODFMD_RCU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_MODFMD_RCU_FWSETSTATES
            remove &VAL_OF_Device from FMD1_MODFMD_RCU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_MODFMD_RCU_FWSETSTATES
            insert &VAL_OF_Device in FMD1_MODFMD_RCU_FWSETACTIONS
            move_to READY

object: FMD1_MODFMD_RCU_FWDM is_of_class FMD1_MODFMD_RCU_FwDevMode_CLASS


objectset: FMD1_MODFMD_RCU_FWSETSTATES {FMD1_PW_RCU }
objectset: FMD1_MODFMD_RCU_FWSETACTIONS {FMD1_PW_RCU }

class: FMD1_MODFmdSector_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD1_MODFMDSECTOR_FWSETSTATES
            remove &VAL_OF_Device from FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD1_MODFMDSECTOR_FWSETSTATES
            insert &VAL_OF_Device in FMD1_MODFMDSECTOR_FWSETACTIONS
            move_to READY

object: FMD1_MODFmdSector_FWDM is_of_class FMD1_MODFmdSector_FwDevMode_CLASS


objectset: FMD1_MODFMDSECTOR_FWSETSTATES {FMD1_PW_INNER_TOP,
	FMD1_PW_INNER_BOTTOM }
objectset: FMD1_MODFMDSECTOR_FWSETACTIONS {FMD1_PW_INNER_TOP,
	FMD1_PW_INNER_BOTTOM }

class: FMD2_PW_RCUTOP_FMD_RCU_CLASS
!panel: FMD_Panels/FMD_RCU.pnl
    parameters: string runtype = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to OFF
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  )  move_to STANDBY
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  )  move_to INVALID_CONF
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state {READY,RUNNING} ) )  )  move_to READY
        when (  not (  (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  ) or (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  ) or (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  and  ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  ) or (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  ) or (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state {READY,RUNNING} ) )  )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state NO_LINK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state NO_LINK ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        action: GO_STANDBY	!visible: 1
            do GO_READY all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {NO_LINK,STARTUP,STANDBY} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to BOOTING
    state: BOOTING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {NO_LINK,STARTUP,STANDBY} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  move_to STANDBY
    state: STANDBY	!color: FwStateOKNotPhysics
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state STANDBY ) )  move_to ERROR
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to POWERING_DOWN
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(Tag = run_type) all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to DOWNLOADING
    state: POWERING_DOWN	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,NO_LINK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  )  do FORCE_NO_LINK
        when (  ( ( all_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to OFF
        action: FORCE_NO_LINK	!visible: 0
            do FORCE_NO_LINK all_in FMD2_PW_RCUDIMRCU_FWSETACTIONS
    state: DOWNLOADING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,CONFIGURING,READY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {IDLE,CONFIGURING,DONE,INVALID_CONF,SKIPPED} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        when ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  move_to INVALID_CONF
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CONFIGURING
    state: INVALID_CONF	!color: FwStateAttention2
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        action: CLEAR	!visible: 1
            do ACKNOWLEDGE all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CLEARING_INVALID
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do ACKNOWLEDGE all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to RESETTING
    state: CLEARING_INVALID	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {INVALID_CONF,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  do CLEAR
        action: CLEAR	!visible: 1
            do RESET all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to SET_CLEAR
    state: RESETTING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {INVALID_CONF,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  do CONFIGURE
        action: CONFIGURE	!visible: 1
            do CONFIGURE(Tag = runtype) all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to DOWNLOADING
    state: CONFIGURING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,CONFIGURING,READY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state READY ) )  )  do UPLOAD
        action: UPLOAD	!visible: 1
            do UPLOAD(runtype = runtype) all_in FMD2_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to INIT_ZERO_SUP
    state: INIT_ZERO_SUP	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state {IDLE,UPLOADING,DONE,SKIPPED} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD2_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to UPLOADING
    state: UPLOADING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES in_state IDLE ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {READY,RUNNING} ) )  move_to ERROR
        action: CLEAR	!visible: 1
            do RESET all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to SET_CLEAR
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(Tag = run_type) all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to DOWNLOADING
    state: SET_CLEAR	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {RUNNING,READY,STANDBY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {IDLE,CONFIGURING,DONE,SKIPPED} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CLEARING
    state: CLEARING	!color: FwStateAttention1
        when ( ( any_in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMRCU_FWSETSTATES not_in_state {RUNNING,READY,STANDBY} ) )  move_to ERROR
        when ( ( any_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) ) and
       ( ( all_in FMD2_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  )  move_to STANDBY
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to MIXED

object: FMD2_PW_RCU is_of_class FMD2_PW_RCUTOP_FMD_RCU_CLASS

class: FMD2_PW_RCUFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_PW_RCUFwCaenChannelDcsLV_FWDM is_of_class FMD2_PW_RCUFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_PW_RCUFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_RCU:LvChannel000 is_of_class FMD2_PW_RCUFwCaenChannelDcsLV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_RCU:LvChannel001 is_of_class FMD2_PW_RCUFwCaenChannelDcsLV_CLASS

objectset: FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_RCU:LvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_RCU:LvChannel001 }
objectset: FMD2_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_RCU:LvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_RCU:LvChannel001 }

class: FMD2_PW_RCUDIMRCU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_RCUDIMRCU_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_RCUDIMRCU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_RCUDIMRCU_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_RCUDIMRCU_FWSETACTIONS
            move_to READY

object: FMD2_PW_RCUDIMRCU_FWDM is_of_class FMD2_PW_RCUDIMRCU_FwDevMode_CLASS


class: FMD2_PW_RCUDIMRCU_CLASS/associated
!panel: FMD_Panels/FMD_DIMRCU.pnl
    parameters: int TimerStart = 0
    state: NO_LINK	!color: FwStateAttention2
        action: WAIT_FOR_STANDBY	!visible: 1
    state: STARTUP	!color: FwStateAttention1
        action: WAIT_FOR_STANDBY	!visible: 1
    state: STANDBY	!color: FwStateOKNotPhysics
        action: FORCE_NO_LINK	!visible: 1
        action: WAIT_FOR_READY	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: WAIT_FOR_READY	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: WAIT_FOR_STANDBY	!visible: 1
    state: MIXED	!color: FwStateAttention1
    state: ERROR	!color: FwStateAttention3
    state: RUNNING	!color: FwStateOKPhysics
    state: WAITING_FOR_STANDBY	!color: FwStateAttention1
    state: WAITING_FOR_READY	!color: FwStateAttention1
    state: UNKNOWN	!color: FwStateAttention3

object: RCU_2 is_of_class FMD2_PW_RCUDIMRCU_CLASS

objectset: FMD2_PW_RCUDIMRCU_FWSETSTATES {RCU_2 }
objectset: FMD2_PW_RCUDIMRCU_FWSETACTIONS {RCU_2 }

class: FMD2_PW_RCUDIMMiniconf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_RCUDIMMINICONF_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_RCUDIMMINICONF_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to READY

object: FMD2_PW_RCUDIMMiniconf_FWDM is_of_class FMD2_PW_RCUDIMMiniconf_FwDevMode_CLASS


class: FMD2_PW_RCUDIMMiniconf_CLASS/associated
!panel: FMD_Panels/FMD_Miniconf.pnl
    state: NO_LINK	!color: FwStateAttention2
    state: IDLE	!color: FwStateOKNotPhysics
        action: CONFIGURE(string Tag = "physics")	!visible: 1
        action: RESET	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: CLEAR	!visible: 1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
        action: CLEAR	!visible: 1
    state: INVALID_CONF	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: SKIPPED	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: CLEAR	!visible: 1
    state: UNKNOWN	!color: FwStateAttention3
        action: CLEAR	!visible: 1

object: Miniconf_2 is_of_class FMD2_PW_RCUDIMMiniconf_CLASS

objectset: FMD2_PW_RCUDIMMINICONF_FWSETSTATES {Miniconf_2 }
objectset: FMD2_PW_RCUDIMMINICONF_FWSETACTIONS {Miniconf_2 }

class: FMD2_PW_RCUDIMPedconf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_RCUDIMPEDCONF_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_RCUDIMPEDCONF_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to READY

object: FMD2_PW_RCUDIMPedconf_FWDM is_of_class FMD2_PW_RCUDIMPedconf_FwDevMode_CLASS


class: FMD2_PW_RCUDIMPedconf_CLASS/associated
!panel: FMD_Panels/FMD_Miniconf.pnl
    state: NO_LINK	!color: FwStateAttention2
    state: IDLE	!color: FwStateOKNotPhysics
        action: UPLOAD(string runtype = "")	!visible: 1
    state: UPLOADING	!color: FwStateAttention1
        action: CLEAR	!visible: 1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
        action: CLEAR	!visible: 1
    state: SKIPPED	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: CLEAR	!visible: 1
    state: UNKNOWN	!color: FwStateAttention3
        action: CLEAR	!visible: 1

object: Pedconf_2 is_of_class FMD2_PW_RCUDIMPedconf_CLASS

objectset: FMD2_PW_RCUDIMPEDCONF_FWSETSTATES {Pedconf_2 }
objectset: FMD2_PW_RCUDIMPEDCONF_FWSETACTIONS {Pedconf_2 }

class: FMD2_3V3_INNER_TOPTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_3V3_INNER_TOP is_of_class FMD2_3V3_INNER_TOPTOP_FmdFEE3V3_CLASS

class: FMD2_3V3_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_3V3_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_3V3_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_3V3_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel000 is_of_class FMD2_3V3_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel000 }
objectset: FMD2_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel000 }

class: FMD2_2V5_INNER_TOPTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_2V5_INNER_TOP is_of_class FMD2_2V5_INNER_TOPTOP_FmdFEE2V5_CLASS

class: FMD2_2V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_2V5_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_2V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_2V5_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel001 is_of_class FMD2_2V5_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel001 }
objectset: FMD2_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel001 }

class: FMD2_1V5_INNER_TOPTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_1V5_INNER_TOP is_of_class FMD2_1V5_INNER_TOPTOP_FmdFEE1V5_CLASS

class: FMD2_1V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_1V5_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_1V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_1V5_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel002 is_of_class FMD2_1V5_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel002 }
objectset: FMD2_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel002 }

class: FMD2_M2V_INNER_TOPTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_M2V_INNER_TOP is_of_class FMD2_M2V_INNER_TOPTOP_FmdFEEM2V_CLASS

class: FMD2_M2V_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_M2V_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_M2V_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_M2V_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel003 is_of_class FMD2_M2V_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel003 }
objectset: FMD2_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:LvChannel003 }

class: FMD2_PW_INNER_TOPTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD2_PW_INNER_TOP is_of_class FMD2_PW_INNER_TOPTOP_FmdSector_CLASS

class: FMD2_PW_INNER_TOPFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPFmdFEE3V3_FWDM is_of_class FMD2_PW_INNER_TOPFmdFEE3V3_FwDevMode_CLASS


objectset: FMD2_PW_INNER_TOPFMDFEE3V3_FWSETSTATES {FMD2_3V3_INNER_TOP }
objectset: FMD2_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS {FMD2_3V3_INNER_TOP }

class: FMD2_PW_INNER_TOPFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPFmdFEE2V5_FWDM is_of_class FMD2_PW_INNER_TOPFmdFEE2V5_FwDevMode_CLASS


objectset: FMD2_PW_INNER_TOPFMDFEE2V5_FWSETSTATES {FMD2_2V5_INNER_TOP }
objectset: FMD2_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS {FMD2_2V5_INNER_TOP }

class: FMD2_PW_INNER_TOPFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPFmdFEE1V5_FWDM is_of_class FMD2_PW_INNER_TOPFmdFEE1V5_FwDevMode_CLASS


objectset: FMD2_PW_INNER_TOPFMDFEE1V5_FWSETSTATES {FMD2_1V5_INNER_TOP }
objectset: FMD2_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS {FMD2_1V5_INNER_TOP }

class: FMD2_PW_INNER_TOPFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPFmdFEEM2V_FWDM is_of_class FMD2_PW_INNER_TOPFmdFEEM2V_FwDevMode_CLASS


objectset: FMD2_PW_INNER_TOPFMDFEEM2V_FWSETSTATES {FMD2_M2V_INNER_TOP }
objectset: FMD2_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS {FMD2_M2V_INNER_TOP }

class: FMD2_PW_INNER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPFwCaenChannelDcsHV_FWDM is_of_class FMD2_PW_INNER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD2_PW_INNER_TOPFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel000 is_of_class FMD2_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel001 is_of_class FMD2_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel002 is_of_class FMD2_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel003 is_of_class FMD2_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel004 is_of_class FMD2_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

objectset: FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel004 }
objectset: FMD2_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_TOP:HvChannel004 }

class: FMD2_PW_INNER_TOPDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPDIMFEC_FWDM is_of_class FMD2_PW_INNER_TOPDIMFEC_FwDevMode_CLASS


class: FMD2_PW_INNER_TOPDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC2_I_U is_of_class FMD2_PW_INNER_TOPDIMFEC_CLASS

objectset: FMD2_PW_INNER_TOPDIMFEC_FWSETSTATES {FEC2_I_U }
objectset: FMD2_PW_INNER_TOPDIMFEC_FWSETACTIONS {FEC2_I_U }

class: FMD2_PW_INNER_TOPFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_TOPFmdLVMode_FWDM is_of_class FMD2_PW_INNER_TOPFmdLVMode_FwDevMode_CLASS


class: FMD2_PW_INNER_TOPFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD2IU_LVMode is_of_class FMD2_PW_INNER_TOPFmdLVMode_CLASS

objectset: FMD2_PW_INNER_TOPFMDLVMODE_FWSETSTATES {FMD2IU_LVMode }
objectset: FMD2_PW_INNER_TOPFMDLVMODE_FWSETACTIONS {FMD2IU_LVMode }

class: FMD2_3V3_INNER_BOTTOMTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_3V3_INNER_BOTTOM is_of_class FMD2_3V3_INNER_BOTTOMTOP_FmdFEE3V3_CLASS

class: FMD2_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_3V3_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel000 is_of_class FMD2_3V3_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel000 }
objectset: FMD2_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel000 }

class: FMD2_2V5_INNER_BOTTOMTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_2V5_INNER_BOTTOM is_of_class FMD2_2V5_INNER_BOTTOMTOP_FmdFEE2V5_CLASS

class: FMD2_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_2V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel001 is_of_class FMD2_2V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel001 }
objectset: FMD2_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel001 }

class: FMD2_1V5_INNER_BOTTOMTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_1V5_INNER_BOTTOM is_of_class FMD2_1V5_INNER_BOTTOMTOP_FmdFEE1V5_CLASS

class: FMD2_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_1V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel002 is_of_class FMD2_1V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel002 }
objectset: FMD2_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel002 }

class: FMD2_M2V_INNER_BOTTOMTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_M2V_INNER_BOTTOM is_of_class FMD2_M2V_INNER_BOTTOMTOP_FmdFEEM2V_CLASS

class: FMD2_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_M2V_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel003 is_of_class FMD2_M2V_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel003 }
objectset: FMD2_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:LvChannel003 }

class: FMD2_PW_INNER_BOTTOMTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD2_PW_INNER_BOTTOM is_of_class FMD2_PW_INNER_BOTTOMTOP_FmdSector_CLASS

class: FMD2_PW_INNER_BOTTOMFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMFmdFEE3V3_FWDM is_of_class FMD2_PW_INNER_BOTTOMFmdFEE3V3_FwDevMode_CLASS


objectset: FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES {FMD2_3V3_INNER_BOTTOM }
objectset: FMD2_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS {FMD2_3V3_INNER_BOTTOM }

class: FMD2_PW_INNER_BOTTOMFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMFmdFEE2V5_FWDM is_of_class FMD2_PW_INNER_BOTTOMFmdFEE2V5_FwDevMode_CLASS


objectset: FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES {FMD2_2V5_INNER_BOTTOM }
objectset: FMD2_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS {FMD2_2V5_INNER_BOTTOM }

class: FMD2_PW_INNER_BOTTOMFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMFmdFEE1V5_FWDM is_of_class FMD2_PW_INNER_BOTTOMFmdFEE1V5_FwDevMode_CLASS


objectset: FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES {FMD2_1V5_INNER_BOTTOM }
objectset: FMD2_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS {FMD2_1V5_INNER_BOTTOM }

class: FMD2_PW_INNER_BOTTOMFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMFmdFEEM2V_FWDM is_of_class FMD2_PW_INNER_BOTTOMFmdFEEM2V_FwDevMode_CLASS


objectset: FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES {FMD2_M2V_INNER_BOTTOM }
objectset: FMD2_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS {FMD2_M2V_INNER_BOTTOM }

class: FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_FWDM is_of_class FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel000 is_of_class FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel001 is_of_class FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel002 is_of_class FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel003 is_of_class FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel004 is_of_class FMD2_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

objectset: FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel004 }
objectset: FMD2_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_INNER_BOTTOM:HvChannel004 }

class: FMD2_PW_INNER_BOTTOMDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMDIMFEC_FWDM is_of_class FMD2_PW_INNER_BOTTOMDIMFEC_FwDevMode_CLASS


class: FMD2_PW_INNER_BOTTOMDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC2_I_D is_of_class FMD2_PW_INNER_BOTTOMDIMFEC_CLASS

objectset: FMD2_PW_INNER_BOTTOMDIMFEC_FWSETSTATES {FEC2_I_D }
objectset: FMD2_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS {FEC2_I_D }

class: FMD2_PW_INNER_BOTTOMFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD2_PW_INNER_BOTTOMFmdLVMode_FWDM is_of_class FMD2_PW_INNER_BOTTOMFmdLVMode_FwDevMode_CLASS


class: FMD2_PW_INNER_BOTTOMFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD2ID_LVMode is_of_class FMD2_PW_INNER_BOTTOMFmdLVMode_CLASS

objectset: FMD2_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES {FMD2ID_LVMode }
objectset: FMD2_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS {FMD2ID_LVMode }

class: FMD2_3V3_OUTER_TOPTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_3V3_OUTER_TOP is_of_class FMD2_3V3_OUTER_TOPTOP_FmdFEE3V3_CLASS

class: FMD2_3V3_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_3V3_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_3V3_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_3V3_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel000 is_of_class FMD2_3V3_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel000 }
objectset: FMD2_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel000 }

class: FMD2_2V5_OUTER_TOPTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_2V5_OUTER_TOP is_of_class FMD2_2V5_OUTER_TOPTOP_FmdFEE2V5_CLASS

class: FMD2_2V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_2V5_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_2V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_2V5_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel001 is_of_class FMD2_2V5_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel001 }
objectset: FMD2_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel001 }

class: FMD2_1V5_OUTER_TOPTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_1V5_OUTER_TOP is_of_class FMD2_1V5_OUTER_TOPTOP_FmdFEE1V5_CLASS

class: FMD2_1V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_1V5_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_1V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_1V5_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel002 is_of_class FMD2_1V5_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel002 }
objectset: FMD2_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel002 }

class: FMD2_M2V_OUTER_TOPTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_M2V_OUTER_TOP is_of_class FMD2_M2V_OUTER_TOPTOP_FmdFEEM2V_CLASS

class: FMD2_M2V_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_M2V_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD2_M2V_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_M2V_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel003 is_of_class FMD2_M2V_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel003 }
objectset: FMD2_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:LvChannel003 }

class: FMD2_PW_OUTER_TOPTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD2_PW_OUTER_TOP is_of_class FMD2_PW_OUTER_TOPTOP_FmdSector_CLASS

class: FMD2_PW_OUTER_TOPFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPFmdFEE3V3_FWDM is_of_class FMD2_PW_OUTER_TOPFmdFEE3V3_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES {FMD2_3V3_OUTER_TOP }
objectset: FMD2_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS {FMD2_3V3_OUTER_TOP }

class: FMD2_PW_OUTER_TOPFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPFmdFEE2V5_FWDM is_of_class FMD2_PW_OUTER_TOPFmdFEE2V5_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES {FMD2_2V5_OUTER_TOP }
objectset: FMD2_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS {FMD2_2V5_OUTER_TOP }

class: FMD2_PW_OUTER_TOPFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPFmdFEE1V5_FWDM is_of_class FMD2_PW_OUTER_TOPFmdFEE1V5_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES {FMD2_1V5_OUTER_TOP }
objectset: FMD2_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS {FMD2_1V5_OUTER_TOP }

class: FMD2_PW_OUTER_TOPFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPFmdFEEM2V_FWDM is_of_class FMD2_PW_OUTER_TOPFmdFEEM2V_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES {FMD2_M2V_OUTER_TOP }
objectset: FMD2_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS {FMD2_M2V_OUTER_TOP }

class: FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_FWDM is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel000 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel001 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel002 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel003 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel004 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel005 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel006 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel007 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel008 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel009 is_of_class FMD2_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

objectset: FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel004,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel005,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel006,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel007,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel008,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel009 }
objectset: FMD2_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel004,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel005,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel006,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel007,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel008,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_TOP:HvChannel009 }

class: FMD2_PW_OUTER_TOPDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPDIMFEC_FWDM is_of_class FMD2_PW_OUTER_TOPDIMFEC_FwDevMode_CLASS


class: FMD2_PW_OUTER_TOPDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC2_O_U is_of_class FMD2_PW_OUTER_TOPDIMFEC_CLASS

objectset: FMD2_PW_OUTER_TOPDIMFEC_FWSETSTATES {FEC2_O_U }
objectset: FMD2_PW_OUTER_TOPDIMFEC_FWSETACTIONS {FEC2_O_U }

class: FMD2_PW_OUTER_TOPFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_TOPFmdLVMode_FWDM is_of_class FMD2_PW_OUTER_TOPFmdLVMode_FwDevMode_CLASS


class: FMD2_PW_OUTER_TOPFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD2OU_LVMode is_of_class FMD2_PW_OUTER_TOPFmdLVMode_CLASS

objectset: FMD2_PW_OUTER_TOPFMDLVMODE_FWSETSTATES {FMD2OU_LVMode }
objectset: FMD2_PW_OUTER_TOPFMDLVMODE_FWSETACTIONS {FMD2OU_LVMode }

class: FMD2_3V3_OUTER_BOTTOMTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_3V3_OUTER_BOTTOM is_of_class FMD2_3V3_OUTER_BOTTOMTOP_FmdFEE3V3_CLASS

class: FMD2_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel000 is_of_class FMD2_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel000 }
objectset: FMD2_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel000 }

class: FMD2_2V5_OUTER_BOTTOMTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_2V5_OUTER_BOTTOM is_of_class FMD2_2V5_OUTER_BOTTOMTOP_FmdFEE2V5_CLASS

class: FMD2_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel001 is_of_class FMD2_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel001 }
objectset: FMD2_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel001 }

class: FMD2_1V5_OUTER_BOTTOMTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_1V5_OUTER_BOTTOM is_of_class FMD2_1V5_OUTER_BOTTOMTOP_FmdFEE1V5_CLASS

class: FMD2_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel002 is_of_class FMD2_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel002 }
objectset: FMD2_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel002 }

class: FMD2_M2V_OUTER_BOTTOMTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD2_M2V_OUTER_BOTTOM is_of_class FMD2_M2V_OUTER_BOTTOMTOP_FmdFEEM2V_CLASS

class: FMD2_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD2_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD2_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD2_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel003 is_of_class FMD2_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel003 }
objectset: FMD2_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:LvChannel003 }

class: FMD2_PW_OUTER_BOTTOMTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD2_PW_OUTER_BOTTOM is_of_class FMD2_PW_OUTER_BOTTOMTOP_FmdSector_CLASS

class: FMD2_PW_OUTER_BOTTOMFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMFmdFEE3V3_FWDM is_of_class FMD2_PW_OUTER_BOTTOMFmdFEE3V3_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES {FMD2_3V3_OUTER_BOTTOM }
objectset: FMD2_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS {FMD2_3V3_OUTER_BOTTOM }

class: FMD2_PW_OUTER_BOTTOMFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMFmdFEE2V5_FWDM is_of_class FMD2_PW_OUTER_BOTTOMFmdFEE2V5_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES {FMD2_2V5_OUTER_BOTTOM }
objectset: FMD2_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS {FMD2_2V5_OUTER_BOTTOM }

class: FMD2_PW_OUTER_BOTTOMFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMFmdFEE1V5_FWDM is_of_class FMD2_PW_OUTER_BOTTOMFmdFEE1V5_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES {FMD2_1V5_OUTER_BOTTOM }
objectset: FMD2_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS {FMD2_1V5_OUTER_BOTTOM }

class: FMD2_PW_OUTER_BOTTOMFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMFmdFEEM2V_FWDM is_of_class FMD2_PW_OUTER_BOTTOMFmdFEEM2V_FwDevMode_CLASS


objectset: FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES {FMD2_M2V_OUTER_BOTTOM }
objectset: FMD2_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS {FMD2_M2V_OUTER_BOTTOM }

class: FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_FWDM is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel000 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel001 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel002 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel003 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel004 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel005 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel006 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel007 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel008 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel009 is_of_class FMD2_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

objectset: FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel004,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel005,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel006,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel007,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel008,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel009 }
objectset: FMD2_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel000,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel001,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel002,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel003,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel004,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel005,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel006,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel007,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel008,
	FMD_DCS:FMD2_MOD:FMD2_PW_OUTER_BOTTOM:HvChannel009 }

class: FMD2_PW_OUTER_BOTTOMDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMDIMFEC_FWDM is_of_class FMD2_PW_OUTER_BOTTOMDIMFEC_FwDevMode_CLASS


class: FMD2_PW_OUTER_BOTTOMDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC2_O_D is_of_class FMD2_PW_OUTER_BOTTOMDIMFEC_CLASS

objectset: FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES {FEC2_O_D }
objectset: FMD2_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS {FEC2_O_D }

class: FMD2_PW_OUTER_BOTTOMFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD2_PW_OUTER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD2_PW_OUTER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD2_PW_OUTER_BOTTOMFmdLVMode_FWDM is_of_class FMD2_PW_OUTER_BOTTOMFmdLVMode_FwDevMode_CLASS


class: FMD2_PW_OUTER_BOTTOMFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD2OD_LVMode is_of_class FMD2_PW_OUTER_BOTTOMFmdLVMode_CLASS

objectset: FMD2_PW_OUTER_BOTTOMFMDLVMODE_FWSETSTATES {FMD2OD_LVMode }
objectset: FMD2_PW_OUTER_BOTTOMFMDLVMODE_FWSETACTIONS {FMD2OD_LVMode }

class: FMD2_MODTOP_FmdPowerModule_CLASS
!panel: FmdPowerModule.pnl
    parameters: string run_type_tag = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  )  move_to STANDBY
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  )  move_to STBY_CONFIGURED
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state {OFF,NO_CONTROL} ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {OFF,NO_CONTROL} ) )  )  move_to ERROR
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: GO_STANDBY	!visible: 1
            move_to CHECKING_COOLING
    state: CHECKING_COOLING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDRINGCOOLING_FWSETSTATES in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  do GO_STANDBY
        action: GO_STANDBY	!visible: 0
            do GO_STANDBY all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to POWERING_RCU
    state: POWERING_RCU	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state {OFF,POWERING,BOOTING,STANDBY} ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state STANDBY ) )  do POWER_SECTORS
        action: POWER_SECTORS	!visible: 0
            do POWER all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to POWERING_SECTORS
    state: POWERING_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {OFF,POWERING,POWERED} ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  move_to STANDBY
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state POWERED ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_OFF	!visible: 1
            do POWER_DOWN all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to POWERING_DOWN_SECTORS
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do START all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            set run_type_tag = run_type
            move_to STARTING_SECTORS
    state: POWERING_DOWN_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {POWERED,POWERING_DOWN,OFF} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  do POWER_DOWN_RCU
        action: POWER_DOWN_RCU	!visible: 0
            do GO_OFF all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to POWERING_DOWN_RCU
    state: POWERING_DOWN_RCU	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state {STANDBY,POWERING_DOWN,OFF} ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state OFF ) )  move_to OFF
    state: STARTING_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {POWERED,STARTING,STARTED_NO_MON} ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state STARTED_NO_MON ) )  do CONFIGURE
        action: CONFIGURE	!visible: 0
            do CONFIGURE(run_type = run_type_tag) all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: CONFIGURING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state {STANDBY,DOWNLOADING,INVALID_CONF,RESETTING,CONFIGURING,INIT_ZERO_SUP,UPLOADING,READY} ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {STARTED_NO_MON,RUNNING,RUNNING_NO_MON} ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when (  ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  )  move_to STBY_CONFIGURED
        when ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state INVALID_CONF ) )  move_to INVALID_CONF
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state RUNNING ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do STOP all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            do CLEAR all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to CLEARING
        action: GO_READY	!visible: 1
            do GO_READY all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_READY
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do INHIBIT all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            do CONFIGURE(run_type = run_type) all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: INVALID_CONF	!color: FwStateAttention2
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state INVALID_CONF ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state RUNNING ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do CLEAR all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to CLEARING
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(run_type = run_type) all_in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: CLEARING	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state {INVALID_CONF,CLEARING_INVALID,READY,SET_CLEAR,CLEARING,STANDBY} ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,STOPPING_PREPARE,STOPPING,POWERED} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMD_RCU_FWSETSTATES in_state STANDBY ) )  move_to MOVING_STANDBY
        action: STOP_SECTORS	!visible: 0
            do STOP all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_STANDBY
    state: MOVING_STANDBY	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,STOPPING_PREPARE,STOPPING,POWERED} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  move_to STANDBY
    state: MOVING_READY	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,POWERING_HYBRIDS,RAMPING_HV,READY} ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STBY_CONF	!visible: 1
            do GO_NOTREADY all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_STBY_CONF
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when (  ( ( any_in FMD2_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD2_MODFMDSECTOR_FWSETSTATES not_in_state {READY,RAMPING_DOWN_HV,POWERING_DOWN_HYBRIDS,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD2_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  move_to STBY_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            do RESET all_in FMD2_MODFMD_RCU_FWSETACTIONS
            do RESET all_in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to MIXED

object: FMD2_MOD is_of_class FMD2_MODTOP_FmdPowerModule_CLASS

class: FMD2_MODFMDRingCooling_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_MODFMDRINGCOOLING_FWSETSTATES
            remove &VAL_OF_Device from FMD2_MODFMDRINGCOOLING_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_MODFMDRINGCOOLING_FWSETSTATES
            insert &VAL_OF_Device in FMD2_MODFMDRINGCOOLING_FWSETACTIONS
            move_to READY

object: FMD2_MODFMDRingCooling_FWDM is_of_class FMD2_MODFMDRingCooling_FwDevMode_CLASS


class: FMD2_MODFMDRingCooling_CLASS/associated
!panel: FMD_Panels/FMD_RingCooling.pnl
    parameters: int TimerStart = 0
    state: IGNORED	!color: FwStateAttention2
    state: NO_COMM	!color: FwStateAttention2
    state: NO_COMM_INT	!color: FwStateAttention1
    state: DISABLED	!color: FwStateOKNotPhysics
    state: DISABLED_INT	!color: FwStateAttention1
    state: OFF	!color: FwStateOKNotPhysics
    state: OFF_INT	!color: FwStateAttention1
    state: RUNNING	!color: FwStateOKPhysics
    state: ERROR	!color: FwStateAttention3

object: FMD2_Cooling is_of_class FMD2_MODFMDRingCooling_CLASS

objectset: FMD2_MODFMDRINGCOOLING_FWSETSTATES {FMD2_Cooling }
objectset: FMD2_MODFMDRINGCOOLING_FWSETACTIONS {FMD2_Cooling }

class: FMD2_MODFMD_RCU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_MODFMD_RCU_FWSETSTATES
            remove &VAL_OF_Device from FMD2_MODFMD_RCU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_MODFMD_RCU_FWSETSTATES
            insert &VAL_OF_Device in FMD2_MODFMD_RCU_FWSETACTIONS
            move_to READY

object: FMD2_MODFMD_RCU_FWDM is_of_class FMD2_MODFMD_RCU_FwDevMode_CLASS


objectset: FMD2_MODFMD_RCU_FWSETSTATES {FMD2_PW_RCU }
objectset: FMD2_MODFMD_RCU_FWSETACTIONS {FMD2_PW_RCU }

class: FMD2_MODFmdSector_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD2_MODFMDSECTOR_FWSETSTATES
            remove &VAL_OF_Device from FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD2_MODFMDSECTOR_FWSETSTATES
            insert &VAL_OF_Device in FMD2_MODFMDSECTOR_FWSETACTIONS
            move_to READY

object: FMD2_MODFmdSector_FWDM is_of_class FMD2_MODFmdSector_FwDevMode_CLASS


objectset: FMD2_MODFMDSECTOR_FWSETSTATES {FMD2_PW_INNER_TOP,
	FMD2_PW_INNER_BOTTOM,
	FMD2_PW_OUTER_TOP,
	FMD2_PW_OUTER_BOTTOM }
objectset: FMD2_MODFMDSECTOR_FWSETACTIONS {FMD2_PW_INNER_TOP,
	FMD2_PW_INNER_BOTTOM,
	FMD2_PW_OUTER_TOP,
	FMD2_PW_OUTER_BOTTOM }

class: FMD3_PW_RCUTOP_FMD_RCU_CLASS
!panel: FMD_Panels/FMD_RCU.pnl
    parameters: string runtype = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to OFF
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  )  move_to STANDBY
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  )  move_to INVALID_CONF
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state {READY,RUNNING} ) )  )  move_to READY
        when (  not (  (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  ) or (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  ) or (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  and  ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  ) or (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  ) or (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) ) and ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state {READY,RUNNING} ) )  )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state NO_LINK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state NO_LINK ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        action: GO_STANDBY	!visible: 1
            do GO_READY all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {NO_LINK,STARTUP,STANDBY} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to BOOTING
    state: BOOTING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {NO_LINK,STARTUP,STANDBY} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  move_to STANDBY
    state: STANDBY	!color: FwStateOKNotPhysics
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state STANDBY ) )  move_to ERROR
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to POWERING_DOWN
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(Tag = run_type) all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to DOWNLOADING
    state: POWERING_DOWN	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,NO_LINK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  )  do FORCE_NO_LINK
        when (  ( ( all_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state NO_LINK ) )  )  move_to OFF
        action: FORCE_NO_LINK	!visible: 0
            do FORCE_NO_LINK all_in FMD3_PW_RCUDIMRCU_FWSETACTIONS
    state: DOWNLOADING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,CONFIGURING,READY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {IDLE,CONFIGURING,DONE,INVALID_CONF,SKIPPED} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        when ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state INVALID_CONF ) )  move_to INVALID_CONF
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CONFIGURING
    state: INVALID_CONF	!color: FwStateAttention2
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        action: CLEAR	!visible: 1
            do ACKNOWLEDGE all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CLEARING_INVALID
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do ACKNOWLEDGE all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to RESETTING
    state: CLEARING_INVALID	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {INVALID_CONF,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  do CLEAR
        action: CLEAR	!visible: 1
            do RESET all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to SET_CLEAR
    state: RESETTING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,READY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {INVALID_CONF,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) )  do CONFIGURE
        action: CONFIGURE	!visible: 1
            do CONFIGURE(Tag = runtype) all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to DOWNLOADING
    state: CONFIGURING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {STANDBY,CONFIGURING,READY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state READY ) )  )  do UPLOAD
        action: UPLOAD	!visible: 1
            do UPLOAD(runtype = runtype) all_in FMD3_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to INIT_ZERO_SUP
    state: INIT_ZERO_SUP	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state {IDLE,UPLOADING,DONE,SKIPPED} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD3_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to UPLOADING
    state: UPLOADING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state IDLE ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES in_state IDLE ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {READY,RUNNING} ) )  move_to ERROR
        action: CLEAR	!visible: 1
            do RESET all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to SET_CLEAR
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(Tag = run_type) all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            set runtype = run_type
            move_to DOWNLOADING
    state: SET_CLEAR	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {RUNNING,READY,STANDBY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {IDLE,CONFIGURING,DONE,SKIPPED} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state {DONE,SKIPPED} ) )  do ACKNOWLEDGE
        action: ACKNOWLEDGE	!visible: 1
            do ACKNOWLEDGE all_in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to CLEARING
    state: CLEARING	!color: FwStateAttention1
        when ( ( any_in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES not_in_state READY ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMRCU_FWSETSTATES not_in_state {RUNNING,READY,STANDBY} ) )  move_to ERROR
        when ( ( any_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES not_in_state {DONE,SKIPPED,IDLE} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_RCUDIMMINICONF_FWSETSTATES in_state IDLE ) ) and
       ( ( all_in FMD3_PW_RCUDIMRCU_FWSETSTATES in_state STANDBY ) )  )  move_to STANDBY
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to MIXED

object: FMD3_PW_RCU is_of_class FMD3_PW_RCUTOP_FMD_RCU_CLASS

class: FMD3_PW_RCUFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_PW_RCUFwCaenChannelDcsLV_FWDM is_of_class FMD3_PW_RCUFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_PW_RCUFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_RCU:LvChannel000 is_of_class FMD3_PW_RCUFwCaenChannelDcsLV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_RCU:LvChannel001 is_of_class FMD3_PW_RCUFwCaenChannelDcsLV_CLASS

objectset: FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_RCU:LvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_RCU:LvChannel001 }
objectset: FMD3_PW_RCUFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_RCU:LvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_RCU:LvChannel001 }

class: FMD3_PW_RCUDIMRCU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_RCUDIMRCU_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_RCUDIMRCU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_RCUDIMRCU_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_RCUDIMRCU_FWSETACTIONS
            move_to READY

object: FMD3_PW_RCUDIMRCU_FWDM is_of_class FMD3_PW_RCUDIMRCU_FwDevMode_CLASS


class: FMD3_PW_RCUDIMRCU_CLASS/associated
!panel: FMD_Panels/FMD_DIMRCU.pnl
    parameters: int TimerStart = 0
    state: NO_LINK	!color: FwStateAttention2
        action: WAIT_FOR_STANDBY	!visible: 1
    state: STARTUP	!color: FwStateAttention1
        action: WAIT_FOR_STANDBY	!visible: 1
    state: STANDBY	!color: FwStateOKNotPhysics
        action: FORCE_NO_LINK	!visible: 1
        action: WAIT_FOR_READY	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: WAIT_FOR_READY	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: WAIT_FOR_STANDBY	!visible: 1
    state: MIXED	!color: FwStateAttention1
    state: ERROR	!color: FwStateAttention3
    state: RUNNING	!color: FwStateOKPhysics
    state: WAITING_FOR_STANDBY	!color: FwStateAttention1
    state: WAITING_FOR_READY	!color: FwStateAttention1
    state: UNKNOWN	!color: FwStateAttention3

object: RCU_3 is_of_class FMD3_PW_RCUDIMRCU_CLASS

objectset: FMD3_PW_RCUDIMRCU_FWSETSTATES {RCU_3 }
objectset: FMD3_PW_RCUDIMRCU_FWSETACTIONS {RCU_3 }

class: FMD3_PW_RCUDIMMiniconf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_RCUDIMMINICONF_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_RCUDIMMINICONF_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_RCUDIMMINICONF_FWSETACTIONS
            move_to READY

object: FMD3_PW_RCUDIMMiniconf_FWDM is_of_class FMD3_PW_RCUDIMMiniconf_FwDevMode_CLASS


class: FMD3_PW_RCUDIMMiniconf_CLASS/associated
!panel: FMD_Panels/FMD_Miniconf.pnl
    state: NO_LINK	!color: FwStateAttention2
    state: IDLE	!color: FwStateOKNotPhysics
        action: CONFIGURE(string Tag = "physics")	!visible: 1
        action: RESET	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: CLEAR	!visible: 1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
        action: CLEAR	!visible: 1
    state: INVALID_CONF	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: SKIPPED	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: CLEAR	!visible: 1
    state: UNKNOWN	!color: FwStateAttention3
        action: CLEAR	!visible: 1

object: Miniconf_3 is_of_class FMD3_PW_RCUDIMMiniconf_CLASS

objectset: FMD3_PW_RCUDIMMINICONF_FWSETSTATES {Miniconf_3 }
objectset: FMD3_PW_RCUDIMMINICONF_FWSETACTIONS {Miniconf_3 }

class: FMD3_PW_RCUDIMPedconf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_RCUDIMPEDCONF_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_RCUDIMPEDCONF_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_RCUDIMPEDCONF_FWSETACTIONS
            move_to READY

object: FMD3_PW_RCUDIMPedconf_FWDM is_of_class FMD3_PW_RCUDIMPedconf_FwDevMode_CLASS


class: FMD3_PW_RCUDIMPedconf_CLASS/associated
!panel: FMD_Panels/FMD_Miniconf.pnl
    state: NO_LINK	!color: FwStateAttention2
    state: IDLE	!color: FwStateOKNotPhysics
        action: UPLOAD(string runtype = "")	!visible: 1
    state: UPLOADING	!color: FwStateAttention1
        action: CLEAR	!visible: 1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
        action: CLEAR	!visible: 1
    state: SKIPPED	!color: FwStateAttention2
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: CLEAR	!visible: 1
    state: UNKNOWN	!color: FwStateAttention3
        action: CLEAR	!visible: 1

object: Pedconf_3 is_of_class FMD3_PW_RCUDIMPedconf_CLASS

objectset: FMD3_PW_RCUDIMPEDCONF_FWSETSTATES {Pedconf_3 }
objectset: FMD3_PW_RCUDIMPEDCONF_FWSETACTIONS {Pedconf_3 }

class: FMD3_3V3_INNER_TOPTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_3V3_INNER_TOP is_of_class FMD3_3V3_INNER_TOPTOP_FmdFEE3V3_CLASS

class: FMD3_3V3_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_3V3_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_3V3_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_3V3_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel000 is_of_class FMD3_3V3_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel000 }
objectset: FMD3_3V3_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel000 }

class: FMD3_2V5_INNER_TOPTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_2V5_INNER_TOP is_of_class FMD3_2V5_INNER_TOPTOP_FmdFEE2V5_CLASS

class: FMD3_2V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_2V5_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_2V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_2V5_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel001 is_of_class FMD3_2V5_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel001 }
objectset: FMD3_2V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel001 }

class: FMD3_1V5_INNER_TOPTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_1V5_INNER_TOP is_of_class FMD3_1V5_INNER_TOPTOP_FmdFEE1V5_CLASS

class: FMD3_1V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_1V5_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_1V5_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_1V5_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel002 is_of_class FMD3_1V5_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel002 }
objectset: FMD3_1V5_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel002 }

class: FMD3_M2V_INNER_TOPTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_M2V_INNER_TOP is_of_class FMD3_M2V_INNER_TOPTOP_FmdFEEM2V_CLASS

class: FMD3_M2V_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_M2V_INNER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_M2V_INNER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_M2V_INNER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel003 is_of_class FMD3_M2V_INNER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel003 }
objectset: FMD3_M2V_INNER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:LvChannel003 }

class: FMD3_PW_INNER_TOPTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD3_PW_INNER_TOP is_of_class FMD3_PW_INNER_TOPTOP_FmdSector_CLASS

class: FMD3_PW_INNER_TOPFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPFmdFEE3V3_FWDM is_of_class FMD3_PW_INNER_TOPFmdFEE3V3_FwDevMode_CLASS


objectset: FMD3_PW_INNER_TOPFMDFEE3V3_FWSETSTATES {FMD3_3V3_INNER_TOP }
objectset: FMD3_PW_INNER_TOPFMDFEE3V3_FWSETACTIONS {FMD3_3V3_INNER_TOP }

class: FMD3_PW_INNER_TOPFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPFmdFEE2V5_FWDM is_of_class FMD3_PW_INNER_TOPFmdFEE2V5_FwDevMode_CLASS


objectset: FMD3_PW_INNER_TOPFMDFEE2V5_FWSETSTATES {FMD3_2V5_INNER_TOP }
objectset: FMD3_PW_INNER_TOPFMDFEE2V5_FWSETACTIONS {FMD3_2V5_INNER_TOP }

class: FMD3_PW_INNER_TOPFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPFmdFEE1V5_FWDM is_of_class FMD3_PW_INNER_TOPFmdFEE1V5_FwDevMode_CLASS


objectset: FMD3_PW_INNER_TOPFMDFEE1V5_FWSETSTATES {FMD3_1V5_INNER_TOP }
objectset: FMD3_PW_INNER_TOPFMDFEE1V5_FWSETACTIONS {FMD3_1V5_INNER_TOP }

class: FMD3_PW_INNER_TOPFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPFmdFEEM2V_FWDM is_of_class FMD3_PW_INNER_TOPFmdFEEM2V_FwDevMode_CLASS


objectset: FMD3_PW_INNER_TOPFMDFEEM2V_FWSETSTATES {FMD3_M2V_INNER_TOP }
objectset: FMD3_PW_INNER_TOPFMDFEEM2V_FWSETACTIONS {FMD3_M2V_INNER_TOP }

class: FMD3_PW_INNER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPFwCaenChannelDcsHV_FWDM is_of_class FMD3_PW_INNER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD3_PW_INNER_TOPFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel000 is_of_class FMD3_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel001 is_of_class FMD3_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel002 is_of_class FMD3_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel003 is_of_class FMD3_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel004 is_of_class FMD3_PW_INNER_TOPFwCaenChannelDcsHV_CLASS

objectset: FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel004 }
objectset: FMD3_PW_INNER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_TOP:HvChannel004 }

class: FMD3_PW_INNER_TOPDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPDIMFEC_FWDM is_of_class FMD3_PW_INNER_TOPDIMFEC_FwDevMode_CLASS


class: FMD3_PW_INNER_TOPDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC3_I_U is_of_class FMD3_PW_INNER_TOPDIMFEC_CLASS

objectset: FMD3_PW_INNER_TOPDIMFEC_FWSETSTATES {FEC3_I_U }
objectset: FMD3_PW_INNER_TOPDIMFEC_FWSETACTIONS {FEC3_I_U }

class: FMD3_PW_INNER_TOPFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_TOPFmdLVMode_FWDM is_of_class FMD3_PW_INNER_TOPFmdLVMode_FwDevMode_CLASS


class: FMD3_PW_INNER_TOPFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD3IU_LVMode is_of_class FMD3_PW_INNER_TOPFmdLVMode_CLASS

objectset: FMD3_PW_INNER_TOPFMDLVMODE_FWSETSTATES {FMD3IU_LVMode }
objectset: FMD3_PW_INNER_TOPFMDLVMODE_FWSETACTIONS {FMD3IU_LVMode }

class: FMD3_3V3_INNER_BOTTOMTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_3V3_INNER_BOTTOM is_of_class FMD3_3V3_INNER_BOTTOMTOP_FmdFEE3V3_CLASS

class: FMD3_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_3V3_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_3V3_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel000 is_of_class FMD3_3V3_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel000 }
objectset: FMD3_3V3_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel000 }

class: FMD3_2V5_INNER_BOTTOMTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_2V5_INNER_BOTTOM is_of_class FMD3_2V5_INNER_BOTTOMTOP_FmdFEE2V5_CLASS

class: FMD3_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_2V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_2V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel001 is_of_class FMD3_2V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel001 }
objectset: FMD3_2V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel001 }

class: FMD3_1V5_INNER_BOTTOMTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_1V5_INNER_BOTTOM is_of_class FMD3_1V5_INNER_BOTTOMTOP_FmdFEE1V5_CLASS

class: FMD3_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_1V5_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_1V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel002 is_of_class FMD3_1V5_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel002 }
objectset: FMD3_1V5_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel002 }

class: FMD3_M2V_INNER_BOTTOMTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_M2V_INNER_BOTTOM is_of_class FMD3_M2V_INNER_BOTTOMTOP_FmdFEEM2V_CLASS

class: FMD3_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_M2V_INNER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_M2V_INNER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel003 is_of_class FMD3_M2V_INNER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel003 }
objectset: FMD3_M2V_INNER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:LvChannel003 }

class: FMD3_PW_INNER_BOTTOMTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD3_PW_INNER_BOTTOM is_of_class FMD3_PW_INNER_BOTTOMTOP_FmdSector_CLASS

class: FMD3_PW_INNER_BOTTOMFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMFmdFEE3V3_FWDM is_of_class FMD3_PW_INNER_BOTTOMFmdFEE3V3_FwDevMode_CLASS


objectset: FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETSTATES {FMD3_3V3_INNER_BOTTOM }
objectset: FMD3_PW_INNER_BOTTOMFMDFEE3V3_FWSETACTIONS {FMD3_3V3_INNER_BOTTOM }

class: FMD3_PW_INNER_BOTTOMFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMFmdFEE2V5_FWDM is_of_class FMD3_PW_INNER_BOTTOMFmdFEE2V5_FwDevMode_CLASS


objectset: FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETSTATES {FMD3_2V5_INNER_BOTTOM }
objectset: FMD3_PW_INNER_BOTTOMFMDFEE2V5_FWSETACTIONS {FMD3_2V5_INNER_BOTTOM }

class: FMD3_PW_INNER_BOTTOMFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMFmdFEE1V5_FWDM is_of_class FMD3_PW_INNER_BOTTOMFmdFEE1V5_FwDevMode_CLASS


objectset: FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETSTATES {FMD3_1V5_INNER_BOTTOM }
objectset: FMD3_PW_INNER_BOTTOMFMDFEE1V5_FWSETACTIONS {FMD3_1V5_INNER_BOTTOM }

class: FMD3_PW_INNER_BOTTOMFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMFmdFEEM2V_FWDM is_of_class FMD3_PW_INNER_BOTTOMFmdFEEM2V_FwDevMode_CLASS


objectset: FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETSTATES {FMD3_M2V_INNER_BOTTOM }
objectset: FMD3_PW_INNER_BOTTOMFMDFEEM2V_FWSETACTIONS {FMD3_M2V_INNER_BOTTOM }

class: FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_FWDM is_of_class FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel000 is_of_class FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel001 is_of_class FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel002 is_of_class FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel003 is_of_class FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel004 is_of_class FMD3_PW_INNER_BOTTOMFwCaenChannelDcsHV_CLASS

objectset: FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel004 }
objectset: FMD3_PW_INNER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_INNER_BOTTOM:HvChannel004 }

class: FMD3_PW_INNER_BOTTOMDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMDIMFEC_FWDM is_of_class FMD3_PW_INNER_BOTTOMDIMFEC_FwDevMode_CLASS


class: FMD3_PW_INNER_BOTTOMDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC3_I_D is_of_class FMD3_PW_INNER_BOTTOMDIMFEC_CLASS

objectset: FMD3_PW_INNER_BOTTOMDIMFEC_FWSETSTATES {FEC3_I_D }
objectset: FMD3_PW_INNER_BOTTOMDIMFEC_FWSETACTIONS {FEC3_I_D }

class: FMD3_PW_INNER_BOTTOMFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD3_PW_INNER_BOTTOMFmdLVMode_FWDM is_of_class FMD3_PW_INNER_BOTTOMFmdLVMode_FwDevMode_CLASS


class: FMD3_PW_INNER_BOTTOMFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD3ID_LVMode is_of_class FMD3_PW_INNER_BOTTOMFmdLVMode_CLASS

objectset: FMD3_PW_INNER_BOTTOMFMDLVMODE_FWSETSTATES {FMD3ID_LVMode }
objectset: FMD3_PW_INNER_BOTTOMFMDLVMODE_FWSETACTIONS {FMD3ID_LVMode }

class: FMD3_3V3_OUTER_TOPTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_3V3_OUTER_TOP is_of_class FMD3_3V3_OUTER_TOPTOP_FmdFEE3V3_CLASS

class: FMD3_3V3_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_3V3_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_3V3_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_3V3_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel000 is_of_class FMD3_3V3_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel000 }
objectset: FMD3_3V3_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel000 }

class: FMD3_2V5_OUTER_TOPTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_2V5_OUTER_TOP is_of_class FMD3_2V5_OUTER_TOPTOP_FmdFEE2V5_CLASS

class: FMD3_2V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_2V5_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_2V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_2V5_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel001 is_of_class FMD3_2V5_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel001 }
objectset: FMD3_2V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel001 }

class: FMD3_1V5_OUTER_TOPTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_1V5_OUTER_TOP is_of_class FMD3_1V5_OUTER_TOPTOP_FmdFEE1V5_CLASS

class: FMD3_1V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_1V5_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_1V5_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_1V5_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel002 is_of_class FMD3_1V5_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel002 }
objectset: FMD3_1V5_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel002 }

class: FMD3_M2V_OUTER_TOPTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_M2V_OUTER_TOP is_of_class FMD3_M2V_OUTER_TOPTOP_FmdFEEM2V_CLASS

class: FMD3_M2V_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_M2V_OUTER_TOPFwCaenChannelDcsLV_FWDM is_of_class FMD3_M2V_OUTER_TOPFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_M2V_OUTER_TOPFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel003 is_of_class FMD3_M2V_OUTER_TOPFwCaenChannelDcsLV_CLASS

objectset: FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel003 }
objectset: FMD3_M2V_OUTER_TOPFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:LvChannel003 }

class: FMD3_PW_OUTER_TOPTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD3_PW_OUTER_TOP is_of_class FMD3_PW_OUTER_TOPTOP_FmdSector_CLASS

class: FMD3_PW_OUTER_TOPFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPFmdFEE3V3_FWDM is_of_class FMD3_PW_OUTER_TOPFmdFEE3V3_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETSTATES {FMD3_3V3_OUTER_TOP }
objectset: FMD3_PW_OUTER_TOPFMDFEE3V3_FWSETACTIONS {FMD3_3V3_OUTER_TOP }

class: FMD3_PW_OUTER_TOPFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPFmdFEE2V5_FWDM is_of_class FMD3_PW_OUTER_TOPFmdFEE2V5_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETSTATES {FMD3_2V5_OUTER_TOP }
objectset: FMD3_PW_OUTER_TOPFMDFEE2V5_FWSETACTIONS {FMD3_2V5_OUTER_TOP }

class: FMD3_PW_OUTER_TOPFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPFmdFEE1V5_FWDM is_of_class FMD3_PW_OUTER_TOPFmdFEE1V5_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETSTATES {FMD3_1V5_OUTER_TOP }
objectset: FMD3_PW_OUTER_TOPFMDFEE1V5_FWSETACTIONS {FMD3_1V5_OUTER_TOP }

class: FMD3_PW_OUTER_TOPFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPFmdFEEM2V_FWDM is_of_class FMD3_PW_OUTER_TOPFmdFEEM2V_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETSTATES {FMD3_M2V_OUTER_TOP }
objectset: FMD3_PW_OUTER_TOPFMDFEEM2V_FWSETACTIONS {FMD3_M2V_OUTER_TOP }

class: FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_FWDM is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel000 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel001 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel002 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel003 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel004 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel005 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel006 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel007 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel008 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel009 is_of_class FMD3_PW_OUTER_TOPFwCaenChannelDcsHV_CLASS

objectset: FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel004,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel005,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel006,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel007,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel008,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel009 }
objectset: FMD3_PW_OUTER_TOPFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel004,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel005,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel006,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel007,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel008,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_TOP:HvChannel009 }

class: FMD3_PW_OUTER_TOPDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPDIMFEC_FWDM is_of_class FMD3_PW_OUTER_TOPDIMFEC_FwDevMode_CLASS


class: FMD3_PW_OUTER_TOPDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC3_O_U is_of_class FMD3_PW_OUTER_TOPDIMFEC_CLASS

objectset: FMD3_PW_OUTER_TOPDIMFEC_FWSETSTATES {FEC3_O_U }
objectset: FMD3_PW_OUTER_TOPDIMFEC_FWSETACTIONS {FEC3_O_U }

class: FMD3_PW_OUTER_TOPFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_TOPFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_TOPFmdLVMode_FWDM is_of_class FMD3_PW_OUTER_TOPFmdLVMode_FwDevMode_CLASS


class: FMD3_PW_OUTER_TOPFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD3OU_LVMode is_of_class FMD3_PW_OUTER_TOPFmdLVMode_CLASS

objectset: FMD3_PW_OUTER_TOPFMDLVMODE_FWSETSTATES {FMD3OU_LVMode }
objectset: FMD3_PW_OUTER_TOPFMDLVMODE_FWSETACTIONS {FMD3OU_LVMode }

class: FMD3_3V3_OUTER_BOTTOMTOP_FmdFEE3V3_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_3V3_OUTER_BOTTOM is_of_class FMD3_3V3_OUTER_BOTTOMTOP_FmdFEE3V3_CLASS

class: FMD3_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel000 is_of_class FMD3_3V3_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel000 }
objectset: FMD3_3V3_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel000 }

class: FMD3_2V5_OUTER_BOTTOMTOP_FmdFEE2V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_2V5_OUTER_BOTTOM is_of_class FMD3_2V5_OUTER_BOTTOMTOP_FmdFEE2V5_CLASS

class: FMD3_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel001 is_of_class FMD3_2V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel001 }
objectset: FMD3_2V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel001 }

class: FMD3_1V5_OUTER_BOTTOMTOP_FmdFEE1V5_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_1V5_OUTER_BOTTOM is_of_class FMD3_1V5_OUTER_BOTTOMTOP_FmdFEE1V5_CLASS

class: FMD3_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel002 is_of_class FMD3_1V5_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel002 }
objectset: FMD3_1V5_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel002 }

class: FMD3_M2V_OUTER_BOTTOMTOP_FmdFEEM2V_CLASS
!panel: FMD_Panels/FMD_LogicalVoltage.pnl
    state: MIXED	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_DW_OFF	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: GO_READY	!visible: 2
            do GO_READY all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 2
            do SWITCH_ON all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: RAMP_UP_READY	!color: FwStateAttention1
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
            do SWITCH_OFF all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 2
            do GO_OFF all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
            do RELOAD all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_OFF	!visible: 1
            do SWITCH_OFF all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
        action: SWITCH_ON	!visible: 1
            do SWITCH_ON all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: CHAN_FAULT	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state UNDEFINED ) )  move_to UNDEFINED
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state OFF ) )  move_to OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_DW_OFF ) )  move_to RAMP_DW_OFF
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state RAMP_UP_READY ) )  move_to RAMP_UP_READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state READY ) )  move_to READY
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to CHAN_FAULT
        action: RESET	!visible: 1
            do RESET all_in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS

object: FMD3_M2V_OUTER_BOTTOM is_of_class FMD3_M2V_OUTER_BOTTOMTOP_FmdFEEM2V_CLASS

class: FMD3_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS
            move_to READY

object: FMD3_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_FWDM is_of_class FMD3_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_FwDevMode_CLASS


class: FMD3_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel003 is_of_class FMD3_M2V_OUTER_BOTTOMFwCaenChannelDcsLV_CLASS

objectset: FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel003 }
objectset: FMD3_M2V_OUTER_BOTTOMFWCAENCHANNELDCSLV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:LvChannel003 }

class: FMD3_PW_OUTER_BOTTOMTOP_FmdSector_CLASS
!panel: FMD_Panels/FMD_Sector.pnl
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to POWERED
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  )  move_to RUNNING
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: POWER	!visible: 1
            do GO_READY all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            do GO_READY all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to POWERING
    state: POWERING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) )  )  move_to POWERED
    state: POWERED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        action: POWER_DOWN	!visible: 1
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to POWERING_DOWN
        action: START	!visible: 1
            do Inhibit_for_Active all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STARTING
    state: POWERING_DOWN	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: STARTING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) )  move_to STARTED_NO_MON
    state: STARTED_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: RUNNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state ACTIVE_OK ) )  move_to ERROR
        action: STOP	!visible: 1
            do Inhibit_for_Off all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to STOPPING_PREPARE
        action: GO_READY	!visible: 1
            do Inhibit_for_Ready all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_READY all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_HYBRIDS
        action: INHIBIT	!visible: 1
            do Inhibit all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to RUNNING_NO_MON
    state: STOPPING_PREPARE	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,OFF} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state OFF ) )  do STOP
        action: STOP	!visible: 1
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to STOPPING
    state: STOPPING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state OFF ) )  do STOP_MONITORS
        action: STOP_MONITORS	!visible: 1
            do Force_Off all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to POWERED
    state: RUNNING_NO_MON	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  move_to RUNNING
    state: POWERING_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,READY_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state READY_OK ) )  )  do RAMP_HV
        action: RAMP_HV	!visible: 0
            do GO_READY all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_HV
    state: RAMPING_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {OFF,RAMP_UP_READY,READY} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        action: GO_NOTREADY	!visible: 1
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to RAMPING_DOWN_HV
    state: RAMPING_DOWN_HV	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  move_to ERROR
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state OFF ) )  do POWER_DOWN_HYBRIDS
        action: POWER_DOWN_HYBRIDS	!visible: 0
            do Inhibit_for_Active all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to POWERING_DOWN_HYBRIDS
    state: POWERING_DOWN_HYBRIDS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF,OFF} ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {INHIBITED,ACTIVE_OK} ) )  move_to ERROR
        when (  ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ACTIVE_OK ) )  )  move_to RUNNING
    state: ERROR	!color: FwStateAttention3
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state READY_OK ) )  )  )  do RAMP_DOWN_HV
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {READY_OK,INHIBITED} ) )  )  )  do MOVE_RUNNING
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES not_in_state {ACTIVE_OK,READY_OK,INHIBITED} ) )  )  )  do MOVE_POWERED
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_1V5_OFF
        when (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES in_state READY ) ) and
       (  ( ( any_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES in_state ERROR ) )  )  )  do MOVE_3V3_OFF
        action: RESET	!visible: 1
            move_to MIXED
        action: RAMP_DOWN_HV	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to ERROR_HV_RAMPDOWN
        action: MOVE_RUNNING	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to ERROR_MOVE_RUNNING
        action: MOVE_POWERED	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to ERROR_MOVE_POWERED
        action: MOVE_1V5_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to ERROR_MOVE_1V5_OFF
        action: MOVE_3V3_OFF	!visible: 0
            do GO_OFF all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to ERROR_MOVE_3V3_OFF
    state: ERROR_HV_RAMPDOWN	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_RUNNING	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_POWERED	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_1V5_OFF	!color: FwStateAttention3
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR
    state: ERROR_MOVE_3V3_OFF	!color: 
        when ( ( all_in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES not_in_state {READY,RAMP_DW_OFF} ) )  move_to ERROR

object: FMD3_PW_OUTER_BOTTOM is_of_class FMD3_PW_OUTER_BOTTOMTOP_FmdSector_CLASS

class: FMD3_PW_OUTER_BOTTOMFmdFEE3V3_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMFmdFEE3V3_FWDM is_of_class FMD3_PW_OUTER_BOTTOMFmdFEE3V3_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETSTATES {FMD3_3V3_OUTER_BOTTOM }
objectset: FMD3_PW_OUTER_BOTTOMFMDFEE3V3_FWSETACTIONS {FMD3_3V3_OUTER_BOTTOM }

class: FMD3_PW_OUTER_BOTTOMFmdFEE2V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMFmdFEE2V5_FWDM is_of_class FMD3_PW_OUTER_BOTTOMFmdFEE2V5_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETSTATES {FMD3_2V5_OUTER_BOTTOM }
objectset: FMD3_PW_OUTER_BOTTOMFMDFEE2V5_FWSETACTIONS {FMD3_2V5_OUTER_BOTTOM }

class: FMD3_PW_OUTER_BOTTOMFmdFEE1V5_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMFmdFEE1V5_FWDM is_of_class FMD3_PW_OUTER_BOTTOMFmdFEE1V5_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETSTATES {FMD3_1V5_OUTER_BOTTOM }
objectset: FMD3_PW_OUTER_BOTTOMFMDFEE1V5_FWSETACTIONS {FMD3_1V5_OUTER_BOTTOM }

class: FMD3_PW_OUTER_BOTTOMFmdFEEM2V_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMFmdFEEM2V_FWDM is_of_class FMD3_PW_OUTER_BOTTOMFmdFEEM2V_FwDevMode_CLASS


objectset: FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETSTATES {FMD3_M2V_OUTER_BOTTOM }
objectset: FMD3_PW_OUTER_BOTTOMFMDFEEM2V_FWSETACTIONS {FMD3_M2V_OUTER_BOTTOM }

class: FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_FWDM is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_FwDevMode_CLASS


class: FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenHVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel000 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel001 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel002 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel003 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel004 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel005 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel006 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel007 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel008 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

object: FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel009 is_of_class FMD3_PW_OUTER_BOTTOMFwCaenChannelDcsHV_CLASS

objectset: FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETSTATES {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel004,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel005,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel006,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel007,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel008,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel009 }
objectset: FMD3_PW_OUTER_BOTTOMFWCAENCHANNELDCSHV_FWSETACTIONS {FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel000,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel001,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel002,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel003,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel004,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel005,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel006,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel007,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel008,
	FMD_DCS:FMD3_MOD:FMD3_PW_OUTER_BOTTOM:HvChannel009 }

class: FMD3_PW_OUTER_BOTTOMDIMFEC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMDIMFEC_FWDM is_of_class FMD3_PW_OUTER_BOTTOMDIMFEC_FwDevMode_CLASS


class: FMD3_PW_OUTER_BOTTOMDIMFEC_CLASS/associated
!panel: FMD_Panels/FMD_DIMFEC.pnl
    parameters: int TimerStart = 0, string Inhibit_Start = "", string Inhibit_End = ""
    state: OFF	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ACTIVE_OK	!color: FwStateOKNotPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: READY_OK	!color: FwStateOKPhysics
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: Inhibit	!visible: 1
        action: Inhibit_for_Off	!visible: 1
        action: Inhibit_for_Active	!visible: 1
        action: Inhibit_for_Ready	!visible: 1
        action: Force_Off	!visible: 1
    state: INHIBITED	!color: FwStateAttention1

object: FEC3_O_D is_of_class FMD3_PW_OUTER_BOTTOMDIMFEC_CLASS

objectset: FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETSTATES {FEC3_O_D }
objectset: FMD3_PW_OUTER_BOTTOMDIMFEC_FWSETACTIONS {FEC3_O_D }

class: FMD3_PW_OUTER_BOTTOMFmdLVMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDLVMODE_FWSETSTATES
            remove &VAL_OF_Device from FMD3_PW_OUTER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDLVMODE_FWSETSTATES
            insert &VAL_OF_Device in FMD3_PW_OUTER_BOTTOMFMDLVMODE_FWSETACTIONS
            move_to READY

object: FMD3_PW_OUTER_BOTTOMFmdLVMode_FWDM is_of_class FMD3_PW_OUTER_BOTTOMFmdLVMode_FwDevMode_CLASS


class: FMD3_PW_OUTER_BOTTOMFmdLVMode_CLASS/associated
!panel: FmdLVMode.pnl
    state: NORMAL	!color: FwStateOKPhysics
    state: DELAYED_COOLING	!color: FwStateAttention2
    state: NO_COOLING	!color: FwStateAttention2
    state: UNKNOWN	!color: FwStateAttention3

object: FMD3OD_LVMode is_of_class FMD3_PW_OUTER_BOTTOMFmdLVMode_CLASS

objectset: FMD3_PW_OUTER_BOTTOMFMDLVMODE_FWSETSTATES {FMD3OD_LVMode }
objectset: FMD3_PW_OUTER_BOTTOMFMDLVMODE_FWSETACTIONS {FMD3OD_LVMode }

class: FMD3_MODTOP_FmdPowerModule_CLASS
!panel: FmdPowerModule.pnl
    parameters: string run_type_tag = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state NO_CONTROL ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  )  move_to OFF
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state STANDBY ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  )  move_to STANDBY
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  )  move_to STBY_CONFIGURED
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  not (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES in_state MIXED ) ) or
           ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES in_state MIXED ) )  )  ) move_to ERROR
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {NO_CONTROL,OFF} ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  )  move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state {OFF,NO_CONTROL} ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {OFF,NO_CONTROL} ) )  )  move_to ERROR
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES in_state NO_CONTROL ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES in_state NO_CONTROL ) )  )  move_to NO_CONTROL
        action: GO_STANDBY	!visible: 1
            move_to CHECKING_COOLING
    state: CHECKING_COOLING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDRINGCOOLING_FWSETSTATES in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  do GO_STANDBY
        action: GO_STANDBY	!visible: 0
            do GO_STANDBY all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to POWERING_RCU
    state: POWERING_RCU	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state {OFF,POWERING,BOOTING,STANDBY} ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state STANDBY ) )  do POWER_SECTORS
        action: POWER_SECTORS	!visible: 0
            do POWER all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to POWERING_SECTORS
    state: POWERING_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {OFF,POWERING,POWERED} ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  move_to STANDBY
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state POWERED ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_OFF	!visible: 1
            do POWER_DOWN all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to POWERING_DOWN_SECTORS
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do START all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            set run_type_tag = run_type
            move_to STARTING_SECTORS
    state: POWERING_DOWN_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {POWERED,POWERING_DOWN,OFF} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state OFF ) )  do POWER_DOWN_RCU
        action: POWER_DOWN_RCU	!visible: 0
            do GO_OFF all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to POWERING_DOWN_RCU
    state: POWERING_DOWN_RCU	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state {STANDBY,POWERING_DOWN,OFF} ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state OFF ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state OFF ) )  move_to OFF
    state: STARTING_SECTORS	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {POWERED,STARTING,STARTED_NO_MON} ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state STARTED_NO_MON ) )  do CONFIGURE
        action: CONFIGURE	!visible: 0
            do CONFIGURE(run_type = run_type_tag) all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: CONFIGURING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state {STANDBY,DOWNLOADING,INVALID_CONF,RESETTING,CONFIGURING,INIT_ZERO_SUP,UPLOADING,READY} ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {STARTED_NO_MON,RUNNING,RUNNING_NO_MON} ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when (  ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  )  move_to STBY_CONFIGURED
        when ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state INVALID_CONF ) )  move_to INVALID_CONF
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state RUNNING ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do STOP all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            do CLEAR all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to CLEARING
        action: GO_READY	!visible: 1
            do GO_READY all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_READY
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do INHIBIT all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            do CONFIGURE(run_type = run_type) all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: INVALID_CONF	!color: FwStateAttention2
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state INVALID_CONF ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state RUNNING ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do CLEAR all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to CLEARING
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(run_type = run_type) all_in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to CONFIGURING
    state: CLEARING	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state {INVALID_CONF,CLEARING_INVALID,READY,SET_CLEAR,CLEARING,STANDBY} ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,STOPPING_PREPARE,STOPPING,POWERED} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMD_RCU_FWSETSTATES in_state STANDBY ) )  move_to MOVING_STANDBY
        action: STOP_SECTORS	!visible: 0
            do STOP all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_STANDBY
    state: MOVING_STANDBY	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state STANDBY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,STOPPING_PREPARE,STOPPING,POWERED} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state POWERED ) )  move_to STANDBY
    state: MOVING_READY	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {RUNNING,POWERING_HYBRIDS,RAMPING_HV,READY} ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state READY ) )  move_to READY
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_MODFMDRINGCOOLING_FWSETSTATES not_in_state {IGNORED,NO_COMM_INT,DISABLED_INT,OFF_INT,RUNNING} ) )  )  move_to ERROR
        action: GO_STBY_CONF	!visible: 1
            do GO_NOTREADY all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to MOVING_STBY_CONF
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when (  ( ( any_in FMD3_MODFMD_RCU_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMD3_MODFMDSECTOR_FWSETSTATES not_in_state {READY,RAMPING_DOWN_HV,POWERING_DOWN_HYBRIDS,RUNNING} ) )  )  move_to ERROR
        when ( ( all_in FMD3_MODFMDSECTOR_FWSETSTATES in_state RUNNING ) )  move_to STBY_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            do RESET all_in FMD3_MODFMD_RCU_FWSETACTIONS
            do RESET all_in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to MIXED

object: FMD3_MOD is_of_class FMD3_MODTOP_FmdPowerModule_CLASS

class: FMD3_MODFMDRingCooling_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_MODFMDRINGCOOLING_FWSETSTATES
            remove &VAL_OF_Device from FMD3_MODFMDRINGCOOLING_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_MODFMDRINGCOOLING_FWSETSTATES
            insert &VAL_OF_Device in FMD3_MODFMDRINGCOOLING_FWSETACTIONS
            move_to READY

object: FMD3_MODFMDRingCooling_FWDM is_of_class FMD3_MODFMDRingCooling_FwDevMode_CLASS


class: FMD3_MODFMDRingCooling_CLASS/associated
!panel: FMD_Panels/FMD_RingCooling.pnl
    parameters: int TimerStart = 0
    state: IGNORED	!color: FwStateAttention2
    state: NO_COMM	!color: FwStateAttention2
    state: NO_COMM_INT	!color: FwStateAttention1
    state: DISABLED	!color: FwStateOKNotPhysics
    state: DISABLED_INT	!color: FwStateAttention1
    state: OFF	!color: FwStateOKNotPhysics
    state: OFF_INT	!color: FwStateAttention1
    state: RUNNING	!color: FwStateOKPhysics
    state: ERROR	!color: FwStateAttention3

object: FMD3_Cooling is_of_class FMD3_MODFMDRingCooling_CLASS

objectset: FMD3_MODFMDRINGCOOLING_FWSETSTATES {FMD3_Cooling }
objectset: FMD3_MODFMDRINGCOOLING_FWSETACTIONS {FMD3_Cooling }

class: FMD3_MODFMD_RCU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_MODFMD_RCU_FWSETSTATES
            remove &VAL_OF_Device from FMD3_MODFMD_RCU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_MODFMD_RCU_FWSETSTATES
            insert &VAL_OF_Device in FMD3_MODFMD_RCU_FWSETACTIONS
            move_to READY

object: FMD3_MODFMD_RCU_FWDM is_of_class FMD3_MODFMD_RCU_FwDevMode_CLASS


objectset: FMD3_MODFMD_RCU_FWSETSTATES {FMD3_PW_RCU }
objectset: FMD3_MODFMD_RCU_FWSETACTIONS {FMD3_PW_RCU }

class: FMD3_MODFmdSector_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD3_MODFMDSECTOR_FWSETSTATES
            remove &VAL_OF_Device from FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD3_MODFMDSECTOR_FWSETSTATES
            insert &VAL_OF_Device in FMD3_MODFMDSECTOR_FWSETACTIONS
            move_to READY

object: FMD3_MODFmdSector_FWDM is_of_class FMD3_MODFmdSector_FwDevMode_CLASS


objectset: FMD3_MODFMDSECTOR_FWSETSTATES {FMD3_PW_INNER_TOP,
	FMD3_PW_INNER_BOTTOM,
	FMD3_PW_OUTER_TOP,
	FMD3_PW_OUTER_BOTTOM }
objectset: FMD3_MODFMDSECTOR_FWSETACTIONS {FMD3_PW_INNER_TOP,
	FMD3_PW_INNER_BOTTOM,
	FMD3_PW_OUTER_TOP,
	FMD3_PW_OUTER_BOTTOM }

class: FmdPowerModule_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMDPOWERMODULE_FWSETSTATES
            remove &VAL_OF_Device from FMDPOWERMODULE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMDPOWERMODULE_FWSETSTATES
            insert &VAL_OF_Device in FMDPOWERMODULE_FWSETACTIONS
            move_to READY

object: FmdPowerModule_FWDM is_of_class FmdPowerModule_FwDevMode_CLASS


objectset: FMDPOWERMODULE_FWSETSTATES {FMD1_MOD,
	FMD2_MOD,
	FMD3_MOD }
objectset: FMDPOWERMODULE_FWSETACTIONS {FMD1_MOD,
	FMD2_MOD,
	FMD3_MOD }

class: TOP_FMD_DCS_CLASS
!panel: FMD_Panels/FMD_DCS.pnl
    parameters: string run_type_tag = ""
    state: MIXED	!color: FwStateAttention1
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state NO_CONTROL ) )  )  move_to OFF
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STANDBY ) )  )  move_to STANDBY
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STBY_CONFIGURED ) )  )  move_to STBY_CONFIGURED
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state MIXED ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES not_in_state MIXED ) )  )  move_to ERROR
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state OFF ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state NO_CONTROL ) )  )  move_to ERROR
        action: GO_STANDBY	!visible: 1
            do GO_READY all_in FMD_INFRASTRUCTURE_FWSETACTIONS
            move_to MOVING_STANDBY
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: MOVING_STANDBY	!color: FwStateAttention1
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state OFF ) )  )  do TURN_ON_POWER_MODS
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STANDBY ) )  )  move_to STANDBY
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state {OFF,ENABLING_RAMPDOWN,ENABLING_LV,READY} ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {NO_CONTROL,OFF,CHECKING_COOLING,POWERING_RCU,POWERING_SECTORS,STANDBY} ) )  )  move_to ERROR
        action: TURN_ON_POWER_MODS	!visible: 0
            do GO_STANDBY all_in FMDPOWERMODULE_FWSETACTIONS
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state STANDBY ) )  )  move_to ERROR
        action: GO_OFF	!visible: 1
            do GO_OFF all_in FMDPOWERMODULE_FWSETACTIONS
            move_to MOVING_OFF
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(run_type = run_type) all_in FMDPOWERMODULE_FWSETACTIONS
            set run_type_tag = run_type
            move_to DOWNLOADING
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: MOVING_OFF	!color: FwStateAttention1
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state READY ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state OFF ) )  )  do TURN_OFF_POWER_ENABLE
        when (  ( ( all_in FMD_INFRASTRUCTURE_FWSETSTATES in_state OFF ) ) and
       ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state NO_CONTROL ) )  )  move_to OFF
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state {READY,DISABLING_LV,DISABLING_RAMPDOWN,OFF} ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {STANDBY,POWERING_DOWN_SECTORS,POWERING_DOWN_RCU,OFF,NO_CONTROL} ) )  )  move_to ERROR
        action: TURN_OFF_POWER_ENABLE	!visible: 0
            do GO_OFF all_in FMD_INFRASTRUCTURE_FWSETACTIONS
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: DOWNLOADING	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {STANDBY,STARTING_SECTORS,CONFIGURING,STBY_CONFIGURED,INVALID_CONF} ) ) and
       ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state IDLE ) )  )  do SET_RCT_ERROR
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {STANDBY,STARTING_SECTORS,CONFIGURING,STBY_CONFIGURED,INVALID_CONF} ) ) and
       ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state {DONE,ERROR} ) )  )  do ACK_RCT_ERROR
        when (  ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STBY_CONFIGURED ) ) and
       ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state IDLE ) )  )  do SET_RCT_OK
        when (  ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STBY_CONFIGURED ) ) and
       ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state {DONE,ERROR} ) )  )  do ACK_RCT_OK
        when (  ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state {STBY_CONFIGURED,INVALID_CONF} ) ) and
       ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state IDLE ) )  )  do SET_RCT_IGNORE
        when (  ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state {STBY_CONFIGURED,INVALID_CONF} ) ) and
       ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state {DONE,ERROR} ) )  )  do ACK_RCT_IGNORE
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
        action: SET_RCT_OK	!visible: 0
            do SET_OK(run_type = run_type_tag) all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: ACK_RCT_OK	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
            move_to STBY_CONFIGURED
        action: SET_RCT_IGNORE	!visible: 0
            do SET_IGNORE(run_type = run_type_tag) all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: ACK_RCT_IGNORE	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
            do GO_STANDBY all_in FMDPOWERMODULE_FWSETACTIONS
            move_to CLEARING
        action: SET_RCT_ERROR	!visible: 0
            do SET_ERROR(run_type = run_type_tag) all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: ACK_RCT_ERROR	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
            move_to ERROR
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state STBY_CONFIGURED ) )  )  move_to ERROR
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state SKIP_CONFIG ) )  do SET_RCT_OK
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state CONFIGURE ) )  do CONFIGURE_ELECTRONICS
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state {DONE,ERROR} ) )  do ACK_RCT
        action: GO_STANDBY	!visible: 1
            do GO_STANDBY all_in FMDPOWERMODULE_FWSETACTIONS
            move_to CLEARING
        action: GO_READY	!visible: 1
            do GO_READY all_in FMDPOWERMODULE_FWSETACTIONS
            move_to MOVING_READY
        action: GO_BEAM_TUN	!visible: 1
            move_to MOVING_BEAM_TUN
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CHECK_CONFIG(run_type = run_type) all_in ALIRCT_INTERFACE_FWSETACTIONS
            set run_type_tag = run_type
        action: SET_RCT_OK	!visible: 0
            do SET_OK(run_type = run_type_tag) all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: CONFIGURE_ELECTRONICS	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
            do CONFIGURE(run_type = run_type_tag) all_in FMDPOWERMODULE_FWSETACTIONS
            move_to DOWNLOADING
        action: ACK_RCT	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: FORCE_CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(run_type = run_type) all_in FMDPOWERMODULE_FWSETACTIONS
            set run_type_tag = run_type
            move_to DOWNLOADING
        action: SOR(string run_type = "",string run_no = "")	!visible: 0
            do SOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
    state: CLEARING	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {STBY_CONFIGURED,CLEARING,MOVING_STANDBY,STANDBY} ) )  )  move_to ERROR
        when ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STANDBY ) )  move_to STANDBY
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: MOVING_READY	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {STBY_CONFIGURED,MOVING_READY,READY} ) )  )  move_to ERROR
        when ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state READY ) )  move_to READY
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state READY ) )  )  move_to ERROR
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state SKIP_CONFIG ) )  do SET_RCT_OK
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state CONFIGURE ) )  do SET_RCT_NOT_CONFIGURABLE
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state {DONE,ERROR} ) )  do ACK_RCT
        action: GO_STBY_CONF	!visible: 1
            do GO_STBY_CONF all_in FMDPOWERMODULE_FWSETACTIONS
            move_to MOVING_STBY_CONF
        action: GO_BEAM_TUN	!visible: 1
            do GO_STBY_CONF all_in FMDPOWERMODULE_FWSETACTIONS
            move_to MOVING_BEAM_TUN
        action: SOR(string run_type = "",string run_no = "")	!visible: 0
            do SOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CHECK_CONFIG(run_type = run_type) all_in ALIRCT_INTERFACE_FWSETACTIONS
            set run_type_tag = run_type
        action: SET_RCT_OK	!visible: 0
            do SET_OK(run_type = run_type_tag) all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: SET_RCT_NOT_CONFIGURABLE	!visible: 0
            do SET_NOT_CONFIGURABLE all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: ACK_RCT	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {READY,MOVING_STBY_CONF,STBY_CONFIGURED} ) )  )  move_to ERROR
        when ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STBY_CONFIGURED ) )  move_to STBY_CONFIGURED
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state STBY_CONFIGURED ) )  )  move_to ERROR
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state SKIP_CONFIG ) )  do SET_RCT_OK
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state CONFIGURE ) )  do CONFIGURE_ELECTRONICS
        when ( ( all_in ALIRCT_INTERFACE_FWSETSTATES in_state {DONE,ERROR} ) )  do ACK_RCT
        action: GO_STANDBY	!visible: 1
            do GO_STANDBY all_in FMDPOWERMODULE_FWSETACTIONS
            move_to CLEARING
        action: GO_READY	!visible: 1
            do GO_READY all_in FMDPOWERMODULE_FWSETACTIONS
            move_to MOVING_READY
        action: GO_STBY_CONF	!visible: 1
            move_to MOVING_STBY_CONF
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
        action: CONFIGURE(string run_type = "physics")	!visible: 1
            do CHECK_CONFIG(run_type = run_type) all_in ALIRCT_INTERFACE_FWSETACTIONS
            set run_type_tag = run_type
        action: SET_RCT_OK	!visible: 0
            do SET_OK(run_type = run_type_tag) all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: CONFIGURE_ELECTRONICS	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
            do CONFIGURE(run_type = run_type_tag) all_in FMDPOWERMODULE_FWSETACTIONS
            move_to DOWNLOADING
        action: ACK_RCT	!visible: 0
            do ACKNOWLEDGE all_in ALIRCT_INTERFACE_FWSETACTIONS
        action: FORCE_CONFIGURE(string run_type = "physics")	!visible: 1
            do CONFIGURE(run_type = run_type) all_in FMDPOWERMODULE_FWSETACTIONS
            set run_type_tag = run_type
            move_to DOWNLOADING
        action: SOR(string run_type = "",string run_no = "")	!visible: 0
            do SOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when (  ( ( any_in FMD_INFRASTRUCTURE_FWSETSTATES not_in_state READY ) ) or
       ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state {READY,MOVING_STBY_CONF,STBY_CONFIGURED} ) )  )  move_to ERROR
        when ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state STBY_CONFIGURED ) )  move_to BEAM_TUNING
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            do RESET all_in FMDPOWERMODULE_FWSETACTIONS
            move_to MIXED
        action: EOR(string run_type = "",string run_no = "")	!visible: 0
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCS_RUNCU_FWSETACTIONS
        action: ACK_RUN_FAILURE	!visible: 0
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS

object: FMD_DCS is_of_class TOP_FMD_DCS_CLASS

class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  move_to Incomplete
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDev ) )  move_to IncompleteDev
        when ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm,LockedOutPerm} ) )  move_to Incomplete
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
        when (  ( ( all_in FWCHILDMODE_FWSETSTATES in_state {Included,ExcludedPerm,LockedOutPerm} ) ) and
       ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state Incomplete ) )  )  move_to Complete
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) ) and
       ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDev ) )  ) move_to Complete
        when (  ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm,LockedOutPerm} ) ) or
       ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  )  move_to Incomplete

object: FMD_DCS_FWCNM is_of_class FwChildrenMode_CLASS

class: ASS_FwChildrenMode_CLASS/associated
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1

object: FMD_DCS_RCT::FMD_DCS_RCT_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD_DCS_RUN::FMD_DCS_RUN_FWCNM is_of_class ASS_FwChildrenMode_CLASS

objectset: FWCHILDRENMODE_FWSETSTATES {FMD_DCS_RCT::FMD_DCS_RCT_FWCNM,
	FMD_DCS_RUN::FMD_DCS_RUN_FWCNM }

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            do Include(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: ReleaseAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded

object: FMD_DCS_FWM is_of_class FwMode_CLASS

class: ASS_FwMode_CLASS/associated
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Manual	!visible: 0
        action: Ignore	!visible: 0
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
        action: Manual(string OWNER = "")	!visible: 0
        action: Ignore(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
        action: ReleaseAll(string OWNER = "")	!visible: 1
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "")	!visible: 1
        action: Exclude(string OWNER = "")	!visible: 0
        action: Ignore	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
        action: SetInLocal	!visible: 0
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
        action: Exclude(string OWNER = "")	!visible: 0
        action: Manual	!visible: 0
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0

object: FMD_DCS_RCT::FMD_DCS_RCT_FWM is_of_class ASS_FwMode_CLASS

object: FMD_DCS_RUN::FMD_DCS_RUN_FWM is_of_class ASS_FwMode_CLASS

class: FMD_DCS_RCT_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Excluded )  move_to EXCLUDED

        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Ignored )  move_to IGNORED

        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Manual )  move_to MANUAL

        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state Included ) then
                if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                else
                    if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                        remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                        remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state Included ) then
                if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state Included ) then
                if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                else
                    if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                        remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                        remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
              insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
              insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            endif
              if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RCT::FMD_DCS_RCT_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            !move_to Manual
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RCT::FMD_DCS_RCT_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
              remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
              remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            !endif
            !move_to Manual
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RCT::FMD_DCS_RCT_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RCT::FMD_DCS_RCT_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Included )  move_to INCLUDED

        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Excluded ) move_to EXCLUDED

        when ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state Included ) then
                if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
            remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                    remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETSTATES
                remove FMD_DCS_RCT::FMD_DCS_RCT from ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RCT::FMD_DCS_RCT_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RCT::FMD_DCS_RCT_FWM
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETSTATES
                insert FMD_DCS_RCT::FMD_DCS_RCT in ALIRCT_INTERFACE_FWSETACTIONS
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: FMD_DCS_RCT_FWM is_of_class FMD_DCS_RCT_FwChildMode_CLASS

class: FMD_DCS_RUN_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Excluded )  move_to EXCLUDED

        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Ignored )  move_to IGNORED

        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Manual )  move_to MANUAL

        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state Included ) then
                if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                else
                    if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                        remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                        remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state Included ) then
                if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state Included ) then
                if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                else
                    if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                        remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                        remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
              insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
              insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            endif
              if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RUN::FMD_DCS_RUN_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            !move_to Manual
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RUN::FMD_DCS_RUN_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
              remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
              remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            !endif
            !move_to Manual
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RUN::FMD_DCS_RUN_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal FMD_DCS_RUN::FMD_DCS_RUN_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Included )  move_to INCLUDED

        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Excluded ) move_to EXCLUDED

        when ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state Included ) then
                if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove FMD_DCS_RUN::FMD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( FMD_DCS_RUN::FMD_DCS_RUN_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS_RUN::FMD_DCS_RUN_FWM
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert FMD_DCS_RUN::FMD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: FMD_DCS_RUN_FWM is_of_class FMD_DCS_RUN_FwChildMode_CLASS

objectset: FWCHILDMODE_FWSETSTATES {FMD_DCS_RCT_FWM,
	FMD_DCS_RUN_FWM }
objectset: FWCHILDMODE_FWSETACTIONS {FMD_DCS_RCT_FWM,
	FMD_DCS_RUN_FWM }

class: ASS_AliRct_Interface_CLASS/associated
!panel: AliRct_Interface.pnl
    state: IDLE	!color: FwStateOKNotPhysics
        action: SET_OK(string run_type = "")	!visible: 1
        action: SET_IGNORE(string run_type = "")	!visible: 1
        action: SET_ERROR(string run_type = "")	!visible: 1
        action: SET_NOT_CONFIGURABLE	!visible: 1
        action: CHECK_CONFIG(string run_type = "")	!visible: 1
    state: CHECKING_CONFIG	!color: FwStateAttention1
    state: SKIP_CONFIG	!color: FwStateOKNotPhysics
        action: SET_OK(string run_type = "")	!visible: 1
        action: SET_IGNORE(string run_type = "")	!visible: 1
        action: SET_ERROR(string run_type = "")	!visible: 1
        action: SET_NOT_CONFIGURABLE	!visible: 1
        action: CHECK_CONFIG(string run_type = "")	!visible: 1
        action: ACKNOWLEDGE	!visible: 1
    state: CONFIGURE	!color: FwStateOKNotPhysics
        action: SET_OK(string run_type = "")	!visible: 1
        action: SET_IGNORE(string run_type = "")	!visible: 1
        action: SET_ERROR(string run_type = "")	!visible: 1
        action: SET_NOT_CONFIGURABLE	!visible: 1
        action: CHECK_CONFIG(string run_type = "")	!visible: 1
        action: ACKNOWLEDGE	!visible: 1
    state: SETTING	!color: FwStateAttention1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: ACKNOWLEDGE	!visible: 1

object: FMD_DCS_RCT::FMD_DCS_RCT is_of_class ASS_AliRct_Interface_CLASS

objectset: ALIRCT_INTERFACE_FWSETSTATES
objectset: ALIRCT_INTERFACE_FWSETACTIONS

class: ASS_AliDcs_runCU_CLASS/associated
!panel: aliDcsRunUnit/aliDcsRunCU.pnl
    parameters: string run_no = "uninitialized"
    state: RUN_OK	!color: FwStateOKPhysics
        action: SOR(string run_type = "",string run_no = "")	!visible: 1
        action: EOR(string run_type = "",string run_no = "")	!visible: 1
        action: INHIBIT_RUN	!visible: 1
    state: SOR_PROGRESSING	!color: FwStateAttention1
    state: EOR_PROGRESSING	!color: FwStateAttention1
    state: SOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: EOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: RUN_INHIBIT	!color: FwStateOKNotPhysics
        action: EOR(string run_type = "",string run_no = "")	!visible: 1
        action: ALLOW_RUN	!visible: 1

object: FMD_DCS_RUN::FMD_DCS_RUN is_of_class ASS_AliDcs_runCU_CLASS

objectset: ALIDCS_RUNCU_FWSETSTATES
objectset: ALIDCS_RUNCU_FWSETACTIONS

class: FMD_INFRASTRUCTURE_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FMD_INFRASTRUCTURE_FWSETSTATES
            remove &VAL_OF_Device from FMD_INFRASTRUCTURE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FMD_INFRASTRUCTURE_FWSETSTATES
            insert &VAL_OF_Device in FMD_INFRASTRUCTURE_FWSETACTIONS
            move_to READY

object: FMD_INFRASTRUCTURE_FWDM is_of_class FMD_INFRASTRUCTURE_FwDevMode_CLASS


objectset: FMD_INFRASTRUCTURE_FWSETSTATES {FMD_INFRASTRUCTURE }
objectset: FMD_INFRASTRUCTURE_FWSETACTIONS {FMD_INFRASTRUCTURE }

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
!panel: FwDevMode.pnl
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD_DCS:FMD1_1V5_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_1V5_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_2V5_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_2V5_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_3V3_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_3V3_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_M2V_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_M2V_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_MOD_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_PW_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_PW_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD1_PW_RCU_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_1V5_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_1V5_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_1V5_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_1V5_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_2V5_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_2V5_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_2V5_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_2V5_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_3V3_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_3V3_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_3V3_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_3V3_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_M2V_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_M2V_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_M2V_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_M2V_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_MOD_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_PW_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_PW_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_PW_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_PW_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD2_PW_RCU_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_1V5_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_1V5_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_1V5_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_1V5_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_2V5_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_2V5_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_2V5_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_2V5_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_3V3_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_3V3_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_3V3_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_3V3_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_M2V_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_M2V_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_M2V_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_M2V_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_MOD_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_PW_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_PW_INNER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_PW_OUTER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_PW_OUTER_TOP_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD3_PW_RCU_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD_INFRASTRUCTURE_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS:FMD_PW_A3486_FWDM is_of_class FwDevMode_CLASS

object: FMD_DCS_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD_DCS:FMD1_1V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_1V5_INNER_TOP_FWDM,
	FMD_DCS:FMD1_2V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_2V5_INNER_TOP_FWDM,
	FMD_DCS:FMD1_3V3_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_3V3_INNER_TOP_FWDM,
	FMD_DCS:FMD1_M2V_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_M2V_INNER_TOP_FWDM,
	FMD_DCS:FMD1_MOD_FWDM,
	FMD_DCS:FMD1_PW_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_PW_INNER_TOP_FWDM,
	FMD_DCS:FMD1_PW_RCU_FWDM,
	FMD_DCS:FMD2_1V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_1V5_INNER_TOP_FWDM,
	FMD_DCS:FMD2_1V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_1V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_2V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_2V5_INNER_TOP_FWDM,
	FMD_DCS:FMD2_2V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_2V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_3V3_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_3V3_INNER_TOP_FWDM,
	FMD_DCS:FMD2_3V3_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_3V3_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_M2V_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_M2V_INNER_TOP_FWDM,
	FMD_DCS:FMD2_M2V_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_M2V_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_MOD_FWDM,
	FMD_DCS:FMD2_PW_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_PW_INNER_TOP_FWDM,
	FMD_DCS:FMD2_PW_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_PW_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_PW_RCU_FWDM,
	FMD_DCS:FMD3_1V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_1V5_INNER_TOP_FWDM,
	FMD_DCS:FMD3_1V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_1V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_2V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_2V5_INNER_TOP_FWDM,
	FMD_DCS:FMD3_2V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_2V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_3V3_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_3V3_INNER_TOP_FWDM,
	FMD_DCS:FMD3_3V3_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_3V3_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_M2V_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_M2V_INNER_TOP_FWDM,
	FMD_DCS:FMD3_M2V_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_M2V_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_MOD_FWDM,
	FMD_DCS:FMD3_PW_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_PW_INNER_TOP_FWDM,
	FMD_DCS:FMD3_PW_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_PW_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_PW_RCU_FWDM,
	FMD_DCS:FMD_INFRASTRUCTURE_FWDM,
	FMD_DCS:FMD_PW_A3486_FWDM,
	FMD_DCS_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD_DCS:FMD1_1V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_1V5_INNER_TOP_FWDM,
	FMD_DCS:FMD1_2V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_2V5_INNER_TOP_FWDM,
	FMD_DCS:FMD1_3V3_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_3V3_INNER_TOP_FWDM,
	FMD_DCS:FMD1_M2V_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_M2V_INNER_TOP_FWDM,
	FMD_DCS:FMD1_MOD_FWDM,
	FMD_DCS:FMD1_PW_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD1_PW_INNER_TOP_FWDM,
	FMD_DCS:FMD1_PW_RCU_FWDM,
	FMD_DCS:FMD2_1V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_1V5_INNER_TOP_FWDM,
	FMD_DCS:FMD2_1V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_1V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_2V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_2V5_INNER_TOP_FWDM,
	FMD_DCS:FMD2_2V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_2V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_3V3_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_3V3_INNER_TOP_FWDM,
	FMD_DCS:FMD2_3V3_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_3V3_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_M2V_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_M2V_INNER_TOP_FWDM,
	FMD_DCS:FMD2_M2V_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_M2V_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_MOD_FWDM,
	FMD_DCS:FMD2_PW_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD2_PW_INNER_TOP_FWDM,
	FMD_DCS:FMD2_PW_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD2_PW_OUTER_TOP_FWDM,
	FMD_DCS:FMD2_PW_RCU_FWDM,
	FMD_DCS:FMD3_1V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_1V5_INNER_TOP_FWDM,
	FMD_DCS:FMD3_1V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_1V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_2V5_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_2V5_INNER_TOP_FWDM,
	FMD_DCS:FMD3_2V5_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_2V5_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_3V3_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_3V3_INNER_TOP_FWDM,
	FMD_DCS:FMD3_3V3_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_3V3_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_M2V_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_M2V_INNER_TOP_FWDM,
	FMD_DCS:FMD3_M2V_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_M2V_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_MOD_FWDM,
	FMD_DCS:FMD3_PW_INNER_BOTTOM_FWDM,
	FMD_DCS:FMD3_PW_INNER_TOP_FWDM,
	FMD_DCS:FMD3_PW_OUTER_BOTTOM_FWDM,
	FMD_DCS:FMD3_PW_OUTER_TOP_FWDM,
	FMD_DCS:FMD3_PW_RCU_FWDM,
	FMD_DCS:FMD_INFRASTRUCTURE_FWDM,
	FMD_DCS:FMD_PW_A3486_FWDM,
	FMD_DCS_FWDM }

