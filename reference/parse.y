%token SLASH
%token ASSOCIATED
%token IS_OF_CLASS
%token IDENTIFIER
%token INITIAL_STATE
%token DEAD_STATE
%token WHEN
%token DO
%token IF
%token THEN
%token ELSE
%token ENDIF
%token LPAREN
%token RPAREN
%token COMMA
%token AND
%token OR
%token NOT
%token DOT
%token IN_STATE
%token NOT_IN_STATE
%token CLASS
%token COLON
%token OBJECT
%token DCOLON
%token SUBOBJECT
%token ACTION
%token LBRACE
%token RBRACE
%token LITERAL
%token IDENTIFIER
%token STATE
%token EQUAL

%%

/* ---------------------------------------------------------------- 
 * Definition of a program 
 */
program 
	: decls
	;

decls
	: 
	| decl
	| decl decls
	;

decl    
	: class_def
	| object_def
        ;


/* ---------------------------------------------------------------- 
 * Definition of a class 
 */
class_def
	: class_decl
        | class_decl logical_object_descr
        | class_decl associated_class_spec
	;

associated_class_spec
        : SLASH ASSOCIATED associated_object_descr
	;

/* ---------------------------------------------------------------- 
 * Definition of an object 
 */
object_def
	: internal_object_def
	| external_object_def
	;

internal_object_def
	: internal_object_decl 
	| internal_object_decl logical_object_spec 
        | internal_object_decl associated_object_spec
	;


external_object_def 
        : external_object_decl
        | associated_object_spec
	;

logical_object_spec
	: reference_to_class
	| logical_object_descr
	; 

associated_object_spec
	: reference_to_class 
	| SLASH ASSOCIATED associated_object_descr
	;

reference_to_class
	: IS_OF_CLASS IDENTIFIER

/* ---------------------------------------------------------------- 
 * Description of an object 
 */
logical_object_descr
	: logical_states
	;

logical_states
	: logical_state_def
	| logical_state_def logical_states
	;

associated_object_descr
	: associated_subobject_defs
	| associated_state_defs
	;

associated_state_defs
	: associated_state_def
	| associated_state_def associated_state_defs
	;

/* ---------------------------------------------------------------- 
 * Definition of a sub-object 
 */
associated_subobject_defs
	: associated_subobject_def
	| associated_subobject_def associated_subobject_defs
	;

associated_subobject_def
	: subobject_decl 
	| subobject_decl associated_state_defs
	;

/* ---------------------------------------------------------------- 
 * Definition of a state
 */
logical_state_def
	: state_decl SLASH INITIAL_STATE when_instructions logical_action_defs
	| state_decl when_instructions logical_action_defs
	;

associated_state_def
	: state_decl SLASH DEAD_STATE action_decls 
	| state_decl action_decls 
	;

/* ---------------------------------------------------------------- 
 * Definition of an action
 */
logical_action_defs
	: logical_action_def
	| logical_action_def logical_action_defs
	;

logical_action_def
	: action_decl standard_instructions

action_decls 
	: action_decl
	| action_decl action_decls
	;
	     

/* ---------------------------------------------------------------- 
 * Definition of an instruction
 */
when_instructions
	: when_instruction 
	| when_instruction when_instructions
	;

when_instruction
	: WHEN condition DO IDENTIFIER
	;

standard_instructions
	: standard_instruction
	| standard_instruction standard_instructions
	;


standard_instruction
	: do_instruction
	| IF condition THEN standard_instructions ELSE standard_instructions ENDIF
	| IF condition THEN standard_instructions ENDIF
	;

do_instruction
	: DO IDENTIFIER LPAREN parameters RPAREN object_reference
	| DO IDENTIFIER object_reference
	;

parameters
	: parameter 
	| parameter COMMA parameters
	;

	
/* ---------------------------------------------------------------- 
 * Logical expressions
 */
condition
	: factor
	| factor AND factor 
	| factor OR factor 
	;

factor	: term
	| NOT term
	; 

term	: object_reference
	| object_reference DOT IDENTIFIER IN_STATE state_list
	| object_reference DOT IDENTIFIER NOT_IN_STATE state_list
	| condition
	;

/* ---------------------------------------------------------------- 
 * Declarations
 */
class_decl: 	      CLASS COLON IDENTIFIER
internal_object_decl: OBJECT COLON IDENTIFIER
external_object_decl: OBJECT COLON IDENTIFIER DCOLON IDENTIFIER
subobject_decl:	      SUBOBJECT COLON IDENTIFIER
state_decl:	      STATE COLON IDENTIFIER
action_decl
	: ACTION COLON IDENTIFIER
	| ACTION COLON LPAREN parameter_decls RPAREN
	;

parameter_decls
	: parameter_decl
	| parameter_decl COMMA parameter_decls
	;

parameter_decl
	: IDENTIFIER
	| IDENTIFIER EQUAL LITERAL
	;



/* ---------------------------------------------------------------- 
 * Syntatic elements
 */
state_list
	: IDENTIFIER 
	| LBRACE identifier_list RBRACE
	;

identifier_list
	: IDENTIFIER 
	| IDENTIFIER COMMA identifier_list
	;
 
parameter
	: IDENTIFIER EQUAL IDENTIFIER
	| IDENTIFIER EQUAL LITERAL
	;

object_reference
	: IDENTIFIER
	| IDENTIFIER DCOLON IDENTIFIER
	;

%%
