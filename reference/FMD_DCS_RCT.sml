class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) )  ) move_to Complete

object: FMD_DCS_RCT_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded

object: FMD_DCS_RCT_FWM is_of_class FwMode_CLASS

class: AliRct_Interface_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from ALIRCT_INTERFACE_FWSETSTATES
            remove &VAL_OF_Device from ALIRCT_INTERFACE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in ALIRCT_INTERFACE_FWSETSTATES
            insert &VAL_OF_Device in ALIRCT_INTERFACE_FWSETACTIONS
            move_to READY

object: AliRct_Interface_FWDM is_of_class AliRct_Interface_FwDevMode_CLASS


class: TOP_AliRct_Interface_CLASS/associated
!panel: AliRct_Interface.pnl
    state: IDLE	!color: FwStateOKNotPhysics
        action: SET_OK(string run_type = "")	!visible: 1
        action: SET_IGNORE(string run_type = "")	!visible: 1
        action: SET_ERROR(string run_type = "")	!visible: 1
        action: SET_NOT_CONFIGURABLE	!visible: 1
        action: CHECK_CONFIG(string run_type = "")	!visible: 1
    state: CHECKING_CONFIG	!color: FwStateAttention1
    state: SKIP_CONFIG	!color: FwStateOKNotPhysics
        action: SET_OK(string run_type = "")	!visible: 1
        action: SET_IGNORE(string run_type = "")	!visible: 1
        action: SET_ERROR(string run_type = "")	!visible: 1
        action: SET_NOT_CONFIGURABLE	!visible: 1
        action: CHECK_CONFIG(string run_type = "")	!visible: 1
        action: ACKNOWLEDGE	!visible: 1
    state: CONFIGURE	!color: FwStateOKNotPhysics
        action: SET_OK(string run_type = "")	!visible: 1
        action: SET_IGNORE(string run_type = "")	!visible: 1
        action: SET_ERROR(string run_type = "")	!visible: 1
        action: SET_NOT_CONFIGURABLE	!visible: 1
        action: CHECK_CONFIG(string run_type = "")	!visible: 1
        action: ACKNOWLEDGE	!visible: 1
    state: SETTING	!color: FwStateAttention1
    state: DONE	!color: FwStateOKNotPhysics
        action: ACKNOWLEDGE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: ACKNOWLEDGE	!visible: 1

object: FMD_DCS_RCT is_of_class TOP_AliRct_Interface_CLASS

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
!panel: FwDevMode.pnl
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD_DCS_RCT_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD_DCS_RCT_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD_DCS_RCT_FWDM }

