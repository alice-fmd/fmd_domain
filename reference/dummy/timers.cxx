#include <dim/dic.hxx>
#include <smixx/smirtl.hxx>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <pthread.h>
#include <Options.h>
#include <cstdarg>
#include <cstdio>

//____________________________________________________________________
void Info(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  char buf[1024];
  vsnprintf(buf, 1024, msg, ap);
  std::cout << buf << std::endl;
  va_end(ap);
}

void* timer(void*);

//____________________________________________________________________
struct TIMER : public SmiProxy
{
  friend void* timer(void* obj);
  /** Stat type */
  enum State_t { 
    /** Idle state */
    Idle = 1, 
    /** Running state */
    Running, 
    /** timeout state */
    Timeout
  };
    
  /** 
   * Constructor 
   * 
   * @param timeout Timeout in miliseconds
   */
  TIMER(const std::string& name, 
	unsigned int       timeout=5, 
	const std::string& domain=std::string("FMD_DCS"))
    : SmiProxy(const_cast<char*>(name.c_str())), 
      _name(name),
      _timeout(timeout),
      _stopped(false)
  {
    attach(const_cast<char*>(domain.c_str()), 
	   const_cast<char*>(name.c_str()));
    setVolatile();
    SetState(Idle);
  }
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  const char* State2Text(State_t s) const
  {
    switch (s) {
    case Idle:        return "IDLE";
    case Running:     return "RUNNING";
    case Timeout:     return "TIMEOUT";
    default:          return "IDLE";
    }
    return "IDLE";
  }
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  State_t State()
  {
    std::string s(getState());
    if      (s == "IDLE")    return Idle;
    else if (s == "RUNNING") return Running;
    else if (s == "TIMEOUT") return Timeout;
    return Idle;
  }
  /** 
   * Set the state from enumeration value
   * 
   * @param s New state
   */  
  void SetState(State_t s) 
  {
    Info("%s: Setting state to %s", _name.c_str(), State2Text(s));
    setState(const_cast<char*>(State2Text(s)));
  }
  /** 
   * Handle an SMI command (action) from state machine
   * 
   */
  virtual void smiCommandHandler()
  {
    Info("%s: Got action %s in state %s", _name.c_str(), getAction(), getState());
    const char* acts[] = { "START", "STOP", "RESET" };
    if      (testAction(const_cast<char*>(acts[0])))  Start();
    else if (testAction(const_cast<char*>(acts[1])))  Stop();
    else if (testAction(const_cast<char*>(acts[2])))  Reset();
  }
  /** 
   * Handle the start action
   * 
   */  
  void Start()
  {
    // Ignore start commands when running 
    if (State() == Running) { 
      SetState(State());
      return;
    }
    
    // Set the state to running 
    SetState(Running);
    _stopped = false;
    
    // Sleep for specified number miliseconds
    Info("%s: Starting thread", _name.c_str());
    pthread_create(&_thread, 0, &timer, this);
  }
  /** 
   * Handle the stop action
   * 
   */  
  void Stop()
  {
    // Ignore start commands when not running 
    // if (State() != Running) { 
    // SetState(State());
    // return;
    // }
    
    // Set state to idle
    _stopped = true;
    SetState(Idle);
  }
  /** 
   * Handle the reset action
   * 
   */
  void Reset()
  {
    // Ignore start commands when not running 
    if (State() != Timeout) { 
      SetState(State());
      return;
    }
    
    // Set state to idle
    _stopped = true;
    SetState(Idle);
  }
  /** 
   * Whether to be verbose or not
   * 
   * @param verb 
   */  
  void SetVerbose(bool verb) 
  {
    if (verb) setPrintOn();
    else      setPrintOff();
  }
  /** 
   * Run timer in separate thread
   * 
   */
  void Run()
  {
    unsigned int i = 0;
    time_t start = time(NULL);
    Info("Starting timer %s (%d ms) on %s", 
	 _name.c_str(), _timeout, asctime(localtime(&start)));
    while (i < _timeout && !_stopped) { 
      dtq_sleep(1); // usleep(100);
      i++;
    }
    time_t stop = time(NULL);
    Info("Stopping timer %s (after %d s) on %s", 
	 _name.c_str(), stop - start, 
	 asctime(localtime(&stop)));

    // Set state to timeout. 
    State_t s = !_stopped ? Timeout : Idle;
    Info("%s: After %d s: %s", _name.c_str(), i, State2Text(s));
    SetState(s);
  }
  
  /** Name */
  const std::string _name;
  /** The timeout - in miliseconds */
  unsigned int _timeout;
  /** Stopped */
  bool _stopped;
  /** Our thread */ 
  pthread_t _thread;
};

//____________________________________________________________________
void* timer(void* obj)
{
  if (!obj) return 0;
  TIMER* t = static_cast<TIMER*>(obj);
  t->Run();
  return 0;
}

//____________________________________________________________________
bool
readConfig(const std::string&        filename, 
	   std::vector<std::string>& names, 
	   std::vector<int>&         timeouts)
{
  std::ifstream file(filename.c_str());
  if (!file) { 
    std::cerr << "Failed to open file '" << filename << "'" << std::endl;
    return false;
  }
  
  while (!file.eof()) {
    std::string line;
    std::getline(file, line);
    if (file.bad()) return false;
    
    std::string name;
    int         timeout = 3;
    size_t      i = 0;
    for (; i < line.size(); i++) {
      if (isspace(line[i])) continue;
      if (line[i] == ',') break;
      name.push_back(line[i]);
    }
    if (i >= line.size()) 
      std::cout << "No timeout given for " << name << " using 3seconds" << std::endl;
    else {
      std::stringstream s(line.substr(i+1));
      s >> timeout;
    }
    std::cout << "Timeout for " << name << " set to " << timeout << std::endl;
    names.push_back(name);
    timeouts.push_back(timeout);
  }
  return true;
}

//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h', "help",    "Show this help", false,false);
  Option<bool>         VOpt('V', "version", "Show version number",false,false);
  Option<bool>         vOpt('v', "verbose", "Be verbose", false, false);
  Option<std::string>  fOpt('f', "file",    "CSV file with timers and delays", "timers.dat");
  Option<std::string>  dOpt('d', "dns",     "DNS host", "localhost");
  Option<std::string>  DOpt('D', "domain",  "SMI Domain", "FMD_DCS");
  
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(DOpt);
  cl.Add(fOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { 
    cl.Help();
    return 0;
  }
  
  std::cout << "DIM DNS node: " << dOpt.Value() << std::endl;
  DimClient::setDnsNode(dOpt.Value().c_str());
  DimServer::setDnsNode(dOpt.Value().c_str());

  std::vector<std::string> names;
  std::vector<int>         timeouts;
  if (!readConfig(fOpt.Value(), names, timeouts)) return 1;
  
  for (size_t i = 0; i < names.size(); i++) {
    TIMER* t = new TIMER(names[i], timeouts[i], DOpt.Value());
    t->SetVerbose(vOpt.Value());
  }
  while (true) pause();

  return 0;
}
//____________________________________________________________________
//
// EOF
//
  
