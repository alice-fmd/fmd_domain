class: TOP_AliDcs_runCU_CLASS
!panel: aliDcsRunUnit/aliDcsRunCU.pnl
    parameters: string run_no = "uninitialized"
    state: RUN_OK	!color: FwStateOKPhysics
        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_PROGRESSING ) )  move_to SOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_PROGRESSING ) )  move_to EOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_FAILURE ) )  move_to SOR_FAILURE

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_FAILURE ) )  move_to EOR_FAILURE

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_INHIBIT ) )  move_to RUN_INHIBIT

        action: SOR(string run_type = "",string run_no = "")	!visible: 1
            set run_no = run_no
            do SOR(run_type = run_type, run_no = run_no) all_in ALIDCSRUN_FWSETACTIONS
        action: EOR(string run_type = "",string run_no = "")	!visible: 1
            set run_no = run_no
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCSRUN_FWSETACTIONS
        action: INHIBIT_RUN	!visible: 1
            do INHIBIT_RUN all_in ALIDCSRUN_FWSETACTIONS
            move_to RUN_INHIBIT
    state: SOR_PROGRESSING	!color: FwStateAttention1
        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_PROGRESSING ) )  stay_in_state

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_PROGRESSING ) )  move_to EOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_FAILURE ) )  move_to SOR_FAILURE

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_FAILURE ) )  move_to EOR_FAILURE

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_INHIBIT ) )  move_to RUN_INHIBIT

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_OK ) )  move_to RUN_OK

    state: EOR_PROGRESSING	!color: FwStateAttention1
        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_PROGRESSING ) )  stay_in_state

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_PROGRESSING ) )  move_to SOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_FAILURE ) )  move_to SOR_FAILURE

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_FAILURE ) )  move_to EOR_FAILURE

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_INHIBIT ) )  move_to RUN_INHIBIT

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_OK ) )  move_to RUN_OK

    state: SOR_FAILURE	!color: FwStateAttention3
        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_FAILURE ) )  stay_in_state

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_PROGRESSING ) )  move_to EOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_PROGRESSING ) )  move_to SOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_FAILURE ) )  move_to EOR_FAILURE

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_INHIBIT ) )  move_to RUN_INHIBIT

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_OK ) )  move_to RUN_OK

        action: RESET	!visible: 1
            do RESET all_in ALIDCSRUN_FWSETACTIONS
    state: EOR_FAILURE	!color: FwStateAttention3
        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_FAILURE ) )  stay_in_state

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_FAILURE ) )  move_to SOR_FAILURE

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_PROGRESSING ) )  move_to EOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_PROGRESSING ) )  move_to SOR_PROGRESSING

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_INHIBIT ) )  move_to RUN_INHIBIT

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_OK ) )  move_to RUN_OK

        action: RESET	!visible: 1
            do RESET all_in ALIDCSRUN_FWSETACTIONS
    state: RUN_INHIBIT	!color: FwStateOKNotPhysics
        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_INHIBIT ) )  stay_in_state

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_FAILURE ) )  move_to EOR_FAILURE

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_FAILURE ) )  move_to SOR_FAILURE

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state EOR_PROGRESSING ) )  move_to EOR_PROGRESSING

        when ( ( any_in ALIDCSRUN_FWSETSTATES in_state SOR_PROGRESSING ) )  move_to SOR_PROGRESSING

        when ( ( all_in ALIDCSRUN_FWSETSTATES in_state RUN_OK ) )  move_to RUN_OK

        action: EOR(string run_type = "",string run_no = "")	!visible: 1
            set run_no = run_no
            do EOR(run_type = run_type, run_no = run_no) all_in ALIDCSRUN_FWSETACTIONS
        action: ALLOW_RUN	!visible: 1
            do ALLOW_RUN all_in ALIDCSRUN_FWSETACTIONS
            move_to RUN_OK

object: FMD_DCS_RUN is_of_class TOP_AliDcs_runCU_CLASS

class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) )  ) move_to Complete

object: FMD_DCS_RUN_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded

object: FMD_DCS_RUN_FWM is_of_class FwMode_CLASS

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
!panel: FwDevMode.pnl
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD_DCS_RUN_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD_DCS_RUN_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD_DCS_RUN_FWDM }

class: AliDcsRun_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from ALIDCSRUN_FWSETSTATES
            remove &VAL_OF_Device from ALIDCSRUN_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in ALIDCSRUN_FWSETSTATES
            insert &VAL_OF_Device in ALIDCSRUN_FWSETACTIONS
            move_to READY

object: AliDcsRun_FWDM is_of_class AliDcsRun_FwDevMode_CLASS


class: AliDcsRun_CLASS/associated
!panel: aliDcsRunUnit/aliDcsRunDU.pnl
    state: RUN_OK	!color: FwStateOKPhysics
        action: SOR(string run_type = "",string run_no = "")	!visible: 1
        action: EOR(string run_type = "",string run_no = "")	!visible: 1
        action: INHIBIT_RUN	!visible: 1
    state: SOR_PROGRESSING	!color: FwStateAttention1
    state: EOR_PROGRESSING	!color: FwStateAttention1
    state: EOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: SOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: RUN_INHIBIT	!color: FwStateOKNotPhysics
        action: EOR(string run_type = "",string run_no = "")	!visible: 1
        action: ALLOW_RUN	!visible: 1

object: aliDcsRun_1 is_of_class AliDcsRun_CLASS

objectset: ALIDCSRUN_FWSETSTATES {aliDcsRun_1 }
objectset: ALIDCSRUN_FWSETACTIONS {aliDcsRun_1 }

