#include "RCU_client.h"
#include <sstream>
#include <iomanip>

//____________________________________________________________________
RCU_client::RCU_client(const std::string& server, const std::string& domain)
  : SmiProxy(const_cast<char*>(MakeObjectName())), 
    _state(const_cast<char*>(MakeServiceName(server, "RCU_STATE")), 
	   0, 0, 0, this), 
    _afl(const_cast<char*>(MakeServiceName(server, "AFL")), 
	 0, 0, 0, this), 
    _value(NoLink), 
    _active(0)
{
  attach(const_cast<char*>(domain.c_str()), 
	 const_cast<char*>(MakeObjectName()), 1);
  setVolatile();
  setState(const_cast<char*>(State2Text()));
}
//____________________________________________________________________
const char* 
RCU_client::State2Text()
{
  switch (_value) {
  case Idle:        return "IDLE";          // 1
  case Standby:     return "STANDBY";       // 2
  case Downloading: return "DOWNLOADING";   // 3
  case Ready:       return "READY";         // 4
  case Error:       return "ERROR";         // 5
  case Mixed:       return "MIXED";         // 6
  case Running:     return "RUNNING";       // 7
  default:          return "OFF";
  }
  return "OFF";
}
//____________________________________________________________________
void
RCU_client::infoHandler()
{
  DimInfo* info = getInfo();
  if (!info) return;
  if (info == &_state) HandleState();
  if (info == &_afl)   HandleActive();
}
/** 
 * Handle upstream state changes 
 * 
 */
//____________________________________________________________________
void
RCU_client::HandleState() 
{ 
  DimInfo* info   = getInfo();
  void*    data   = info->getData();
  int      size   = info->getSize();
  bool     nolink =  (!data || size <= 0);
  int      val    = NoLink;
  if (!nolink) memcpy(&val, data, sizeof(int));
    
  State_t  next = NoLink;
  switch (val) { 
  case NoLink:      next = NoLink;      break; // 0
  case Idle:        next = Idle;        break; // 1
  case Standby:     next = Standby;     break; // 2
  case Downloading: next = Downloading; break; // 3
  case Ready:       next = Ready;       break; // 4
  case Error:       next = Error;       break; // 5
  case Mixed:       next = Mixed;       break; // 6
  case Running:     next = Running;     break; // 7
  }    
  // Set new SMI state
  // Info("FMD_RCU: Now in state %s", State2Text());
  if (_value != next) { 
    _value = next;
    setState(const_cast<char*>(State2Text()));
  }
}
//____________________________________________________________________
void
RCU_client::HandleActive() 
{ 
  DimInfo* info   = getInfo();
  void*    data   = info->getData();
  int      size   = info->getSize();
  bool     nolink =  (!data || size <= 0);
  if (nolink) _active = 0;
  else        memcpy(&_active, data, sizeof(int));
}
//____________________________________________________________________
void
RCU_client::SetVerbose(bool verb) 
{
  if (verb) setPrintOn();
  else      setPrintOff();
}
//____________________________________________________________________
const char* 
RCU_client::MakeServiceName(const std::string& server, 
			    const char*        what)
{
  static std::string name;
  std::stringstream s;
  s << server << "_" << what;
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
//
// EOF
//
