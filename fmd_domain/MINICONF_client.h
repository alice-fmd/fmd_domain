// -*- mode: C++ -*-
#ifndef MINICONF_CLIENT_H
#define MINICONF_CLIENT_H
#include <CONF_client.h>
#include <sstream>
class FMD_domain;

void Info(const char* msg, ...); 
void Error(const char* msg, ...);
void Fatal(const char* msg, ...);

#if 0
#include <smixx/smiuirtl.hxx>
struct FEC_Lock : public SmiObject
{
  FEC_Lock(const std::string& domain, unsigned int n);
  void smiStateChangeHandler();
  bool fIncluded;
};
#else 
struct FEC_Lock;
#endif
    
//____________________________________________________________________
class MINICONF_client : public CONF_client
{
public:
  /** 
   * Constructor
   * 
   * @param server Server name
   * 
   */
  MINICONF_client(const std::string& server, const std::string& domain);
  /** 
   * Handle the configure action
   * 
   */
  void HandleSafety();
  /** 
   * Handle the configure action
   * 
   */
  void HandleConfigure();
protected:
  unsigned int ExtractFec(const std::string& cur);
  /** 
   * Parse card parameter
   * 
   * @param args  Stream to write to
   * @param value Value 
   * @param def   Default value
   */
  void ParseCards(std::stringstream& args, 
		  const std::string& value, 
		  unsigned int def);
#if 0
  FEC_Lock fFec00;
  FEC_Lock fFec01;
  FEC_Lock fFec16;
  FEC_Lock fFec17;
#endif
};



#endif
//____________________________________________________________________
//
// EOF
//
