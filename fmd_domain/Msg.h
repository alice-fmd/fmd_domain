// -*- mode: C++ -*- 
#ifndef FMD_DOMAIN_MSG
#define FMD_DOMAIN_MSG

#ifdef USE_INFOLOGGER
# define LOG_FACILITY "FMD-DCS"
extern "C" {
# include <infoLogger.h>
# define MY_INFO(X)  do{INFO(X);  std::cout << "I: "<<(X)<< std::endl;}while(0)
# define MY_ERROR(X) do{ERROR(X); std::cout << "E: "<<(X)<< std::endl;}while(0)
# define MY_FATAL(X) do{FATAL(X); std::cout << "F: "<<(X)<< std::endl;}while(0)
}
#else
# define MY_INFO(X)  do { std::cout << "I: " << (X) << std::endl; } while (0)
# define MY_ERROR(X) do { std::cerr << "E: " << (X) << std::endl; } while (0)
# define MY_FATAL(X) do { std::cerr << "F: " << (X) << std::endl; } while (0)
#endif
#include <cstdarg>
#include <cstdio>
#include <iostream>

//____________________________________________________________________
void Msg(int type, const char* format, va_list ap)
{
  static char buf[1024];
  vsnprintf(buf, 1024, format, ap);
  switch (type) { 
  case 2:  MY_ERROR(buf); break;
  case 3:  MY_FATAL(buf); break;
  default: MY_INFO(buf);  break;
  }
}
//____________________________________________________________________
void Info(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  Msg(1, msg, ap);
  va_end(ap);
}
//____________________________________________________________________
void Error(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  Msg(2, msg, ap);
  va_end(ap);
}
//____________________________________________________________________
void Fatal(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  Msg(3, msg, ap);
  va_end(ap);
}
#endif
//
// EOF
//
