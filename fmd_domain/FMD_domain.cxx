#include <iostream>
#include <PEDCONF_client.h>
#include <MINICONF_client.h>
#include <RCU_client.h>
#include <FEC_client.h>
#include <TIMER.h>
#include <Options.h>
#include <Msg.h>


//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h',"help",     "Show this help", false,false);
  Option<bool>         VOpt('V',"version",  "Show version number", false,false);
  Option<bool>         vOpt('v', "verbose", "Be verbose", false, false);
  Option<unsigned>     tOpt('t', "rcu-timeout", "RCU Timeout in msecs", 8);
  Option<unsigned>     TOpt('T', "fec-timeout", "FEC Timeout in msecs", 60);
  Option<std::string>  dOpt('d', "dns",     "DNS host", "localhost");
  Option<std::string>  DOpt('D', "domain",  "SMI Domain", "FMD_DCS");
  Option<std::string>  pOpt('p', "pedname", "PedConf name", "FMD-FEE_0_0_1");
  Option<std::string>  mOpt('m', "mininame","MiniConf name", "FMD-FEE_0_0_1");
  Option<std::string>  nOpt('n', "name",    "Server name", "FMD-FEE_0_0_1");
  
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(DOpt);
  cl.Add(nOpt);
  cl.Add(mOpt);
  cl.Add(pOpt);
  cl.Add(tOpt);
  cl.Add(TOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { 
    cl.Help();
    return 0;
  }
  
  std::cout << "DIM DNS node: " << dOpt.Value() << std::endl;
  DimClient::setDnsNode(dOpt.Value().c_str());
  DimServer::setDnsNode(dOpt.Value().c_str());

  Info("Starting %s proxy", DOpt.Value().c_str());
  RCU_client      r(nOpt.Value(), DOpt.Value());
  FEC_client      f00(nOpt.Value(),  0, DOpt.Value());
  FEC_client      f01(nOpt.Value(),  1, DOpt.Value());
  FEC_client      f16(nOpt.Value(), 16, DOpt.Value());
  FEC_client      f17(nOpt.Value(), 17, DOpt.Value());
  MINICONF_client m(mOpt.Value(), DOpt.Value());
  PEDCONF_client  p(pOpt.Value(), DOpt.Value());
  TIMER           tr("RCU_TIMER", tOpt.Value(), DOpt.Value());
  TIMER           t00("FMD_FEC_00_TIMER", TOpt.Value(), DOpt.Value());
  TIMER           t01("FMD_FEC_01_TIMER", TOpt.Value(), DOpt.Value());
  TIMER           t16("FMD_FEC_16_TIMER", TOpt.Value(), DOpt.Value());
  TIMER           t17("FMD_FEC_17_TIMER", TOpt.Value(), DOpt.Value());
  r.SetVerbose(vOpt.Value());
  m.SetVerbose(vOpt.Value());
  p.SetVerbose(vOpt.Value());
  tr.SetVerbose(vOpt.Value());
  t00.SetVerbose(vOpt.Value());
  t01.SetVerbose(vOpt.Value());
  t16.SetVerbose(vOpt.Value());
  t17.SetVerbose(vOpt.Value());
  f00.SetVerbose(vOpt.Value());
  f01.SetVerbose(vOpt.Value());
  f16.SetVerbose(vOpt.Value());
  f17.SetVerbose(vOpt.Value());

  while (true) pause();

  Info("Stopping FMD DCS proxy");
  return 0;
}
//____________________________________________________________________
//
// EOF
//

