// -*- mode: C++ -*-
#ifndef PEDCONF_CLIENT_H
#define PEDCONF_CLIENT_H
#include <CONF_client.h>


//____________________________________________________________________
class PEDCONF_client : public CONF_client
{
public:
  /** 
   * Constructor
   * 
   * @param server 
   * 
   */
  PEDCONF_client(const std::string& server, const std::string& domain)
    : CONF_client("PEDCONF", server, domain)
  {}
  /** 
   * Handle the configure action
   * 
   */
  void HandleConfigure()
  {
#if 0
    std::stringstream args;
    ParseOption(args, "check",     GetParameter("CHECK"),     true);
    ParseOption(args, "verify",    GetParameter("VERIFY"),    true);
    std::string a = args.str();
#endif
    std::string a(GetParameter("ARGS"));
    Info("Sending configuration parameters \"%s\" to PEDCONF", a.c_str());
    SendCommand(a);
  }  
protected:
};
#endif
//____________________________________________________________________
//
// EOF
//
