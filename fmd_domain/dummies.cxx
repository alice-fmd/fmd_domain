#include <dim/dis.hxx>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <Options.h>

//====================================================================
namespace 
{
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}


//====================================================================
struct dummy : public DimCommandHandler
{
  enum { 
    kIdle = 1, 
    kConfiguring, 
    kError, 
    kDone
  };
    
  dummy(const char* prefix, const char* name) 
    : fState(0), 
      fGo(0)
  {
    std::string sname;
    std::string cname;
    MakeServiceName(prefix, name, "/STATE", sname);
    MakeServiceName(prefix, name, "/GO",    cname);
    const char* cfmt = "C";

    fState = new DimService(sname.c_str(), const_cast<char*>(fgIdle));
    fGo    = new DimCommand(cname.c_str(), const_cast<char*>(cfmt), this);
  }
  virtual ~dummy()
  {
    if (fState) delete fState;
    if (fGo)    delete fGo;
  }
  
  void MakeServiceName(const char*  prefix, 
		       const char*  name, 
		       const char*  what, 
		       std::string& n)
  {
    std::stringstream s;
    if (prefix && prefix[0] != '\0') s << prefix << "_";
    if (name   && name[0]   != '\0') s << name;
    s << what;
    n = s.str();
  }
  void SetState(unsigned int s)
  {
    if (!fState) return;
    const char* newValue = 0;
    switch (s) { 
    case kIdle:        newValue = fgIdle;        break;
    case kConfiguring: newValue = fgConfiguring; break;
    case kDone:        newValue = fgDone;        break;
    default:           newValue = fgError;       break;
    }
    fState->updateService(const_cast<char*>(newValue));
  }
  void commandHandler()
  {
    SetState(kConfiguring);
    char* str = fGo->getString();
    if (!str || str[0] == '\0') { 
      std::cerr << fGo->getName() << ": Empty command string" << std::endl;
      return;
    }
    std::string tag(str);
    std::transform(tag.begin(),tag.end(),tag.begin(),to_lower());
    // std::cout << fGo->getName() << ": Argument is " << tag << std::endl;
    if (tag == "clear") { 
      HandleClear();
      return;
    }
    HandleCommand(tag);
  }
  virtual void HandleClear() { SetState(kIdle); }
  virtual void HandleCommand(const std::string& arg) 
  { 
    sleep(1); 
    if (arg != "acknowledge") 
      SetState(kDone); 
    else 
      SetState(kIdle);
  }
  DimService* fState;
  DimCommand* fGo;
  static const char* fgIdle;
  static const char* fgConfiguring;
  static const char* fgError;
  static const char* fgDone;
private:
  dummy(const dummy& o);
  dummy& operator=(const dummy& o);

  
};

  
//____________________________________________________________________
const char* dummy::fgIdle        = "idle";
const char* dummy::fgConfiguring = "configuring";
const char* dummy::fgError       = "error";
const char* dummy::fgDone        = "done";
  

//====================================================================
struct dummy_FEC
{
  dummy_FEC(const char* server, unsigned short id)
    : fCSR0(0), 
      fCSR0Serv(const_cast<char*>(MakeServiceName(server, id, "CSR0")), fCSR0)
  {
    sleep(1);
    fCSR0 = 0;
    fCSR0Serv.updateService(fCSR0);
  }
  const char* MakeServiceName(const char* server, unsigned short id,
			      const char* what)
  {
    static std::string name;
    std::stringstream s;
    s << server << "_" << std::setfill('0') << std::setw(2) << id 
      << "_" << what;
    name = s.str();
    return name.c_str();
  }
  int            fCSR0;
  DimService     fCSR0Serv;
};

//====================================================================
struct dummy_RCU 
{
  enum {
    /** Idle state (or off) */ 
    Idle = 1,
    /** Standby state */ 
    Standby, 
    /** When downloading configurations */ 
    Downloading, 
    /** When configured */ 
    Ready, 
    /** Error state */ 
    Error,
    /** Mixed parent state */ 
    Mixed
  };
  
  dummy_RCU(const char* serv, unsigned int active)
    : fServer(serv),
      fState(Idle),
      fActive(active & 0x30003), 
      fAFL(0x0),
      fStateServ(const_cast<char*>(MakeServiceName("RCU_STATE")),fState), 
      fAFLServ(const_cast<char*>(MakeServiceName("AFL")),fAFL), 
      fFecs(32)
  {
    for (size_t i = 0; i < fFecs.size(); i++) fFecs[i] = 0;
    sleep(1);
    ChangeState(Standby);
  }
  const char* MakeServiceName(const char* what)
  {
    static std::string name;
    std::stringstream s;
    s << fServer << "_" << what;
    name = s.str();
    return name.c_str();
  }
  void ChangeState(int state) 
  { 
    fState = state;
    fStateServ.updateService(fState);
  }
  void Configure(unsigned int active)
  {
    ChangeState(Downloading);
    fAFL = active;
    std::cout << "AFL set to 0x" << std::hex << active 
	      << std::dec << std::endl;
    for (unsigned short i = 0; i < 32; i++) { 
      if (!((1 << i) & fAFL)) continue;
      
      TurnOnFec(i);
    }
    fAFLServ.updateService(fAFL);
    sleep(1);
    ChangeState(Ready);
  }
  void Reset() 
  {
    for (unsigned short i = 0; i < 32; i++) { 
      if (!((1 << i) & fAFL)) continue;
      
      TurnOffFec(i);
    }
    fAFL = 0;
    fAFLServ.updateService(fAFL);
    sleep(1);
    ChangeState(Standby);
  }
  void TurnOnFec(unsigned short id) 
  { 
    if (fFecs[id]) return;
    
    fFecs[id] = new dummy_FEC(fServer.c_str(), id);
  }
  void TurnOffFec(unsigned short id)
  {
    if (!fFecs[id]) return;
    
    delete fFecs[id];
    fFecs[id] = 0;
  }
  std::string        fServer;
  int                fState;
  const unsigned int fActive;
  int    	     fAFL;
  DimService         fStateServ;
  DimService         fAFLServ;
  typedef std::vector<dummy_FEC*> FecVector;
  FecVector          fFecs;
};
      
//====================================================================
struct dummy_MINICONF : public dummy
{
  bool GetPart(std::stringstream& s, std::string& val)
  {
    std::string sp;
    std::getline(s, sp, ';');
    if (sp.empty()) return false;
    val = sp;
    return true;
  }
  template <typename T> 
  bool GetValue(std::stringstream& s, T& val)
  {
    std::string t;
    if (!GetPart(s, t)) return false;
    std::stringstream ts(t);
    ts >> val;
    return true;
  }
  dummy_MINICONF(const char* name, dummy_RCU& rcu) 
    : dummy("MINICONF", name), 
      fRcu(rcu)
  {
  }
  void HandleCommand(const std::string& args);
private:
  dummy_MINICONF(const dummy_MINICONF& o);
  dummy_MINICONF& operator=(const dummy_MINICONF& o);
  dummy_RCU& fRcu;
};

template <>
bool 
dummy_MINICONF::GetValue<unsigned int>(std::stringstream& s, unsigned int& val)
{
  std::string t;
  if (!GetPart(s, t)) return false;
  std::stringstream ts(t);
  if (t[0] == '0') { 
    if (t.size() > 1 && (t[1] == 'x' || t[1] == 'X')) 
      ts << std::hex;
    else
      ts << std::oct;
  }
  ts >> val;
  return true;
}
void 
dummy_MINICONF::HandleCommand(const std::string& args)
{
  if (args == "reset") fRcu.Reset();
  else  {
    std::stringstream st(args);
    std::string conf;
    if (!GetPart(st, conf)) { 
      std::cerr << "No run-type specified" << std::endl;
      return;
    }
    unsigned int cards;
    if (!GetValue(st, cards)) { 
      std::cerr << "No cards specified" << std::endl;
      return;
    }
      
      
    fRcu.Configure(cards);
  }
  sleep(1); 
  SetState(kIdle);
}

  
//====================================================================
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h',"help",     "Show this help", false,false);
  Option<bool>         VOpt('V',"version",  "Show version number", false,false);
  Option<std::string>  dOpt('d', "dns",    "DNS host", "localhost");
  Option<std::string>  nOpt('n', "name",   "Server name", "FMD-FEE_0_0_0");
  Option<unsigned int> aOpt('a', "active", "Bit mask of active cards", 0x10000);
  
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(dOpt);
  cl.Add(nOpt);
  cl.Add(aOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { 
    cl.Help();
    return 0;
  }
  
  std::stringstream s;
  s << "dummy_" << nOpt.Value();
  std::string sname = s.str();
  
  DimServer::setDnsNode(dOpt.Value().c_str());
  DimServer::start(const_cast<char*>(sname.c_str()));

  dummy_RCU      rcu(nOpt.Value().c_str(), aOpt.Value());
  dummy_MINICONF miniconf(nOpt.Value().c_str(), rcu);
  dummy          pedconf("PEDCONF", nOpt.Value().c_str());
  
  sleep(1);
  
  while (true) pause();
  
  return 0;
}

