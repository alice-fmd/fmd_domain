#include "TIMER.h"
#include <sstream>
#include <algorithm>

//____________________________________________________________________
namespace {
  void* timer(void* obj)
  {
    if (!obj) return 0;
    TIMER* t = static_cast<TIMER*>(obj);
    t->Run();
    return 0;
  }
}
//____________________________________________________________________
TIMER::TIMER(const std::string& object,
	     unsigned int timeout, 
	     const std::string& domain)
  : SmiProxy(const_cast<char*>(object.c_str())), 
    _object(object),
    _timeout(timeout),
    _stopped(false)
{
  attach(const_cast<char*>(domain.c_str()), 
	 const_cast<char*>(ObjectName()));
  setVolatile();
  SetState(Idle);
}
//____________________________________________________________________
const char* 
TIMER::State2Text(State_t s) const
{
  switch (s) {
  case Idle:        return "IDLE";
  case Running:     return "RUNNING";
  case Timeout:     return "TIMEOUT";
  default:          return "IDLE";
  }
  return "IDLE";
}
//____________________________________________________________________
TIMER::State_t
TIMER::State()
{
  std::string s(getState());
  if      (s == "IDLE")    return Idle;
  else if (s == "RUNNING") return Running;
  else if (s == "TIMEOUT") return Timeout;
  return Idle;
}
//____________________________________________________________________
void
TIMER::SetState(State_t s) 
{
  Info("%s: Setting state to %s", _object.c_str(), State2Text(s));
  setState(const_cast<char*>(State2Text(s)));
}
//____________________________________________________________________
void
TIMER::smiCommandHandler()
{
  Info("%s: Got action %s in state %s", 
       _object.c_str(), getAction(), getState());
  const char* acts[] = { "START", "STOP", "RESET" };
  if      (testAction(const_cast<char*>(acts[0])))  Start();
  else if (testAction(const_cast<char*>(acts[1])))  Stop();
  else if (testAction(const_cast<char*>(acts[2])))  Reset();
}

//____________________________________________________________________
void
TIMER::Start()
{
  // Ignore start commands when running 
  if (State() == Running) { 
    SetState(State());
    return;
  }
    
  // Set the state to running 
  SetState(Running);
  _stopped = false;
    
  // Sleep for specified number miliseconds
  Info("%s: Starting thread", _object.c_str());
  pthread_create(&_thread, 0, &timer, this);
}
//____________________________________________________________________
void
TIMER::Stop()
{
  // Ignore start commands when not running 
  // if (State() != Running) { 
  // SetState(State());
  // return;
  // }
    
  // Set state to idle
  _stopped = true;
  SetState(Idle);
}
//____________________________________________________________________
void
TIMER::Reset()
{
  // Ignore start commands when not running 
  if (State() != Timeout) { 
    SetState(State());
    return;
  }
    
  // Set state to idle
  _stopped = true;
  SetState(Idle);
}
//____________________________________________________________________
void
TIMER::SetVerbose(bool verb) 
{
  if (verb) setPrintOn();
  else      setPrintOff();
}
//____________________________________________________________________
void
TIMER::Run()
{
  unsigned int i = 0;
  time_t start = time(NULL);
  Info("Starting timer %s (%d ms) on %s", _object.c_str(), 
       _timeout, asctime(localtime(&start)));
  while (i < _timeout && !_stopped) { 
    // Info("Timer %s at %d/%d", _object.c_str(), i, _timeout);
    dtq_sleep(1); // usleep(100);
    i++;
  }
  time_t stop = time(NULL);
  Info("Stopping timer %s (after %d s) on %s", _object.c_str(), 
       stop - start,  asctime(localtime(&stop)));

  // Set state to timeout. 
  State_t s = !_stopped ? Timeout : Idle;
  Info("%s: After %d s: %s", _object.c_str(), i, State2Text(s));
  SetState(s);
}
//____________________________________________________________________
// 
// EOF
//

