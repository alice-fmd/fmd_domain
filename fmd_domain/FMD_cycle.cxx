#include <iostream>
#include "TIMER.h"
#include <Options.h>
#include "Msg.h"

//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h',"help",     "Show this help", false,false);
  Option<bool>         VOpt('V',"version",  "Show version number",false,false);
  Option<bool>         vOpt('v', "verbose", "Be verbose", false, false);
  Option<unsigned>     tOpt('t', "timeout", "Timeout in msecs", 3);
  Option<std::string>  dOpt('d', "dns",     "DNS host", "localhost");
  Option<std::string>  DOpt('D', "domain",  "SMI Domain", "FMD_CYCLE");
  
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(DOpt);
  cl.Add(tOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { 
    cl.Help();
    return 0;
  }
  
  std::cout << "DIM DNS node: " << dOpt.Value() << std::endl;
  DimClient::setDnsNode(dOpt.Value().c_str());
  DimServer::setDnsNode(dOpt.Value().c_str());

  TIMER           t("CYCLE_TIMER", tOpt.Value(), DOpt.Value());

  t.SetVerbose(vOpt.Value());
  while (true) pause();

  return 0;
}
//____________________________________________________________________
//
// EOF
//
  
