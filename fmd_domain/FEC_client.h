// -*- mode: C++ -*-
#ifndef FEC_CLIENT_H
#define FEC_CLIENT_H
#include <dim/dic.hxx>
#include <smixx/smirtl.hxx>
#include <string>

//____________________________________________________________________
/**
 * Proxy of a FEC 
 * 
 * This class subscribes to the services 
 * 
 *    <server>_AFL
 *    <server>_<FEC_NO>_CSR0
 *    <server>_<FEC_NO>_CSR1
 *
 * where <server> is the FeeServer name, and <FEC_NO> is the
 * zero-padded, two-digit FEC address in decimal (i.e., 0, 1, 16, or
 * 17), and the state is deduced from these 
 *
 * CSR0 contains the error and interrupt mask, as well as the
 * continous conversion bit.  The meaning of the bits are 
 * 
 *     0     Mask temperature errors (internal to the AD7417s)
 *     1     Mask negative voltages
 *     2     Mask temperature sensors and negative currents 
 *     3     Mask other voltages 
 *     4     Mask other currents 
 *     5     Mask pre-amp power supply errors 
 *     6     Mask ALTRO power supply errors 
 *     7     Mask missed slow clock errors 
 *     8     Mask parity (on bus) errors 
 *     9     Mask instruction errors 
 *    10     Continous conversion 
 *
 * CSR1 contains the interrupts and errors 
 *
 *     
 *     0     Temperature errors (internal to the AD7417s)
 *     1     Negative voltages
 *     2     Temperature sensors and negative currents 
 *     3     Other voltages 
 *     4     Other currents 
 *     5     Pre-amp power supply errors 
 *     6     ALTRO power supply errors 
 *     7     Missed slow clock errors 
 *     8     Parity (on bus) errors 
 *     9     Instruction errors 
 *    10     ALTRO error 
 *    11     Slow control error 
 *    12     Error (true if bits 8 or 9 are asserted and enabled in CSR0)
 *    13     Error (true if any bits 0-7 are asserted and enabled in CSR0)
 *
 */
class FEC_client : public DimInfoHandler, public SmiProxy
{
 public:
  enum State_t {
    Off = 0,
    On, 
    NoMonitor,
    Interrupt,
    Error
  };
  /** 
   * Front-end card client
   * 
   * @param server Server name
   * @param id     Front-end card id
   * 
   */
  FEC_client(const std::string& server, unsigned int id, 
	     const std::string& domain);
  /** 
   * Get string represenstation of state
   * 
   * 
   * @return State (as a string)
   */
  const char* State2Text(State_t v) const;
  const char* State2Text() const { return State2Text(_value); }
  
  /** 
   * Set the state 
   * 
   * @param v The new state 
   */
  void SetState(State_t v);
  /** 
   * Get the state
   * 
   * 
   * @return state value
   */
  State_t State() const { return _value; }
  /** 
   * Handle upstream service changes
   * 
   */
  void infoHandler();
  /** 
   * Whether to be verbose or not
   * 
   * @param verb 
   */  
  void SetVerbose(bool verb);
protected:
  /** 
   * Print information on internal variables, etc. 
   */
  void PrintInfo(State_t v) const;
  void PrintInfo() const { PrintInfo(_value); }
  /** 
   * Check if recieved values are valid
   */
  bool IsValid() const;
  /** 
   * Check if error bits are set 
   */
  bool InError() const;
  /** 
   * Check if interrupt bits are set 
   */
  bool HasInterrupt() const;
  /**
   * Check if the continous conversion bit is asserted 
   */
  bool IsConverting() const;
  /** 
   * Update the state 
   */
  void UpdateState();
  /** 
   * Utility function to make a service name
   * 
   * @param server Server
   * @param what   Service
   * 
   * @return The service name
   */
  static const char* MakeServiceName(const std::string& server, 
				     unsigned short     id,
				     const char*        what);
  static const char* MakeTopServiceName(const std::string& server, 
					const char* what);
  static const char* MakeObjectName(unsigned short id);
  /** 
   * Helper to work-around SMI's bad interface
   * 
   * @param p 
   * 
   * @return 
   */
  char* GetParameter(const char* p) 
  {
    return getParameterString(const_cast<char*>(p));
  }
  /** Identifier */
  unsigned short _id;
  std::string _object;
  /** State service */
  DimStampedInfo _csr0;
  /** State service */
  DimStampedInfo _csr1;
  /** State service */
  DimStampedInfo _afl;
  /** State value */
  State_t        _value;
  /** The CSR0 mask */
  unsigned int _mask;
  /** The CSR1 bits */
  unsigned int _bits;
  /** The conversion bit */
  bool _conversion;
  /** On or not */
  bool _on;
  
};

#endif
//
// EOF
//
