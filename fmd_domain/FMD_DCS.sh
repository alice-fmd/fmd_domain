#!/bin/sh

detector=FMD
prefix=/opt/ecs
exec_prefix=${prefix}
ecssitedir=/ecsSite
bindir=${prefix}/DCS/${detector}/Linux
smidir=${ecssitedir}/DCS/${detector}
logdir=${ecssitedir}/DCS/logs/${detector}
proxy=${bindir}/${detector}_DCS
sobjfile=${smidir}/${detector}_domain.sobj
domain=${detector}_DCS
rsobjfile=${smidir}/${detector}_RUN_domain.sobj
rdomain=${detector}_DCS_RUN


get_domain_pid()
{
    pgrep -f "smiSM ${domain} ${sobjfile}"
}

get_run_pid()
{
    pgrep -f "smiSM ${rdomain} ${rsobjfile}"
}

get_proxy_pid()
{
    pgrep ${proxy}
}

kill_them()
{
    pproxy=`get_proxy_pid`
    echo -n "Killing domain ${proxy} ($pproxy) ... "
    pkill -9 ${proxy}
    echo "OK"
    
    pdomain=`get_domain_pid`
    echo -n "Killing SMI manager of ${domain} ($pdomain) ... "
    pkill -9 -f "smiSM ${domain} ${sobjfile}"
    echo "OK"

    prun=`get_run_pid` 
    echo -n "Killing SMI manager of ${rdomain} ($prun) ... "
    pkill -9 -f "smiSM ${rdomain}_RUN ${rsobjfile}"
    echo "OK"

    # echo -n "Killing DCS controls for $detector ... "
    # pkill -9 ${detector}_dcs
    # echo "OK"
}


start_them()
{
    echo -n "Starting domain ${proxy} ... "
    ${proxy} > ${logdir}/${domain}.assocLog 2>&1 & 
    echo "Done"

    echo -n "Starting state manager of ${domain} ... "
    smiSM ${domain} ${sobjfile} > ${logdir}/${domain}.smiLog 2>&1 &

    echo -n "Starting state manager of ${rdomain} ... "
    smiSM ${rdomain} ${rsobjfile} > ${logdir}/${rdomain}.smiLog 2>&1 &
    echo "Done"
}

show_status()
{
    pdomain=`get_domain_pid`
    prun=`get_run_pid`
    pproxy=`get_proxy_pid`
    domain_exists=`smiDomainExists ${domain} | cut -f3 -d' '` 
    run_exists=`smiDomainExists ${rdomain}_RUN | cut -f3 -d' '` 
    if test "X$pproxy" = "X" ; then 
	proxy_exists=no
    else
	proxy_exists=yes
    fi
    printf "Domain running: %3s (PID %8d)\n" ${domain_exists} ${pdomain}
    printf "Run    running: %3s (PID %8d)\n" ${run_exists}    ${prun}
    printf "Proxy  running: %3s (PID %8d)\n" ${proxy_exists}  ${pproxy}
}
    

oper=restart

arg=`echo $1 | tr '[A-Z]' '[a-z]'` 
case $arg in 
    start|stop|restart|status) oper=$arg ;; 
    --help|-h) usage ; exit 0 ;; 
    *)
	echo "Unknown option $1, try $0 --help" > /dev/stderr 
	exit 1
	;;
esac

case ${oper} in 
    start)	start_them ;;
    stop)	kill_them ;;
    restart)    kill_them ; sleep 1 ; start_them ;; 
    status)	show_status ;;
esac

#
# EOF
#

	



    
