// -*- mode: C++ -*-
#ifndef CONF_CLIENT_H
#define CONF_CLIENT_H
#include <dim/dic.hxx>
#include <smixx/smirtl.hxx>
#include <sstream>
#include <algorithm>

void Info(const char* msg, ...); 
void Error(const char* msg, ...);
void Fatal(const char* msg, ...);

//____________________________________________________________________
namespace
{
  /** 
   * Functor class to turn characters into lower case
   * 
   */
  struct to_lower
  {
    /** 
     * Functor member function.  Turns the passed character into lower
     * case and returns that.
     * 
     * @param c Character to down-case
     *
     * @return Lower-case version of @a c 
     */
    char operator()(char c) const { return std::tolower(c); }
  };
}
//____________________________________________________________________
/**
 * Base class for configurator clients 
 * 
 */
struct CONF_client : public DimInfoHandler, public SmiProxy
{
  /**
   * State enumeration
   * 
   */
  enum State_t { 
    NoLink,
    Idle, 
    Configuring, 
    Done,
    Error
  };
  /** 
   * Constructor 
   * 
   * @param prefix Prefix 
   * @param server Server name 
   * 
   */
  CONF_client(const std::string& prefix, 
	      const std::string& server,
	      const std::string& domain);
  /** 
   * Get the state
   * 
   * 
   * @return The state
   */
  State_t State() const { return _value; }
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  const char* State2Text();
  /** 
   * Handle up-stream service change
   * 
   */
  virtual void infoHandler();
  /** 
   * Handle upstream message service change.
   * 
   */  
  void HandleMessage();
  /** 
   * Handle up-stream state change
   * 
   */
  void HandleState();
  /** 
   * Send command to upstream
   * 
   * @param args Arguments to command 
   * 
   * @return @c true on success
   */
  bool SendCommand(const std::string& args);
  /** 
   * Send the reset command
   * 
   * 
   * @return @c true on success
   */
  bool Reset() { return SendCommand("reset"); }
  /** 
   * Send the clear command 
   * 
   * 
   * @return @c true on success
   */
  bool Clear() { return SendCommand("clear"); }
  /** 
   * Send the acknowledge command
   * 
   * 
   * @return @c true on success
   */
  bool Acknowledge() { return SendCommand("acknowledge"); }
  /** 
   * Handle an SMI command (action) from state machine
   * 
   */
  virtual void smiCommandHandler();
  /** 
   * Whether to be verbose or not
   * 
   * @param verb 
   */  
  void SetVerbose(bool verb);
  /** 
   * Handle the configure command.  Must be implemented in concrete
   * class. 
   * 
   */
  virtual void HandleConfigure() = 0;
  virtual void HandleSafety() {}
protected:
  /** 
   * Copy constructor
   * 
   * @param o Object to copy from
   */  
  CONF_client(const CONF_client& o);
  /** 
   * Static function to make service names 
   * 
   * @param prefix config prefix
   * @param server server name
   * @param what   Which service
   * 
   * @return Pointer into static buffer
   */
  static const char* MakeServiceName(const std::string& prefix, 
				     const std::string& server, 
				     const char*        what);
  /** 
   * Helper member function to make the object name
   * 
   * @param prefix Prefix of configurator
   * 
   * @return 
   */  
  static const char* MakeObjectName(const std::string& prefix);
  /** 
   * Service function to extract arguments from passed parameters 
   * 
   * @param args    Stream to write to
   * @param option  Option to look for
   * @param value   Value to send
   * @param def     Default value. 
   */
  template <typename T>
  void ParseOption(std::stringstream& args, 
		   const char*        option, 
		   const std::string& value,
		   const T&           def=T())
  {
    std::string val(value);
    std::transform(val.begin(),val.end(),val.begin(),to_lower());
    if (val == "undefined" || val == "default") { 
      std::stringstream s;
      s << def;
      val = s.str();
    }
    args << ";" << option << "=" << val;
  }
    
  /** 
   * Helper to work-around SMI's bad interface
   * 
   * @param p 
   * 
   * @return 
   */
  const std::string& GetParameter(const char* p);
  /** PRefix */
  std::string _prefix;
  /** Domain of object */
  std::string _domain;
  /** Name of object */
  std::string _name;
  /** PRefix */
  std::string _server;
  /** State service */
  DimStampedInfo _state;
  /** State service */
  DimStampedInfo _msg;
  /** Command name */
  std::string    _go;
  /** State */
  State_t        _value;
  
};

//____________________________________________________________________
template <>
inline void 
CONF_client::ParseOption<bool>(std::stringstream& args, 
			       const char*        option, 
			       const std::string& value, 
			       const bool&        def)
{
  std::string val(value);
  std::transform(val.begin(),val.end(),val.begin(),to_lower());
  bool use = false;
  if      (val == "yes"       || val == "true") use = true;
  else if (val == "undefined" || val == "default") use = def;
    Info("Got value %s for option %s -> %s", value.c_str(),option,
	 use ? "true" : "false");
  if (use) args << ";" << option;
}
#endif
//____________________________________________________________________
//
// EOF
//
