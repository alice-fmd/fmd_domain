#include "FEC_client.h"
#include <sstream>
#include <iostream>
#include <iomanip>

void Info(const char* msg, ...); 
void Error(const char* msg, ...);
void Fatal(const char* msg, ...);


//____________________________________________________________________
FEC_client::FEC_client(const std::string& server, unsigned int id, 
		       const std::string& domain)
  : SmiProxy(const_cast<char*>(MakeObjectName(id))), 
    _id(id),
    _object(MakeObjectName(id)),
    _csr0(const_cast<char*>(MakeServiceName(server, id, "CSR0")), 
	  0, 0, 0, this), 
    _csr1(const_cast<char*>(MakeServiceName(server, id, "CSR1")), 
	  0, 0, 0, this), 
    _afl(const_cast<char*>(MakeTopServiceName(server, "AFL")), 
	 0, 0, 0, this), 
    _value(Off), 
    _mask(0x0),
    _bits(0x0), 
    _on(false)
{
  attach(const_cast<char*>(domain.c_str()), 
	 const_cast<char*>(MakeObjectName(id)), 1);
  setVolatile();
  setState(const_cast<char*>(State2Text()));
}

//____________________________________________________________________
const char* 
FEC_client::State2Text(State_t v) const
{
  switch (v) {
  case On:        return "ON";
  case Error:     return "ERROR";
  case NoMonitor: return "NOMONITOR";
  case Interrupt: return "INTERRUPT";
  default:        return "OFF";
  }
  return "OFF";
}
//____________________________________________________________________
bool
FEC_client::IsValid() const
{
  return (_bits != unsigned(-1) && _mask != unsigned(-1) &&
	  _bits <= 0x3FFF       && _mask <= 0x7FF);
}
//____________________________________________________________________
bool
FEC_client::InError() const
{
  if (!IsValid()) return false;
  // return _bits & (1 << 12);
  return (((_bits & (_mask|0xc00)) & 0xF00) != 0);
}
//____________________________________________________________________
bool
FEC_client::HasInterrupt() const
{
  if (!IsValid()) return false;
  // return _bits & (1 << 13);
  return (((_bits & _mask) & 0xFF) != 0);
}
//____________________________________________________________________
bool
FEC_client::IsConverting() const
{
  if (!IsValid()) return false;
  return (_mask & (1 << 10)) != 0;
}

//____________________________________________________________________
void 
FEC_client::SetState(State_t v)
{
  if (_value == v) return;
  // PrintInfo(v);
  _value = v;
  setState(const_cast<char*>(State2Text()));
}
//____________________________________________________________________
void 
FEC_client::PrintInfo(State_t v) const
{
  Info("%s: is %s, %s, %s, monitoring is %s, values are %s, "
       "bits=0x%08x, mask=0x%08x -> %s",
       _object.c_str(), 
       (_on            ? "ON"         : "OFF"),
       (InError()      ? "ERROR"      : "NO-ERRORS"),
       (HasInterrupt() ? "INTERRUPT"  : "ALL-GOOD"),
       (IsConverting() ? "ON"         : "OFF"), 
       (IsValid()      ? "VALID"      : "INVALID"),
       _bits, _mask, State2Text(v));
}

//____________________________________________________________________
void 
FEC_client::infoHandler()
{
  DimInfo* info = getInfo();
  if (!info) return;
  void*    data   = info->getData();
  int      size   = info->getSize();
  bool     nolink =  (!data || size <= 0);
  if (nolink) { 
    _on         = false;
    _bits       = unsigned(-1);
    _mask       = unsigned(-1);
  } 
  else {
    unsigned int val = 0;
    memcpy(&val, data, sizeof(int));
    //Info("%s: Update of %s: 0x%08x", _object.c_str(), info->getName(), val);
    if      (info == &_csr0) _mask = val;
    else if (info == &_csr1) _bits = val;
    else if (info == &_afl)  _on   = (val & (1 << _id)) != 0;;
  }
  UpdateState();
}
//____________________________________________________________________
void 
FEC_client::SetVerbose(bool verb) 
{
  if (verb) setPrintOn();
  else      setPrintOff();
}
//____________________________________________________________________
void 
FEC_client::UpdateState() 
{
  State_t next     = _value;
  if      (!_on)            next = Off;
  else if (!IsValid())      next = NoMonitor;
  else if (HasInterrupt())  next = Interrupt;
  else if (InError())       next = Error;
  else if (!IsConverting()) next = NoMonitor;
  else                      next = On;
  SetState(next);
}    

//____________________________________________________________________
const char*
FEC_client::MakeServiceName(const std::string& server, 
				     unsigned short     id,
				     const char*        what)
{
  static std::string name;
  std::stringstream s;
  s << server << "_" << std::setfill('0') << std::setw(2) 
    << id << "_" << what;
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
const char*
FEC_client::MakeTopServiceName(const std::string& server, 
					const char* what)
{
  static std::string name;
  std::stringstream s;
  s << server << "_" << what;
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
const char*
FEC_client::MakeObjectName(unsigned short id) 
{ 
  static std::string name;
  std::stringstream s;
  s << "FMD_FEC_" << std::setfill('0') << std::setw(2) << id;
  name = s.str();
  return name.c_str();
}


//____________________________________________________________________
//
// EOF
//
