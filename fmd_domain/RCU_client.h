// -*- mode: C++ -*-
#ifndef RCU_CLIENT_H
#define RCU_CLIENT_H
#include <dim/dic.hxx>
#include <smixx/smirtl.hxx>
#include <string>


//____________________________________________________________________
class RCU_client : public DimInfoHandler, public SmiProxy
{
 public:
  enum State_t {
    NoLink = 0,
    Idle,
    Standby,
    Downloading,
    Ready,
    Mixed, 
    Error,
    Running
  };
  RCU_client(const std::string& server, const std::string& domain);
  /** 
   * Get string represenstation of state
   * 
   * 
   * @return State (as a string)
   */
  const char* State2Text();
  /** 
   * Get the state
   * 
   * 
   * @return state value
   */
  State_t State() const { return _value; }
  /** 
   * Handle upstream service changes
   * 
   */
  void infoHandler();
  /** 
   * Handle upstream state changes 
   * 
   */
  void HandleState(); 
  /** 
   * Handle upstream state changes 
   * 
   */
  void HandleActive();
  /** 
   * Whether to be verbose or not
   * 
   * @param verb 
   */  
  void SetVerbose(bool verb);
protected:
  /** 
   * Utility function to make a service name
   * 
   * @param server Server
   * @param what   Service
   * 
   * @return The service name
   */
  static const char* MakeServiceName(const std::string& server, 
				     const char*        what);
  static const char* MakeObjectName() { return "FMD_RCU"; }
  /** 
   * Helper to work-around SMI's bad interface
   * 
   * @param p 
   * 
   * @return 
   */
  char* GetParameter(const char* p) 
  {
    return getParameterString(const_cast<char*>(p));
  }
  /** State service */
  DimStampedInfo _state;
  /** State service */
  DimStampedInfo _afl;
  /** State value */
  State_t        _value;
  /** Active cards */
  unsigned int   _active;
};


#endif
//____________________________________________________________________
//
// EOF
//
