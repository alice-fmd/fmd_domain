#include "CONF_client.h"
#include <string>
#include <sstream>
#include <algorithm>

//____________________________________________________________________
CONF_client::CONF_client(const std::string& prefix, const std::string& server,
			 const std::string& domain)
  : SmiProxy(const_cast<char*>(MakeObjectName(prefix))), 
    _prefix(prefix),
    _domain(domain), 
    _name(MakeObjectName(prefix)),
    _server(server),
    _state(const_cast<char*>(MakeServiceName(prefix, server, "STATE")), 
	   0, 0, 0, this), 
    _msg(const_cast<char*>(MakeServiceName(prefix, server, "MSG")), 
	 0, 0, 0, this), 
    _go(MakeServiceName(prefix, server, "GO")),
    _value(NoLink)
{
  Info("%s: attaching to %s:%s", prefix.c_str(), domain.c_str(), 
       _name.c_str());
  attach(const_cast<char*>(domain.c_str()), 
	 const_cast<char*>(_name.c_str()));
  Info("%s: Attached to %s::%s", prefix.c_str(), domain.c_str(), getCommand());
  setVolatile();
  Info("%s: Subscribing to service %s", _prefix.c_str(), _state.getName());
  setState(const_cast<char*>(State2Text()));
}
//____________________________________________________________________
const char* 
CONF_client::State2Text()
{
  switch (_value) {
  case Idle:        return "IDLE";
  case Configuring: return "CONFIGURING";
  case Done:        return "DONE";
  case Error:       return "ERROR";
  default:          return "OFF";
  }
  return "OFF";
}
//____________________________________________________________________
void
CONF_client::infoHandler()
{
  DimInfo* info = getInfo();
  if      (!info) return;
  else if (info == &_state) HandleState();
  else if (info == &_msg)   HandleMessage();
}
//____________________________________________________________________
void
CONF_client::HandleMessage()
{
  void*          data   = _msg.getData();
  int            size   = _msg.getSize();
  bool           nolink =  (!data || size <= 0);
  if (nolink) return;
  unsigned short level;
  memcpy(&level, data, sizeof(short));
  char*          cdata  = reinterpret_cast<char*>(data);
  char*          msg    = &(cdata[sizeof(short)]);
    
  switch (level) { 
  case 1:  ::Error("%s says: %s", _prefix.c_str(), msg); break;
  case 2:  ::Fatal("%s says: %s", _prefix.c_str(), msg); break;
  default: ::Info("%s says: %s",  _prefix.c_str(), msg); break;
  }
}
//____________________________________________________________________
void
CONF_client::HandleState()
{
  void*       data   = _state.getData();
  int         size   = _state.getSize();
  bool        nolink =  (!data || size <= 0 || size > 1024);
  // Info("Got data %p of size %d", data, size);
  State_t     next = NoLink;
  std::string s("nolink");
  if      (!nolink)                                    s = _state.getString();
  if      (std::string::npos != s.find("nolink"))      next = NoLink;
  else if (std::string::npos != s.find("idle"))        next = Idle;
  else if (std::string::npos != s.find("configuring")) next = Configuring;
  else if (std::string::npos != s.find("done"))        next = Done;
  else if (std::string::npos != s.find("error"))       next = Error;
  else                                                 next = Error;
  // Set SMI state 
  if (next != _value) { 
    _value = next;
    Info("%s: Now in state %s (%s)", _prefix.c_str(), State2Text(), s.c_str());
    setState(const_cast<char*>(State2Text()));
  }
}  
//____________________________________________________________________
bool
CONF_client::SendCommand(const std::string& args)
{
  int ret = DimClient::sendCommand(_go.c_str(), args.c_str());
  return ret == 0;
}
//____________________________________________________________________
void
CONF_client::smiCommandHandler()
{
  Info("%s: Got action %s", _prefix.c_str(), getAction());
  // std::cout << "Object " << _prefix << " parameters: " << std::endl;
  // char* param = 0;
  // while ((param = getNextParameter())) {
  // std::cout << "\t" << param << std::endl;
  // }
  const char* acts[] = { "CLEAR", 
			 "RESET", 
			 "CONFIGURE", 
			 "SAFETY", 
			 "ACKNOWLEDGE" };
  if      (testAction(const_cast<char*>(acts[0])))  Clear();
  else if (testAction(const_cast<char*>(acts[1])))  Reset();
  else if (testAction(const_cast<char*>(acts[2])))  HandleConfigure();
  else if (testAction(const_cast<char*>(acts[3])))  HandleSafety();
  else if (testAction(const_cast<char*>(acts[4])))  Acknowledge();
}
//____________________________________________________________________
void
CONF_client::SetVerbose(bool verb) 
{
  if (verb) setPrintOn();
  else      setPrintOff();
}
//____________________________________________________________________
CONF_client::CONF_client(const CONF_client& o) 
  : DimInfoHandler(),
    SmiProxy(const_cast<char*>(o._name.c_str())), 
    _prefix(o._prefix), 
    _name(o._name), 
    _domain(o._domain),
    _server(o._server),
    _state(const_cast<char*>(MakeServiceName(_prefix, _server, "/STATE")), 
	   0, 0, 0, this), 
    _go(o._go),
    _value(o._value)
{}
//____________________________________________________________________
const char*
CONF_client::MakeServiceName(const std::string& prefix, 
			     const std::string& server, 
			     const char*        what)
{
  static std::string name;
  std::stringstream s;
  s << prefix << "_" << server << "/" << what;
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
const char*
CONF_client::MakeObjectName(const std::string& prefix) 
{
  static std::string name;
  std::stringstream s;
  s << "FMD_" << prefix;
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
const std::string&
CONF_client::GetParameter(const char* p) 
{
  static std::string ret;
  char* cp = const_cast<char*>(p);
  // Info("Values of %s are int=%d, float=%f, string=%s", p, 
  //      getParameterInt(cp), getParameterFloat(cp), getParameterString(cp));
  if (getParameterType(cp) != SMI_STRING) { 
    std::stringstream s;
    if (getParameterType(cp) == SMI_INTEGER) 
      s << getParameterInt(cp);
    else 
      s << getParameterFloat(cp);
    ret = s.str();
  }
  else 
    ret = getParameterString(cp);
  Info("Parameter %s -> %s", p, ret.c_str());
  return ret;
}
//____________________________________________________________________
//
// EOF
//
