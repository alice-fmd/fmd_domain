// -*- mode: C++ -*-
#ifndef TIMER_H
#define TIMER_H
#include <dim/dic.hxx>
#include <smixx/smirtl.hxx>
#include <string>
#include <pthread.h>

void Info(const char* msg, ...); 
void Error(const char* msg, ...);
void Fatal(const char* msg, ...);

//____________________________________________________________________
struct TIMER : public SmiProxy
{
  /** Stat type */
  enum State_t { 
    /** Idle state */
    Idle = 1, 
    /** Running state */
    Running, 
    /** timeout state */
    Timeout
  };
    
  /** 
   * Constructor 
   * 
   * @param timeout Timeout in miliseconds
   */
  TIMER(const std::string& object,
	unsigned int timeout=5, 
	const std::string& domain=std::string("FMD_DCS"));
  /** 
   * Get the object name
   * 
   * 
   * @return The object name 
   */
  const char* ObjectName() const { return _object.c_str(); }
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  const char* State2Text(State_t s) const;
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  State_t State();
  /** 
   * Set the state from enumeration value
   * 
   * @param s New state
   */  
  void SetState(State_t s);
  /** 
   * Handle an SMI command (action) from state machine
   * 
   */
  virtual void smiCommandHandler();
  /** 
   * Handle the start action
   * 
   */  
  void Start();
  /** 
   * Handle the stop action
   * 
   */  
  void Stop();
  /** 
   * Handle the reset action
   * 
   */
  void Reset();
  /** 
   * Whether to be verbose or not
   * 
   * @param verb 
   */  
  void SetVerbose(bool verb);
  /** 
   * Run timer in separate thread
   * 
   */
  void Run();
  /** Object name */
  std::string _object;
  /** The timeout - in miliseconds */
  unsigned int _timeout;
  /** Stopped */
  bool _stopped;
  /** Our thread */ 
  pthread_t _thread;
};


#endif
//____________________________________________________________________
//
// EOF
//

  
